/****************************************************************************
** Meta object code from reading C++ file 'chesswindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../QtChess/chesswindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'chesswindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ChessWindow_t {
    QByteArrayData data[35];
    char stringdata[379];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_ChessWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_ChessWindow_t qt_meta_stringdata_ChessWindow = {
    {
QT_MOC_LITERAL(0, 0, 11),
QT_MOC_LITERAL(1, 12, 13),
QT_MOC_LITERAL(2, 26, 0),
QT_MOC_LITERAL(3, 27, 17),
QT_MOC_LITERAL(4, 45, 10),
QT_MOC_LITERAL(5, 56, 12),
QT_MOC_LITERAL(6, 69, 12),
QT_MOC_LITERAL(7, 82, 11),
QT_MOC_LITERAL(8, 94, 19),
QT_MOC_LITERAL(9, 114, 20),
QT_MOC_LITERAL(10, 135, 7),
QT_MOC_LITERAL(11, 143, 8),
QT_MOC_LITERAL(12, 152, 8),
QT_MOC_LITERAL(13, 161, 10),
QT_MOC_LITERAL(14, 172, 6),
QT_MOC_LITERAL(15, 179, 6),
QT_MOC_LITERAL(16, 186, 4),
QT_MOC_LITERAL(17, 191, 20),
QT_MOC_LITERAL(18, 212, 2),
QT_MOC_LITERAL(19, 215, 18),
QT_MOC_LITERAL(20, 234, 2),
QT_MOC_LITERAL(21, 237, 10),
QT_MOC_LITERAL(22, 248, 10),
QT_MOC_LITERAL(23, 259, 8),
QT_MOC_LITERAL(24, 268, 15),
QT_MOC_LITERAL(25, 284, 3),
QT_MOC_LITERAL(26, 288, 12),
QT_MOC_LITERAL(27, 301, 11),
QT_MOC_LITERAL(28, 313, 12),
QT_MOC_LITERAL(29, 326, 5),
QT_MOC_LITERAL(30, 332, 5),
QT_MOC_LITERAL(31, 338, 5),
QT_MOC_LITERAL(32, 344, 12),
QT_MOC_LITERAL(33, 357, 11),
QT_MOC_LITERAL(34, 369, 8)
    },
    "ChessWindow\0newGameSignal\0\0validationSucceed\0"
    "const int*\0syncPvPBoard\0const Piece*\0"
    "performMove\0selectComputerPiece\0"
    "cancelComputerSelect\0newGame\0openGame\0"
    "saveGame\0saveGameAs\0unmove\0remove\0"
    "help\0recievePiecePosition\0n1\0"
    "recieveNewPosition\0n2\0sendViaLan\0"
    "createGame\0joinGame\0recievedMessage\0"
    "str\0infoRecieved\0std::string\0switchPlayer\0"
    "black\0score\0check\0computerTurn\0"
    "changeDepth\0autoMode\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ChessWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  134,    2, 0x06,
       3,    2,  135,    2, 0x06,
       5,    2,  140,    2, 0x06,
       7,    2,  145,    2, 0x06,
       8,    0,  150,    2, 0x06,
       9,    0,  151,    2, 0x06,

 // slots: name, argc, parameters, tag, flags
      10,    0,  152,    2, 0x08,
      11,    0,  153,    2, 0x08,
      12,    0,  154,    2, 0x08,
      13,    0,  155,    2, 0x08,
      14,    0,  156,    2, 0x08,
      15,    0,  157,    2, 0x08,
      16,    0,  158,    2, 0x08,
      17,    1,  159,    2, 0x08,
      19,    2,  162,    2, 0x08,
      22,    0,  167,    2, 0x08,
      23,    0,  168,    2, 0x08,
      24,    1,  169,    2, 0x08,
      26,    2,  172,    2, 0x08,
      28,    2,  177,    2, 0x08,
      31,    0,  182,    2, 0x08,
      32,    0,  183,    2, 0x08,
      33,    1,  184,    2, 0x08,
      34,    1,  187,    2, 0x08,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4, QMetaType::Int,    2,    2,
    QMetaType::Void, 0x80000000 | 4, 0x80000000 | 6,    2,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    2,    2,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void, QMetaType::Int, QMetaType::Bool,   20,   21,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   25,
    QMetaType::Void, 0x80000000 | 27, QMetaType::Int,    2,    2,
    QMetaType::Void, QMetaType::Bool, QMetaType::Int,   29,   30,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Bool,    2,

       0        // eod
};

void ChessWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ChessWindow *_t = static_cast<ChessWindow *>(_o);
        switch (_id) {
        case 0: _t->newGameSignal(); break;
        case 1: _t->validationSucceed((*reinterpret_cast< const int*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->syncPvPBoard((*reinterpret_cast< const int*(*)>(_a[1])),(*reinterpret_cast< const Piece*(*)>(_a[2]))); break;
        case 3: _t->performMove((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: _t->selectComputerPiece(); break;
        case 5: _t->cancelComputerSelect(); break;
        case 6: _t->newGame(); break;
        case 7: _t->openGame(); break;
        case 8: _t->saveGame(); break;
        case 9: _t->saveGameAs(); break;
        case 10: _t->unmove(); break;
        case 11: _t->remove(); break;
        case 12: _t->help(); break;
        case 13: _t->recievePiecePosition((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->recieveNewPosition((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 15: _t->createGame(); break;
        case 16: _t->joinGame(); break;
        case 17: _t->recievedMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 18: _t->infoRecieved((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 19: _t->switchPlayer((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 20: _t->check(); break;
        case 21: _t->computerTurn(); break;
        case 22: _t->changeDepth((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->autoMode((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ChessWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ChessWindow::newGameSignal)) {
                *result = 0;
            }
        }
        {
            typedef void (ChessWindow::*_t)(const int * , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ChessWindow::validationSucceed)) {
                *result = 1;
            }
        }
        {
            typedef void (ChessWindow::*_t)(const int * , const Piece * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ChessWindow::syncPvPBoard)) {
                *result = 2;
            }
        }
        {
            typedef void (ChessWindow::*_t)(int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ChessWindow::performMove)) {
                *result = 3;
            }
        }
        {
            typedef void (ChessWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ChessWindow::selectComputerPiece)) {
                *result = 4;
            }
        }
        {
            typedef void (ChessWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ChessWindow::cancelComputerSelect)) {
                *result = 5;
            }
        }
    }
}

const QMetaObject ChessWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_ChessWindow.data,
      qt_meta_data_ChessWindow,  qt_static_metacall, 0, 0}
};


const QMetaObject *ChessWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ChessWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ChessWindow.stringdata))
        return static_cast<void*>(const_cast< ChessWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int ChessWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 24;
    }
    return _id;
}

// SIGNAL 0
void ChessWindow::newGameSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void ChessWindow::validationSucceed(const int * _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ChessWindow::syncPvPBoard(const int * _t1, const Piece * _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ChessWindow::performMove(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ChessWindow::selectComputerPiece()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void ChessWindow::cancelComputerSelect()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}
QT_END_MOC_NAMESPACE
