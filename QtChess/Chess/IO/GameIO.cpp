#include "GameIO.h"
#include <QDebug>
#include <array>
//------------------------------------------------------------------- 

void GameIO::formatDataForFile(char *map, const int *board, const Piece *pieces)
{
	int p = -1;
	for(int n = 0; n < 64; ++n)
	{
		p = board[n];
		if(p == -1) // �����
			map[n] = 0;
		else
		{
			Piece piece = pieces[p];
		    uchar type = piece.getPiece();
            map[n] = piece.getColor() == BLACK ? -type : type;
		}
	}
}

void GameIO::formatDataForProgram(char *map, int *board, Piece *pieces)
{
	int p = -1;
	for(int n = 0, i = 0; n < 64; ++n)
	{
		p = map[n];
		if(!p)
			board[n] = -1;
		else
		{
			pieces[i].setPiece(n, abs(p), p > 0 ? WHITE : BLACK);
			board[n] = i;
			++i;
		}
	}
}

//------------------------------------------------------------------- 

/// ������ ������ � ����
bool GameIO::saveGame(const int *board, const Piece *pieces, bool player)
{
    io_.open(fileName_, std::ios::out);
	if(io_.is_open())
	{
		// �������� ������ ����������...
		char map[64];
		formatDataForFile(map, board, pieces);
		unsigned int i;
        unsigned int j;
        for (i = 0; i < 8; ++i)
        {
            for (j = 0; j < 8; ++j)
            {
                int n = i * 8 + j;
                int data = map[n];
                io_ << data << "|";
            }
            io_ << "\n";
        }
		io_ << player;
		io_.close();
		return true;
	}
	return false;
}

/// ������ ������ �� �����
bool GameIO::loadGame(int *board, Piece *pieces, bool &player)
{
	io_.open(fileName_, std::ios::in);
	if(io_.is_open())
	{
        std::vector<std::string> map;
        unsigned int i;
        std::string token;
        for (i = 0; i < 8; ++i)
        {
            while(std::getline(io_, token, '|')) {
                //qDebug() << QString::fromStdString(token) << ' ';
                map.push_back(token);
            }
            //qDebug() << '\n';
        }
		io_.close();
        player = std::stoi(map[map.size()-1]); // last digit
        char mapChar[64];
        for(i = 0; i < 64; ++i)
             mapChar[i] = (char)std::stoi(map.at(i));
		// ����������� 
        formatDataForProgram(mapChar, board, pieces);
		return true;
	}
	return false;
}
