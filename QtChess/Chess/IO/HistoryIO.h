

#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include "../../Chess/PvP/chessDataTypes.h"

#include <stack>

using std::stack;
using std::pair;

class HistoryIO
{
public:

    class CellData
    {
    public:
        CellData() { }
        CellData(const Piece &pn1, const Piece &pn21)
        {
            pN1 = pn1;
            pN21 = pn21;
        }

        ~CellData()
        {}
        Piece pN1;     // ������ �� n1 (������ �������)
        Piece pN21;    // ������ �� n2 �� ����
    };

	typedef enum 
	{
        MODE_MV_BY_MV_FILE,             // ������ ���� �� �����
        MODE_ONE_PROC_FILE,             // ������� ����� ���������� ��� ��������
        MODE_MV_BY_MV_ONLY_LOCAL		// ������ �������������� � ������ stack, ��� ������ � ����
	}Mode;

    HistoryIO(Mode mode);
	~HistoryIO(void);
    //---------------------------------------------------------------
    // ������ �� ������ MODE_MV_BY_MV_ONLY_LOCAL
    //---------------------------------------------------------------
    void setPiecePosition(uchar n1);
    void setNewPosition(uchar n2, const CellData & data);
    pair<data::move, HistoryIO::CellData> unMove();
    pair<data::move, HistoryIO::CellData> reMove();
    std::string getMoveString();
    //---------------------------------------------------------------
    // ������ � ������ MODE_MV_BY_MV_FILE
    //---------------------------------------------------------------
	/// ������ ���� � ����
	bool writeMove(data::move mv);
	/// ������ ���� �� �����
    data::move readMove();
	/// ������� �����
	/// >> ������ ��� ������
    void open(std::string fileName);
	/// ������� �����
	void close();
    //---------------------------------------------------------------
    // ������ � ������ MODE_ONE_PROC_FILE
    //---------------------------------------------------------------
    /// ������ ���� � ����
    bool writeMove(std::string fileName);
    data::move readMove(std::string fileName);
public:
	/// ��������������� ���� (�������� 0 � a8 ��� 52 � e2)
	static void toStandartNotation(unsigned n, char **str);
	/// �������� �����������
	static int fromStandartNotation(const char* str);

private:

private:
	Mode mode_;
	/// ��������� �� �����
	std::fstream f_;
    stack<pair<data::move, CellData>> history_;
    stack<pair<data::move, CellData>> historyUnDo_;
    data::move currentMove_;
};

