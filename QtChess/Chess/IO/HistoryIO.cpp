#include "HistoryIO.h"

 HistoryIO::HistoryIO(Mode mode) : mode_(mode)
 { }

HistoryIO::~HistoryIO(void)
{
	if(f_.is_open())
		close();
	while(!history_.empty())
		history_.pop();
}

//--------------------------------------------------------------------------
// ������ �� ������
void HistoryIO::setPiecePosition(uchar n1)
{
    if(n1 > 63)
        throw "Smth went wrong with n1 or n2! (HistoryIO)";
    currentMove_.n1 = n1;
}

void HistoryIO::setNewPosition(uchar n2, const CellData & data)
{
    if(currentMove_.n1 > 63 || n2 > 63)
        throw "Smth went wrong with n1 or n2! (HistoryIO)";
    currentMove_.n2 = n2;
    history_.push(pair<data::move, CellData>(currentMove_, data));
}

pair<data::move, HistoryIO::CellData> HistoryIO::unMove()
{
    if(history_.empty())
        throw "overflow";
    historyUnDo_.push(history_.top());
    history_.pop();
    auto move = historyUnDo_.top();
    currentMove_ = move.first;
    return move;
}

pair<data::move, HistoryIO::CellData> HistoryIO::reMove()
{
    if(historyUnDo_.empty())
        throw "overflow";
    history_.push(historyUnDo_.top());
    historyUnDo_.pop();
    auto move = history_.top();
    currentMove_ = move.first;
    return move;
}

std::string HistoryIO::getMoveString()
{
    char *n1Str = NULL;
    char *n2Str = NULL;
    toStandartNotation(currentMove_.n1, &n1Str);
    toStandartNotation(currentMove_.n2, &n2Str);
    std::string str = n1Str;
    str.append(n2Str);
    delete []n1Str;
    delete []n2Str;
    return str;
}

//--------------------------------------------------------------------------
// ������ � ������

void HistoryIO::open(std::string fileName)
{
    if(mode_ == MODE_MV_BY_MV_FILE)
    {
        // �������� ������ ������/������
        f_.open(fileName.c_str(), std::ios::in & std::ios::app);
    }
    else
        throw "Wrong use of open(), invalide mode (HistoryIO)";

}

void HistoryIO::close()
{
     if(mode_ == MODE_MV_BY_MV_FILE)
     {
         // �������� ������ ������/������
         f_.close();
     }
     else
         throw "Wrong use of close(), invalide mode (HistoryIO)";
}

//------------------------------------------------------------------- 
/// ������ ���� � ����
bool HistoryIO::writeMove(data::move mv)
{
    if(mode_ != MODE_MV_BY_MV_FILE)
         throw "invalide mode (writeMove)(HistoryIO)";
    if(!f_.is_open())
        throw "Stream wasn't open()! (writeMove)(HistoryIO)";

	char *n1Str = NULL;
	char *n2Str = NULL;
	toStandartNotation(mv.n1, &n1Str);
	toStandartNotation(mv.n2, &n2Str);
	//-------
	// ������ n1Str n2Str
	f_<<n1Str<<n2Str;
    f_.put('\0');
    f_.put('\n');
	//-------
	delete[] n1Str;
	delete[] n2Str;

	return true;
}
/// ������ ���� �� �����
data::move HistoryIO::readMove()
{
	data::move mv;
	mv.n1 = 0;
	mv.n2 = 0;

    if(mode_ == MODE_MV_BY_MV_FILE)
    {
        if(!f_.is_open()) throw "Stream wasn't open()! (readMove)(HistoryIO)";

        if(f_.good())
        {
            char n1Str[2];
            char n2Str[2];
            char* tmp = new char[5];
            // ������ ������ ����
            f_.getline(tmp, 5);
            // letter          // digit
            n1Str[0] = tmp[0]; n1Str[1] = tmp[1];
            n2Str[0] = tmp[2]; n2Str[1] = tmp[3];
            mv.n1 = fromStandartNotation(n1Str);
            mv.n2 = fromStandartNotation(n2Str);
            delete[] tmp;
        }
        else
        {
            close();
        }
    }
	return mv;
}

bool HistoryIO::writeMove(std::string fileName)
{
    fileName = "";
    if(mode_ == MODE_MV_BY_MV_FILE)
    {
         // write all "stacked" data
    }
    else
        throw "invalide mode (writeMove)(HistoryIO)";
    return true;
}

data::move HistoryIO::readMove(std::string fileName)
{
    fileName = "";
    if(mode_ == MODE_ONE_PROC_FILE)
    {
        if(f_.is_open())
            f_.close();
        // read all "stacked" data
    }
    else
        throw "invalide mode (read)(HistoryIO)";
    return data::move();
}

//------------------------------------------------------------------- Helpers
/// ��������������� ���� (�������� 0 � a8 ��� 52 � e2)
void HistoryIO::toStandartNotation(unsigned n, char **str)
{
	*str = new char[3];
	char *ptr = *str;
	unsigned x = n & 7;
	unsigned y = n >> 3;
	ptr[0] = 'a' + x;
	ptr[1] = '\0';
    std::string s = std::to_string(8-y);  /*C++ 11*/
	strcat(ptr, s.c_str());
}
/// �������� �����������
int HistoryIO::fromStandartNotation(const char* str)
{
	if(!str) return -1;
	unsigned x = str[0] - 'a';
	unsigned y = 8 - atoi(++str);
	return y * 8 + x;
}
