#pragma once

#include <math.h>
#include <fstream>
#include <string>
#include "../../Chess/PvP/chessDataTypes.h"

class GameIO
{
public:

    typedef enum // ����� ������ ������
	{
		WRITE,
		READ
	}Mode;

    GameIO(std::string fileName, Mode mode) : fileName_(fileName), mode_(mode)  // ������ ������������� ������������
	{ }
	~GameIO(void)
	{
        if(io_.is_open())   // ���� ����� ������
            io_.close();    // ������� ���
	}

	/// ������ ������ � ����
    bool saveGame(const int *board,      /*�����-�������� ������ ���������� ����� �� ������� pieces*/
                  const Piece *pieces,   /*������ ���������� ����� (�������� ���������� � ������ ������)*/
                  bool player);          /*��� ���*/
	/// ������ ������ �� �����
    /// ���������� ���������������� ������
	bool loadGame(int *board, Piece *pieces, bool &player);

protected:
    std::fstream io_;       // ��������� �� �����
    std::string fileName_;  // ��� �����
    Mode mode_;             // ����� ������
private:
    /// �������������� ������ (���������� ������ � ������ � ����)
	void formatDataForFile(char *map, const int *board, const Piece *pieces);
    /// ����������� � ������, ������������ � ��������� (���������� ������ � ������������� � ���������)
	void formatDataForProgram(char *map, int *board, Piece *pieces);
};


