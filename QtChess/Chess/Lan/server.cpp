#include "Server.h"

#include <iostream>

namespace NetworkArdic {

Server::Server(QObject* parent , quint16 port): QTcpServer(parent)
{
//tcpServer = new QTcpServer(this);
connect(this, SIGNAL(newConnection()),this, SLOT(acceptConnection()));

if(!this->listen(QHostAddress::Any, port ))
{
    std::cout<<"unable to start server\n"<<this->errorString().toUtf8().constData()<<std::endl;
}
else { 
	std::cout<<"server started\n";
	
}
	
}

Server::~Server()
{
close();
}

void Server::acceptConnection()
    {
	//std::cout<<"new connection!\n";
	// �������� ������ ������, ������� ������ ������ ����
	client = nextPendingConnection();
    connect(client, SIGNAL(readyRead()), this, SLOT(startRead()));
    connect(client, SIGNAL(disconnected()), this, SLOT(disconnected()));
	SendData("Privet zemlyanin");
	emit RecieveMessage("New connection!");
    }

void Server::startRead() 
    { 
        while(client->canReadLine())
        {
            QString line = QString::fromUtf8(client->readLine()).trimmed();
            //qDebug() << "Client :" << line;

            emit RecieveMessage(line);
        }

    }

void Server::disconnected()
    {

        //qDebug() << "Client disconnected:" << client->peerAddress().toString();
		emit RecieveMessage("Client Disconnected");
        client->write(QString("I wish you didn't leave ):\n").toUtf8());

    }

void Server::SendData(QString data)
    {
        if(!data.isEmpty())
        {
            //client->write((data +"\n").toUtf8());
            //client->flush();
        }
    }

}
