#include "client.h"
#include <QHostAddress>

#include <iostream>

namespace NetworkArdic{}

Client::Client(const QString& add, quint16 port, QObject * obj)
	: QObject(obj)
{
	//������� �����
	socket = new QTcpSocket(this);
	//��������� ����������
	socket->connectToHost(add, port);
	//������ �������� �������� � ������
    connect(socket, SIGNAL(readyRead()),  SLOT(ReadData()));
	connect(socket, SIGNAL(connected()),  SLOT(slotConnected()));
	connect(socket, SIGNAL(error(QAbstractSocket::SocketError)),
            this,         SLOT(slotError(QAbstractSocket::SocketError))
           );
}

Client::~Client()
{
	socket->close();
    delete socket;
}

void Client::SendData(QString data)
    {
        if(!data.isEmpty())
        {
			socket->write((data +"\n").toUtf8());
			socket->flush();
        }
    }

void Client::ReadData()
    {
	QDataStream in(socket);
	in.setVersion(QDataStream::Qt_5_1);
        while(socket->canReadLine())
        {

            QString line = QString::fromUtf8(socket->readLine()).trimmed();
			emit RecieveMessage(line);
            qDebug() << line;
        }
    }

void Client::slotConnected()
    {
        //socket->write(QString("Server connection has been made (: \n").toUtf8());
    }

void Client::slotError(QAbstractSocket::SocketError err)
{
    QString strError = 
        "Error: " + (err == QAbstractSocket::HostNotFoundError ? 
                     "The host was not found." :
                     err == QAbstractSocket::RemoteHostClosedError ? 
                     "The remote host is closed." :
                     err == QAbstractSocket::ConnectionRefusedError ? 
                     "The connection was refused." :
                     QString(socket->errorString())
                    );
    std::cout<<strError.toUtf8().constData()<<std::endl;
}

int Client::status() {
	return socket->state();
}

