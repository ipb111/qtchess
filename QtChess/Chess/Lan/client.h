#ifndef CLIENT_H
#define CLIENT_H

#include <QtNetwork>
#include <QObject>
#include <QtNetwork/QTcpSocket>

namespace NetworkArdic {}

class Client : public QObject
{
	Q_OBJECT

public:
	Client(const QString& add, quint16 port, QObject * obj = 0);
	void SendData(QString data);
    virtual ~Client();
	int status();
	QString err;
signals:
	void RecieveMessage(QString);
private slots:
    void ReadData();
    void slotConnected();
	void slotError(QAbstractSocket::SocketError);
private:
	QTcpSocket *socket;
};

#endif // CLIENT_H
