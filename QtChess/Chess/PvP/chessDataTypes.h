#ifndef CHESSDATATYPES_H
#define CHESSDATATYPES_H

typedef unsigned char uchar;
typedef unsigned int uint;

namespace data 
{
//-------------------------------------------------------------------
// �����, ����������� ���
//-------------------------------------------------------------------
class move
{
public:
    move() : n1(0), n2(0) { }
    uchar n1;	// ������ �������
    uchar n2;	// ����� �������
};
}
//-------------------------------------------------------------------
// ��������� ��������� ������
//-------------------------------------------------------------------

typedef enum : uchar		// ���� ������
{
	BLACK = 0,
	WHITE = 1
} PieceColor;

typedef enum : uchar 	// ��� ������
{
	KING	= 1,		// ������
	QUEEN	= 2,		// �����
	BISHOP	= 3,		// ����
	KNIGHT	= 4,		// ����
	ROOK	= 5,		// �����
	PAWN	= 6,		// �����
    NONE	= 0			// �����
}	PieceType;

typedef enum : uchar	// ��������� ������
{
	KING_COST			= 100,	
	QUEEN_COST			= 9,		
	ROOK_COST			= 5,		
	BISHOP_KNIGHT_COST	= 3,		
	PAWN_COST			= 1,
	FREE_COST			= 0
} PieceCost;

typedef enum : int
{
    NOTHING = 0,
    SHAH = 1,
    MAT=2,
    PAT=3
} KingStatus;

class Piece
{
public:
    Piece();
    Piece(const Piece& o);
    Piece(uchar coord, uchar piece, uchar color, uchar cost);
	~Piece() {}
public:

    void setPiece(uchar coord, uchar piece, uchar color);

    uchar getCoord() const;
    uchar getPiece() const;
    uchar getColor() const;
    uchar getCost() const;
	bool isMove() const; 
	bool isEnable() const;
    void setCoord(uchar coord);
    void setPiece(uchar piece);
    void setColor(uchar color);
    void setCost(uchar cost);
	void setIsEnable(bool isenable);
	void setIsMove(bool ismove);
private:
	void determineCost();
private:
    uchar coord_;
    uchar piece_;	// = PieceType
    uchar color_;	// = PieceColor
    uchar cost_;		// = PieceCost
	bool isMove_;
	bool isEnable_;	
};

inline uchar Piece::getCoord() const
{
	return coord_;
}

inline uchar Piece::getPiece() const
{
	return piece_;
}

inline uchar Piece::getColor() const
{
	return color_;
}

inline uchar Piece::getCost() const
{
	return cost_;
}

inline bool Piece::isMove() const
{
	return isMove_;
}

inline bool Piece::isEnable() const
{
	return isEnable_;
}

inline void Piece::setCoord(uchar coord)
{
	coord_ = coord;
}

inline void Piece::setPiece(uchar piece)
{
	piece_ = piece;
}

inline void Piece::setColor(uchar color)
{
	color_ = color;
}

inline void Piece::setCost(uchar cost)
{
	cost_ = cost;
}
 
inline void Piece::setIsEnable(bool isenable)
{
	isEnable_ = isenable;
}

inline void Piece::setIsMove(bool ismove)
{
	isMove_ = ismove;
}

#endif
