
#include "chessPartiya.h"
#include <QDebug>

Piece pieces[32]; 
// ��������� ����� � �� ���������� ������������ ��� ������ ����� ������

// ��������� ����� � �� ���������� ������������ ��� ������ ����� ������

Partiya::Partiya(QObject* parent) : QObject(parent)
{}

 Partiya::~Partiya()
 {

 }

void Partiya::startPartiyaInitiation()
{
    partiyaTipe=PVPLoc_Pvc;
  for(int i=0;i<32;i++)
  {   //������ ����
	  if(i<=15)
             {
				 pieces[i].setColor(BLACK);}
       else
		   pieces[i].setColor(WHITE);
	  //������ ��� ������ � � ��������
       if(i==0 || i==7 || i==24 || i==31)
	   { pieces[i].setPiece(ROOK);
	   pieces[i].setCost(5);}
	   if(i==1 || i==6|| i==25 || i==30)
	   { pieces[i].setPiece(KNIGHT);
	         pieces[i].setCost(3);}
	   if(i==2 || i==5 || i==26 || i==29)
	   {pieces[i].setPiece(BISHOP);
	   pieces[i].setCost(3);}
	   if(i==28 || i==4)
	   {pieces[i].setPiece(KING);
	          pieces[i].setCost(9);}
	   if(i==27 || i==3)
	   { pieces[i].setPiece(QUEEN);
	   pieces[i].setCost(100);}
	   if((i>=8 && i<= 15) || (i>=16 && i<=23))
	   {pieces[i].setPiece(PAWN);
	    pieces[i].setCost(1);}
	   //������ ��������� ����������
	    if(i<=15)
			pieces[i].setCoord(i);
       else
		   pieces[i].setCoord(i+32);
      // ������ ������� ��� ���
		pieces[i].setIsEnable(true);
	  // ������ ��������� ��� ���
		pieces[i].setIsMove(false);

		
	 
     }
     //������ ��������� ������������ ����� �� �����
    for(int i=0;i<64;i++)
         Partiya::map[i]=-1;
         //int y=0;
  for(int i=0;i<64;i++)
	    if(i<=15)
			map[i]=i;
       else if(i>=48)
		   map[i]=(i-32);

	   else
		   map[i]=-1;

	for(int i=0;i<32;i++)
		{
                enableTurnArray[i]=-1;
                //y= enableTurnArray[i];
                }
	whichTurn=WHITE;
	schot[0] = 0;
	schot[1] = 0;

	KingStarusCode[BLACK]=NOTHING;
    KingStarusCode[WHITE]=NOTHING;
}

void Partiya::NewLanPartiyaInitiation(int LanColor)
{
    partiyaTipe=PVPVLan;
  myLanColor=LanColor;	
  for(int i=0;i<32;i++)
  {   //������ ����
	  if(i<=15)
             {
				 pieces[i].setColor(BLACK);}
       else
		   pieces[i].setColor(WHITE);
	  //������ ��� ������ � � ��������
       if(i==0 || i==7 || i==24 || i==31)
	   { pieces[i].setPiece(ROOK);
	   pieces[i].setCost(5);}
	   if(i==1 || i==6|| i==25 || i==30)
	   { pieces[i].setPiece(KNIGHT);
	         pieces[i].setCost(3);}
	   if(i==2 || i==5 || i==26 || i==29)
	   {pieces[i].setPiece(BISHOP);
	   pieces[i].setCost(3);}
	   if(i==28 || i==4)
	   {pieces[i].setPiece(KING);
	          pieces[i].setCost(9);}
	   if(i==27 || i==3)
	   { pieces[i].setPiece(QUEEN);
	   pieces[i].setCost(100);}
	   if((i>=8 && i<= 15) || (i>=16 && i<=23))
	   {pieces[i].setPiece(PAWN);
	    pieces[i].setCost(1);}
	   //������ ��������� ����������
	    if(i<=15)
			pieces[i].setCoord(i);
       else
		   pieces[i].setCoord(i+32);
      // ������ ������� ��� ���
		pieces[i].setIsEnable(true);
	  // ������ ��������� ��� ���
		pieces[i].setIsMove(false);

		
	 
     }
     //������ ��������� ������������ ����� �� �����
    for(int i=0;i<64;i++)
         Partiya::map[i]=-1;
         //int y=0;
  for(int i=0;i<64;i++)
	    if(i<=15)
			map[i]=i;
       else if(i>=48)
		   map[i]=(i-32);

	   else
		   map[i]=-1;

	for(int i=0;i<32;i++)
		{
                enableTurnArray[i]=-1;
                //y= enableTurnArray[i];
                }
	whichTurn=WHITE;
	schot[0] = 0;
	schot[1] = 0;

	KingStarusCode[BLACK]=NOTHING;
    KingStarusCode[WHITE]=NOTHING;
}

// ������������� ������ �� ����������
void Partiya::loadPartiaInitiation(Partiya a)
{
	partiyaTipe=a.partiyaTipe;
for(int i=0;i<32;i++)
  {
	  pieces[i].setColor(a.pieces[i].getColor());
	  pieces[i].setCoord(a.pieces[i].getCoord());
	  pieces[i].setPiece(a.pieces[i].getPiece());
	  pieces[i].setCost(a.pieces[i].getCost());
	  pieces[i].setIsEnable(a.pieces[i].isEnable());
	  pieces[i].setIsMove(a.pieces[i].isMove());
  }
for(int i=0;i<64;i++)
  {
	  map[i]=a.map[i];
  }
for(int i=0;i<30;i++)
		enableTurnArray[i]=-1;
whichTurn=a.whichTurn;
schot[0]=a.schot[0];
schot[1]=a.schot[1];
KingStarusCode[BLACK]=a.KingStarusCode[BLACK];
KingStarusCode[WHITE]=KingStarusCode[WHITE];
}




   // ��������� ������� ��������� ����� ��� ������ ������
int * Partiya::isEnableTurnArray(int n, int   enableTurnArray[])
{
		int k=0,l=0;
		int desk[8][8];
		int etr1[32][2];
		 enableTurnArraySize=0;
		int i=0,r;
		
	    for(i=0;i<64;i++)
		{
		desk[k][l]=map[i];
		 l++;
 		if(i==7 ||  i==15 || i==23 || i==31 || i==39 || i==47 || i==55 || i==63)
			{ k++;
		       l=0;}
		}
		int m=n%8;//������� �� �������
		int j=n/8;

		//-�����-
		if(pieces[(int)desk[j][m]].getPiece()==ROOK)
		{int p=0;
		i=0;
			    i=m;
				for(i=m;i<8;i++)
				{ if(i!=m)
						{etr1[p][0]=i;// No stolba
			        	etr1[p][1]=j;//No stroki
						p++;
				enableTurnArraySize++;}
						if(desk[j][i+1]!=-1 && (i+1)<8 && pieces[(int)desk[j][i+1]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[j][i]!=-1 && i!=(m+1) && pieces[(int)desk[j][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
				}
				i=m;
				for(i=m;i>=0;i--)
					{if(i!=m)
						{ etr1[p][0]=i;// No stolba
			        	etr1[p][1]=j;//No stroki
						p++; 
						enableTurnArraySize++;
				}
						if(desk[j][i-1]!=-1 && (i-1)>=0 && pieces[(int)desk[j][i-1]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[j][i]!=-1 && i!=(m-1) && pieces[(int)desk[j][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
						
				}
				
			i=j;
                for(i=j;i>=0;i--)
				{if(i!=j)
						{ etr1[p][0]=m;// No stolba
			        	etr1[p][1]=i;//No stroki
						p++;
						enableTurnArraySize++;
				}
						if(desk[i-1][m]!=-1 && (i-1)>=0 && pieces[(int)desk[i-1][m]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[i][m]!=-1  && pieces[(int)desk[i][m]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
				}
				i=j;
                for(i=j;i<8;i++)
				{if(i!=j)
						{ etr1[p][0]=m;// No stolba
			        	etr1[p][1]=i;//No stroki
						p++;
						enableTurnArraySize++;
				}
						if(desk[i+1][m]!=-1  && (i+1)<8 && pieces[(int)desk[i+1][m]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[i][m]!=-1  && i!=j && pieces[(int)desk[i][m]].getColor()!=pieces[(int)desk[j][m]].getColor() )
							break;
				}

		}

		//-�����-
		
			if(pieces[(int)desk[j][m]].getPiece()==PAWN)
			{int p=0;
				if(pieces[(int)desk[j][m]].getColor()==BLACK)
				{
				if(desk[j+1][m]==-1 && j<=8)
				 {
				 etr1[p][0]=m;// No stolba
			        	etr1[p][1]=j+1;//No stroki
						p++;
						enableTurnArraySize++;
				 }
				if(desk[j+2][m]==-1 && desk[j+1][m]==-1 && pieces[(int)desk[j][m]].isMove()==false   )
				 {
				 etr1[p][0]=m;// No stolba
			        	etr1[p][1]=j+2;//No stroki
						p++;
						enableTurnArraySize++;
				 }
				if(desk[j+1][m+1]!=-1 && pieces[(int)desk[j+1][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() && m>=0 && j<8)
				 {
				 etr1[p][0]=m+1;// No stolba
			        	etr1[p][1]=j+1;//No stroki
						p++;
						enableTurnArraySize++;
				 }
				if(desk[j+1][m-1]!=-1 && pieces[(int)desk[j-1][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor()  && m<8 && j>=0)
				 {
				 etr1[p][0]=m-1;// No stolba
			        	etr1[p][1]=j+1;//No stroki
						p++;
						enableTurnArraySize++;
				 }

				}

			

			
				if(pieces[(int)desk[j][m]].getColor()==WHITE)
				{
				if(desk[j-1][m]==-1 && j>=0)
				 {
				 etr1[p][0]=m;// No stolba
			        	etr1[p][1]=j-1;//No stroki
						p++;
						enableTurnArraySize++;
				 }
				if(desk[j-2][m]==-1 && desk[j-1][m]==-1 && pieces[(int)desk[j][m]].isMove()==false  )
				 {
				 etr1[p][0]=m;// No stolba
			        	etr1[p][1]=j-2;//No stroki
						p++;
						enableTurnArraySize++;
				 }
				if(desk[j-1][m+1]!=-1 && pieces[(int)desk[j-1][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() && j>=0 && m<8)
				 {
				 etr1[p][0]=m+1;// No stolba
			        	etr1[p][1]=j-1;//No stroki
						p++;
						enableTurnArraySize++;
				 }
				if(desk[j-1][m-1]!=-1 && pieces[(int)desk[j-1][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor()  && j>=0 && m>=0)
				 {
				 etr1[p][0]=m-1;// No stolba
			        	etr1[p][1]=j-1;//No stroki
						p++;
						enableTurnArraySize++;
				 }

				}
		  
		}

		//-������-
			if(pieces[(int)desk[j][m]].getPiece()==BISHOP)
		{int p=0;
		i=0;
		r=0;
			    i=m;
				r=j;
				for(r=j;r<8;r++)
				{
				  if((i)!=m && r!=j)
						{etr1[p][0]=i;// No stolba
			        	etr1[p][1]=r;//No stroki
						p++;
				        enableTurnArraySize++;
				        }
			           i++;
					   if ((i)>=8 || (r+1)>=8) 
					      break;
				
				if(desk[r+1][i]!=-1  && pieces[(int)desk[r+1][i]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[r][i-1]!=-1 && pieces[(int)desk[r][i-1]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
				}
				i=m;
				r=j;
				for(r=j;r<8;r--)
				{
				 if(i!=m && j!=m)
						{etr1[p][0]=i;// No stolba
			        	etr1[p][1]=r;//No stroki
						p++;
				enableTurnArraySize++;}
				i--;
			           
				
				if ((i)<0 || (r-1)<0 || r<0) 
					      break;
				 if (  desk[r-1][i]!=-1  && pieces[(int)desk[r-1][i]].getColor()==pieces[(int)desk[j][m]].getColor() ) 
							break;
						if(desk[r][i+1]!=-1  && pieces[(int)desk[r][i+1]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
				}
				
				i=m;
				r=j;
					for(r=j;r<8;r++)
				{
				 if(i!=m && j!=m)
						{etr1[p][0]=i;// No stolba
			        	etr1[p][1]=r;//No stroki
						p++;
				enableTurnArraySize++;}
				i--;
			           
			
				if ((i)<0 || (r+1)>=8) 
					      break;
				if(desk[r+1][i]!=-1  && pieces[(int)desk[r+1][i]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[r][i+1]!=-1 && pieces[(int)desk[r][i+1]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
				}
				i=m;
				r=j;
				for(r=j;r<8;r--)
				{
				 if(i!=m && j!=m)
						{etr1[p][0]=i;// No stolba
			        	etr1[p][1]=r;//No stroki
						p++;
				enableTurnArraySize++;}
				i++;
			           
				
				if ((i+1)>=8 || (r-1)<0) 
					      break;
				if(desk[r-1][i]!=-1  && pieces[(int)desk[r-1][i]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[r][i-1]!=-1  && pieces[(int)desk[r][i-1]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
				}
				

		}

			//-��������-
			if(pieces[(int)desk[j][m]].getPiece()==QUEEN)
		{int p=0;
		i=0;
		r=0;
		enableTurnArraySize=0;
			    i=m;
				r=j;
				for(r=j;r<8;r++)
				{
				  if((i)!=m && r!=j)
						{etr1[p][0]=i;// No stolba
			        	etr1[p][1]=r;//No stroki
						p++;
				        enableTurnArraySize++;
				        }
			           i++;
					   if ((i)>=8 || (r+1)>=8) 
					      break;
				
				if(desk[r+1][i]!=-1  && pieces[(int)desk[r+1][i]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[r][i-1]!=-1 && pieces[(int)desk[r][i-1]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
				}
				i=m;
				r=j;
				for(r=j;r<8;r--)
				{
				 if(i!=m && j!=m)
						{etr1[p][0]=i;// No stolba
			        	etr1[p][1]=r;//No stroki
						p++;
				enableTurnArraySize++;}
				i--;
			           
				
				if ((i)<0 || (r-1)<0) 
					      break;
				 if (  desk[r-1][i]!=-1  && pieces[(int)desk[r-1][i]].getColor()==pieces[(int)desk[j][m]].getColor() ) 
							break;
						if(desk[r][i+1]!=-1  && pieces[(int)desk[r][i+1]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
				}
				
				i=m;
				r=j;
					for(r=j;r<8;r++)
				{
				 if(i!=m && j!=m)
						{etr1[p][0]=i;// No stolba
			        	etr1[p][1]=r;//No stroki
						p++;
				enableTurnArraySize++;}
				i--;
			           
			
				if ((i)<0 || (r+1)>=8) 
					      break;
				if(desk[r+1][i]!=-1  && pieces[(int)desk[r+1][i]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[r][i+1]!=-1 && pieces[(int)desk[r][i+1]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
				}
				i=m;
				r=j;
				for(r=j;r<8;r--)
				{
				 if(i!=m && j!=m)
						{etr1[p][0]=i;// No stolba
			        	etr1[p][1]=r;//No stroki
						p++;
				enableTurnArraySize++;}
				i++;
			           
				
				if ((i)>=8 || (r-1)<0) 
					      break;
				if(desk[r-1][i]!=-1  && pieces[(int)desk[r-1][i]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[r][i-1]!=-1  && pieces[(int)desk[r][i-1]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
				}

				//-������-
				
		
			    i=m;
				for(i=m;i<8;i++)
				{ if(i!=m)
						{etr1[p][0]=i;// No stolba
			        	etr1[p][1]=j;//No stroki
						p++;
				enableTurnArraySize++;}
						if(desk[j][i+1]!=-1 && (i+1)<8 && pieces[(int)desk[j][i+1]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[j][i]!=-1 && i!=(m+1) && pieces[(int)desk[j][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
				}
				i=m;
				for(i=m;i>=0;i--)
					{if(i!=m)
						{ etr1[p][0]=i;// No stolba
			        	etr1[p][1]=j;//No stroki
						p++; 
						enableTurnArraySize++;
				}
						if(desk[j][i-1]!=-1 && (i-1)>=0 && pieces[(int)desk[j][i-1]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[j][i]!=-1 && i!=(m-1) && pieces[(int)desk[j][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
						
				}
				
			i=j;
                for(i=j;i>=0;i--)
				{if(i!=j)
						{ etr1[p][0]=m;// No stolba
			        	etr1[p][1]=i;//No stroki
						p++;
						enableTurnArraySize++;
				}
						if(desk[i-1][m]!=-1 && (i-1)>=0 && pieces[(int)desk[i-1][m]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[i][m]!=-1  && pieces[(int)desk[i][m]].getColor()!=pieces[(int)desk[j][m]].getColor())
							break;
				}
				i=j;
                for(i=j;i<8;i++)
				{if(i!=j)
						{ etr1[p][0]=m;// No stolba
			        	etr1[p][1]=i;//No stroki
						p++;
						enableTurnArraySize++;
				}
						if(desk[i+1][m]!=-1  && (i+1)<8 && pieces[(int)desk[i+1][m]].getColor()==pieces[(int)desk[j][m]].getColor() )
							break;
						if(desk[i][m]!=-1  && i!=j && pieces[(int)desk[i][m]].getColor()!=pieces[(int)desk[j][m]].getColor() )
							break;
				}
				
			

		}

		//-������-
		if(pieces[(int)desk[j][m]].getPiece()==KING)
		{int p=0;
		//������
			if((m+1)<8 &&(desk[j][m+1]==-1 ||(desk[j][m+1]!=-1 &&  pieces[(int)desk[j][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
				{ etr1[p][0]=m+1;// No stolba
			        	etr1[p][1]=j;//No stroki
						p++;
						enableTurnArraySize++;
				}

			if((m-1)>=0 &&(desk[j][m-1]==-1 ||(desk[j][m-1]!=-1 &&  pieces[(int)desk[j][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
				{ etr1[p][0]=m-1;// No stolba
			        	etr1[p][1]=j;//No stroki
						p++;
						enableTurnArraySize++;
				}

			if((j+1)<8 &&(desk[j+1][m]==-1 ||(desk[j+1][m]!=-1 &&  pieces[(int)desk[j+1][m]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
				{ etr1[p][0]=m;// No stolba
			        	etr1[p][1]=j+1;//No stroki
						p++;
						enableTurnArraySize++;
				}

			if((j-1)>=0 &&(desk[j-1][m]==-1 ||(desk[j-1][m]!=-1 &&  pieces[(int)desk[j-1][m]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
				{ etr1[p][0]=m;// No stolba
			        	etr1[p][1]=j-1;//No stroki
						p++;
						enableTurnArraySize++;
				}
			//���������
			if((m+3)<8)
			if(pieces[(int)desk[j][m]].isMove()==false &&  pieces[(int)desk[j][m+3]].getPiece()==ROOK && pieces[(int)desk[j][m+3]].isMove()==false)
				if(desk[j][m-1]==-1 && desk[j][m-2]==-1)
					{etr1[p][0]=m+2;// No stolba
			        	etr1[p][1]=j;//No stroki
						p++;
						enableTurnArraySize++;
			}

			if((m-4)>=0)
			if(pieces[(int)desk[j][m]].isMove()==false &&  pieces[(int)desk[j][m-4]].getPiece()==ROOK && pieces[(int)desk[j][m-4]].isMove()==false)
				if(desk[j][m-1]==-1 && desk[j][m-2]==-1)
					{etr1[p][0]=m-3;// No stolba
			        	etr1[p][1]=j;//No stroki
						p++;
						enableTurnArraySize++;
			}
			//���������
			if((j-1)>=0 && (m-1)>=0 &&(desk[j-1][m-1]==-1 ||(desk[j-1][m-1]!=-1 &&  pieces[(int)desk[j-1][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
				{ etr1[p][0]=m-1;// No stolba
			        	etr1[p][1]=j-1;//No stroki
						p++;
						enableTurnArraySize++;
				}

			if((j-1)>=0 && (m+1)<8 &&(desk[j-1][m+1]==-1 ||(desk[j-1][m+1]!=-1 &&  pieces[(int)desk[j-1][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
				{ etr1[p][0]=m+1;// No stolba
			        	etr1[p][1]=j-1;//No stroki
						p++;
						enableTurnArraySize++;
				}

			
			if((j+1)<8 && (m+1)<8 &&(desk[j+1][m+1]==-1 ||(desk[j+1][m+1]!=-1 &&  pieces[(int)desk[j+1][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
				{ etr1[p][0]=m+1;// No stolba
			        	etr1[p][1]=j+1;//No stroki
						p++;
						enableTurnArraySize++;
				}

			if((j+1)<8 && (m-1)>=0 &&(desk[j+1][m-1]==-1 ||(desk[j+1][m-1]!=-1 &&  pieces[(int)desk[j+1][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
				{ etr1[p][0]=m-1;// No stolba
			        	etr1[p][1]=j+1;//No stroki
						p++;
						enableTurnArraySize++;
				}
		}
			//-����-

			if(pieces[(int)desk[j][m]].getPiece()==KNIGHT)
		{int p=0;
           if((m+2)<8)
			   { if((j+1)<8  &&(desk[j+1][m+2]==-1 ||(desk[j+1][m+2]!=-1 &&  pieces[(int)desk[j+1][m+2]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m+2;// No stolba
			        	etr1[p][1]=j+1;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   if((j-1)>=0  &&(desk[j-1][m+2]==-1 ||(desk[j-1][m+2]!=-1 &&  pieces[(int)desk[j-1][m+2]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m+2;// No stolba
			        	etr1[p][1]=j-1;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   
		   }

		   if((m-2)>=0)
			   { if((j+1)<8  &&(desk[j+1][m-2]==-1 ||(desk[j+1][m-2]!=-1 &&  pieces[(int)desk[j+1][m-2]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m-2;// No stolba
			        	etr1[p][1]=j+1;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   if((j-1)>=0  &&(desk[j-1][m-2]==-1 ||(desk[j-1][m-2]!=-1 &&  pieces[(int)desk[j-1][m-2]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m-2;// No stolba
			        	etr1[p][1]=j-1;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   
		   }

		   if((j+2)<8)
			   { if((m+1)<8  &&(desk[j+2][m+1]==-1 ||(desk[j+2][m+1]!=-1 &&  pieces[(int)desk[j+2][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m+1;// No stolba
			        	etr1[p][1]=j+2;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   if((m-1)>=0  &&(desk[j+2][m-1]==-1 ||(desk[j+2][m-1]!=-1 &&  pieces[(int)desk[j+2][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m-1;// No stolba
			        	etr1[p][1]=j+2;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   
		   }

		   if((j-2)>=0)
			   { if((m+1)<8  &&(desk[j-2][m+1]==-1 ||(desk[j-2][m+1]!=-1 &&  pieces[(int)desk[j-2][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m+1;// No stolba
			        	etr1[p][1]=j-2;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   if((m-1)>=0  &&(desk[j-2][m-1]==-1 ||(desk[j-2][m-1]!=-1 &&  pieces[(int)desk[j-2][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m-1;// No stolba
			        	etr1[p][1]=j-2;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   
		   }
}



		
		


	// �������� �����������
		int	p=0;
		while(p<enableTurnArraySize)
		{
n = etr1[p][1] * 8 + etr1[p][0];
enableTurnArray[p]=n;
p++;}
		for(p=enableTurnArraySize;p<29;p++)
			enableTurnArray[p]=-1;
return enableTurnArray;
		
} 






// �������� �� ����������� ����
bool Partiya::isThisTurnEnable(int enableTurnArray[], int n)
{ 
 for(int i=0;i<29;i++)
	    if (enableTurnArray[i]==n)
			return true;
return false;
 	   		
}

// n- ��, ��� ����
// n2- ��� ����
int Partiya::eatChess(int n, int n2)
{
	if (pieces[ map[n]].getColor()!=pieces[ map[n2]].getColor())
	{
        if(map[n]!=-1)
        {
            eatenChess=pieces[map[n]];
        }
        else
        {
            eatenChess.setPiece(PieceType::NONE);
        }
        pieces[map[n]].setIsEnable(false);
		
		return 1;
	}
	return 0;
}

void Partiya::goTurn(Piece & b,  int n2)
{
	isEnableTurnArray(b.getCoord(),enableTurnArray);
	
		if(b.getPiece()==KING && (b.getCoord()+2  || n2==b.getCoord()-3) && b.isMove()==false )
		{
			if(n2==b.getCoord()+2 && pieces[(int) map[n2+1]].getPiece()==ROOK && pieces[(int) map[n2+1]].isMove()==false)
		   {
			   map[n2]=map[b.getCoord()];
			    map[b.getCoord()]=-1;
		   b.setCoord(n2);
		   b.setIsMove(true);
		   pieces[(int)map[n2+1]].setIsMove(true);
		   pieces[(int)map[n2+1]].setCoord(n2-1);
		   map[n2-1]=map[n2+1];
		   map[n2+1]=-1;
		  }
		 
			if((n2==pieces[(int) map[n2]].getCoord()-3)  && pieces[(int) map[n2-1]].getPiece()==ROOK && pieces[(int) map[n2-1]].isMove()==false)
		  {
          
		   map[n2]=map[b.getCoord()];
		    map[b.getCoord()]=-1;
		  b.setCoord(n2);
		   b.setIsMove(true);
		   pieces[(int)map[n2-1]].setIsMove(true);
		   pieces[(int)map[n2-1]].setCoord(n2-1);;
		  map[n2+1]=map[n2-1];
		  map[n2-1]=-1;
		  }
		  }
		else
		{  if(map[n2]!=-1 && pieces[(int)map[n2]].getColor()!=b.getColor())
		   if( eatChess(n2,b.getCoord()) == 1)
	         
               {
				   schot[whichTurn]=schot[whichTurn]+pieces[map[n2]].getCost(); 
				   map[n2]=map[b.getCoord()];
					map[b.getCoord()]=-1;
					b.setCoord(n2);
					 // ���������� ����� ������
			   }

		 if(map[n2]==-1)
			 { map[n2]=map[b.getCoord()];
	         map[b.getCoord()]=-1;
			 b.setCoord(n2);}

		  }
	b.setIsMove(true);

}

void Partiya::newTurn()
{
	if (whichTurn==BLACK)
		whichTurn=WHITE;
	else
		whichTurn=BLACK;

	num1=-1;

    bool colorBlack = whichTurn == BLACK;
    emit switchPlayer(colorBlack, getSchot(colorBlack ? BLACK : WHITE));
}


bool Partiya::takeCoord(int n, int **entarray, int &arraySize)
{
	int i;
	int *ptr = 0;
	if ((num1==-1 ||  whichTurn==pieces[ map[n]].getColor()  )&& map[n]!=-1 )
        {if(partiyaTipe==PVPVLan && whichTurn!=(uint)myLanColor)
				{
					errorCode=7;// ������ �� ��� ��� (��� LAN)
					return false;
				}
			if(whichTurn==pieces[map[n]].getColor() && map[n]!=-1)
				 { num1=map[n];
					isEnableTurnArray(n, enableTurnArray);
					arraySize = enableTurnArraySize;
					*entarray = new int [enableTurnArraySize];
					ptr = *entarray;
					for(i=0;i<enableTurnArraySize;i++)
					{
						*ptr=enableTurnArray[i];
						++ptr;
					}

					return true;
				 }
			 else 
				 if(map[n]==-1)
				  { 
						errorCode=-3;//������ � ���, ��� ������� ������ ����
						return false;
					 }
				 else
					 {
						  errorCode=-4; //������ � ���, ��� ������� ������ ��������� ����
						return false;
					 }
	   
	}
	else
	{
		if(errorCode==-6 )
			isEnableTurnArray(pieces[num1].getCoord(),enableTurnArray);
		if(isThisTurnEnable( enableTurnArray,  n))
			{
				move1.n1=pieces[num1].getCoord();
                jmChess=pieces[num1];
				move1.n2=n;

				goTurn(pieces[num1],n);//���������� ����

				isShahMatPat();
				
			
			newTurn();
			return true;
			
	      }
		else
		{
			errorCode=-6;//������ � ���, ��� ��� ����������
			return false;
		}
	}
				
}



bool Partiya::isShah(int color)
{
	int ff;
	if (color == BLACK)
        ff=3;
	else 
        ff=27;
	return uSMPCheck(ff,color);

}

bool Partiya::uMPCheck(int etr4[], int ds)
{
	int oo=0,i;
	for(i=0;i<29;i++)
		if(etr4[i]!=-1)
			oo++;
	i=0;
	for(i=0;i<29;i++)
	{
		if(etr4[i]!=-1)
		if(uSMPCheck(etr4[i],ds))
			oo--;
		if(oo==0)
			return true;
	}

	if(oo!=0)
		return false;

    return true;
}


bool Partiya::isPat(int ds)
{
	int oo=0;
	int etr5[16][29];
	for(int i=0;i<16;i++)
		for(int j=0;j<16;j++)
			etr5[i][j]=-1;
	for(int i=0;i<16;i++)
		{
			if(ds==0)
				isEnableTurnArray( i, etr5[i]);
			else
				isEnableTurnArray( i+48, etr5[i]);
		}
	for(int i=0;i<16;i++)
		{if(uMPCheck(etr5[i],ds)==false)
			oo++;
		 if(oo>0)
			 return false;
		}
	if (oo==0)
		return true;

    return false;
}


void Partiya::isShahMatPat()
{
	bool nnn;
	KingStarusCode[0]=0;
	KingStarusCode[1]=0;
	int ds;// ????
	int kingEnableTurnes[2][29];//????????? ???? ??????

	ds=((whichTurn-1)*(whichTurn-1));
	// ????????? ?? ???
		if(isShah(ds))
        {   	// ???? ??? ????, ????????? ?? ???
                /*nnn=(uMPCheck(kingEnableTurnes[ds],ds));
			if(nnn==true)
			  	KingStarusCode[ds]=MAT;
            else*/
            KingStarusCode[ds]=SHAH;
            emit check();
        }
        else    // ???? ???? ???, ????????? ?? ???
            KingStarusCode[ds]=NOTHING;

		
	qDebug()<<"Color = "<<ds<<" Status = "<<KingStarusCode[ds];
	qDebug()<<"Color = "<<!ds<<" Status = "<<KingStarusCode[!ds];
	
}


/*int * Partiya::isNSPArray(int n, int   NSPArray[])
{
        int k=0,l=0;
        int desk[8][8];
        int etr1[32][2];
        int NSPSize=0;
        int i=0,r;

        for(i=0;i<64;i++)
        {
        desk[k][l]=map[i];
         l++;
        if(i==7 ||  i==15 || i==23 || i==31 || i==39 || i==47 || i==55 || i==63)
            { k++;
               l=0;}
        }
        int m=n%8;//������� �� �������
        int j=n/8;




        int p=0;
        i=0;
        r=0;
        NSPSize=0;
                i=m;
                r=j;
                for(r=j;r<8;r++)
                {
                  if((i)!=m && r!=j)
                        {etr1[p][0]=i;// No stolba
                        etr1[p][1]=r;//No stroki
                        p++;
                        NSPSize++;
                        }
                       i++;
                       if ((i)>=8 || (r+1)>=8)
                          break;

                if(desk[r+1][i]!=-1  && pieces[(int)desk[r+1][i]].getColor()==pieces[(int)desk[j][m]].getColor() )
                            break;
                if(desk[r][i-1]!=-1 && pieces[(int)desk[r][i-1]].getColor()!=pieces[(int)desk[j][m]].getColor())
                            break;
                }
                i=m;
                r=j;
                for(r=j;r<8;r--)
                {
                 if(i!=m && j!=m && pieces[(int)desk[r][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
                        {etr1[p][0]=i;// No stolba
                        etr1[p][1]=r;//No stroki
                        p++;
                        }
                i--;


                if ((i)<0 || (r-1)<0)
                          break;
                 if (  desk[r-1][i]!=-1  && pieces[(int)desk[r-1][i]].getColor()==pieces[(int)desk[j][m]].getColor() )
                            break;
                        if(desk[r][i+1]!=-1  && pieces[(int)desk[r][i+1]].getColor()!=pieces[(int)desk[j][m]].getColor())
                            break;
                }

                i=m;
                r=j;
                    for(r=j;r<8;r++)
                {
                 if(i!=m && j!=m)
                        {etr1[p][0]=i;// No stolba
                        etr1[p][1]=r;//No stroki
                        p++;
                NSPSize++;}
                i--;


                if ((i)<0 || (r+1)>=8)
                          break;
                if(desk[r+1][i]!=-1  && pieces[(int)desk[r+1][i]].getColor()==pieces[(int)desk[j][m]].getColor() )
                            break;
                        if(desk[r][i+1]!=-1 && pieces[(int)desk[r][i+1]].getColor()!=pieces[(int)desk[j][m]].getColor())
                            break;
                }
                i=m;
                r=j;
                for(r=j;r<8;r--)
                {
                 if(i!=m && j!=m && pieces[(int)desk[r][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
                        {etr1[p][0]=i;// No stolba
                        etr1[p][1]=r;//No stroki
                        p++;
                NSPSize++;}
                i++;


                if ((i)>=8 || (r-1)<0)
                          break;
                if(desk[r-1][i]!=-1  && pieces[(int)desk[r-1][i]].getColor()==pieces[(int)desk[j][m]].getColor() )
                            break;
                        if(desk[r][i-1]!=-1  && pieces[(int)desk[r][i-1]].getColor()!=pieces[(int)desk[j][m]].getColor())
                            break;
                }

                //-������-


                i=m;
                for(i=m;i<8;i++)
                { if(i!=m)
                        {etr1[p][0]=i;// No stolba
                        etr1[p][1]=j;//No stroki
                        p++;
                NSPSize++;}
                        if(desk[j][i+1]!=-1 && (i+1)<8 && pieces[(int)desk[j][i+1]].getColor()==pieces[(int)desk[j][m]].getColor() )
                            break;
                        if(desk[j][i]!=-1 && i!=(m+1) && pieces[(int)desk[j][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
                            break;
                }
                i=m;
                for(i=m;i>=0;i--)
                    {if(i!=m)
                        { etr1[p][0]=i;// No stolba
                        etr1[p][1]=j;//No stroki
                        p++;
                        NSPSize++;
                }
                        if(desk[j][i-1]!=-1 && (i-1)>=0 && pieces[(int)desk[j][i-1]].getColor()==pieces[(int)desk[j][m]].getColor() )
                            break;
                        if(desk[j][i]!=-1 && i!=(m-1) && pieces[(int)desk[j][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
                            break;

                }

            i=j;
                for(i=j;i>=0;i=i--)
                {if(i!=j && pieces[(int)desk[j][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
                        { etr1[p][0]=m;// No stolba
                        etr1[p][1]=i;//No stroki
                        p++;
                        NSPSize++;
                }
                        if(desk[i-1][m]!=-1 && (i-1)>=0 && pieces[(int)desk[i-1][m]].getColor()==pieces[(int)desk[j][m]].getColor() )
                            break;
                        if(desk[i][m]!=-1  && pieces[(int)desk[i][m]].getColor()!=pieces[(int)desk[j][m]].getColor())
                            break;
                }
                i=j;
                for(i=j;i<8;i=i++)
                {if(i!=j && pieces[(int)desk[j][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
                        { etr1[p][0]=m;// No stolba
                        etr1[p][1]=i;//No stroki
                        p++;
                        NSPSize++;
                }
                        if(desk[i+1][m]!=-1  && (i+1)<8 && pieces[(int)desk[i+1][m]].getColor()==pieces[(int)desk[j][m]].getColor() )
                            break;
                        if(desk[i][m]!=-1  && i!=j && pieces[(int)desk[i][m]].getColor()!=pieces[(int)desk[j][m]].getColor() )
                            break;
                }






    // ����

         p=0;
           if((m+2)<8)
               { if((j+1)<8  && pieces[(int)desk[j+1][m+2]].getPiece()==KNIGHT && ((desk[j+1][m+2]!=-1 &&  pieces[(int)desk[j+1][m+2]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
                    { etr1[p][0]=m+2;// No stolba
                        etr1[p][1]=j+1;//No stroki
                        p++;
                        NSPSize++;
                }
           if(pieces[(j-1)>=0  && (int)desk[j-1][m+2]].getPiece()==KNIGHT && ((desk[j-1][m+2]!=-1 &&  pieces[(int)desk[j-1][m+2]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
                    { etr1[p][0]=m+2;// No stolba
                        etr1[p][1]=j-1;//No stroki
                        p++;
                        NSPSize++;
                }

           }

           if((m-2)>=0)
               { if((j+1)<8 && pieces[(int)desk[j+1][m-2]].getPiece()==KNIGHT &&  (desk[j+1][m-2]!=-1 &&  pieces[(int)desk[j+1][m-2]].getColor()!=pieces[(int)desk[j][m]].getColor() ))
                    { etr1[p][0]=m-2;// No stolba
                        etr1[p][1]=j+1;//No stroki
                        p++;
                        NSPSize++;
                }
           if(pieces[(j-1)>=0  && (int)desk[j-1][m-2]].getPiece()==KNIGHT && (desk[j-1][m-2]!=-1 &&  pieces[(int)desk[j-1][m-2]].getColor()!=pieces[(int)desk[j][m]].getColor() ))
                    { etr1[p][0]=m-2;// No stolba
                        etr1[p][1]=j-1;//No stroki
                        p++;
                        NSPSize++;
                }

           }

           if((j+2)<8)
               { if((m+1)<8  && pieces[(int)desk[j+2][m+1]].getPiece()==KNIGHT && ((desk[j+2][m+1]!=-1 &&  pieces[(int)desk[j+2][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
                    { etr1[p][0]=m+1;// No stolba
                        etr1[p][1]=j+2;//No stroki
                        p++;
                        NSPSize++;
                }
           if((m-1)>=0  && pieces[desk[j+2][m-1]].getPiece()==KNIGHT && ( desk[j+2][m-1]!=-1 &&  pieces[desk[j+2][m-1]].getColor()!=pieces[desk[j][m]].getColor() ) )
                    { etr1[p][0]=m-1;// No stolba
                        etr1[p][1]=j+2;//No stroki
                        p++;
                        NSPSize++;
                }

           }

           if((j-2)>=0)
               { if((m+1)<8  && pieces[(int)desk[j-2][m+1]].getPiece()==KNIGHT && (desk[j-2][m+1]!=-1 &&  pieces[(int)desk[j-2][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() ) )
                    { etr1[p][0]=m+1;// No stolba
                        etr1[p][1]=j-2;//No stroki
                        p++;
                        NSPSize++;
                }
           if(pieces[(m-1)>=0  && (int)desk[j-2][m-1]].getPiece()==KNIGHT && ((desk[j-2][m-1]!=-1 &&  pieces[(int)desk[j-2][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
                    { etr1[p][0]=m-1;// No stolba
                        etr1[p][1]=j-2;//No stroki
                        p++;
                        NSPSize++;
                }

           }








    // �������� �����������
        int	qw=0;
        while(qw<NSPSize)
        {
n = etr1[qw][1] * 8 + etr1[p][0];
NSPArray[qw]=n;
qw++;}
        for(qw=NSPSize;qw<16;qw++)
            NSPArray[qw]=-1;
return NSPArray;

}  */

void Partiya::allPicesEnableTurnes()
{int k=0,l=0,m=0;
for( l=0;l<32;l++)
    {
        m=pieces[l].getColor();
        if(k==16)
            k=0;
        isEnableTurnArray(pieces[l].getCoord() , allPicesEnableTurnesArray[k][m]);
            k++;
    }
}

//???????? ?? ????????????.
// ds - ???? ????, ??? ???? ?????????.
// n - ??????????.
bool Partiya::uSMPCheck(int r, int ds)
{
     allPicesEnableTurnes();

     for(int i=0;i<16;i++)
             for(int j=0;j<29;j++)
                 if(allPicesEnableTurnesArray[i][whichTurn][j]==pieces[r].getCoord())
                 {
                    qDebug()<<"Shah piece = "<<i<<endl;
                     return true;
                 }
      return false;
}

void Partiya::unMove(move mv, Piece jmChess_, Piece eatenChess_)
{
    if(map[mv.n2]!=-1)
        pieces[map[mv.n2]]=jmChess_;
    map[mv.n1]=map[mv.n2];
    if(eatenChess_.getPiece()!=NONE)
    {
        for(int i=0;i<32;i++)
            if(pieces[i].getCoord()==mv.n2)
                pieces[i].setIsEnable(true);
    }
    else
        map[mv.n2]=-1;

}
//---------------------------------------------------------------------------}

