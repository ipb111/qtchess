//---------------------------------------------------------------------------

#ifndef chessPartiyaH
#define chessPartiyaH

//---------------------------------------------------------------------------

 #include "chessDataTypes.h"

#include <QObject>

using namespace data;

class Partiya : public QObject
{
	Q_OBJECT
private:
	Piece pieces[32];			// � ������� pieces ����������� ���������� ������ Piece.
	int map[64];
	int enableTurnArray[29];
    //int ETR4[29];
    int num1;// ����� ������, ����������� ��� ���� � ������� pieces[32]
    uint whichTurn;//��� ����� � ���� ���(������ ��� �����)
    int schot[2];
    int errorCode;
    int enableTurnArraySize;
    int KingStarusCode[2];// 0- ��������, 1- ���, 2- ���, 3-���.
    public:
        move move1;// ���
    //���� ������
    //int playerColor;//�� ����� ���� ������ �����
    //--
    /*typedef enum
    {
    PVP1COMPUTER=1, // PVP ���� �� 1 ����������
    PVP2COMPUTER=2, // PVP ���� �� 2 ����������
    PVC=3  // ���� ������ ����������
    } partyiaTipe;*/

    /*typedef enum
    {
    NEWPARTIYA=1,// �������� ����� ������
    LOADPARTIYA=2 // �������� ������ ������
    } startTipe;*/

signals:
    void switchPlayer(bool black, int score);
private:

    int *isEnableTurnArray(int n, int enableTurnArray[29]);//���������� ��������� �����
    bool isThisTurnEnable(int enableTurnArray[], int n);//�������� �� ����������� ����
    void goTurn(Piece & b,  int n2); // ����������� ������
    void newTurn();//��������� ����� ���������� ����
    bool perehodCherezStroku(int n, int i);// �������� �� ������� ��� ���� -> ��� <- ����� ������
    int eatChess(int n, int n2);// ��������� ������

    void isShahMatPat();// �������� �� ���, ���, ���
    bool uSMPCheck(int r,int ds);// ���������, ��������� �� ������ ��� ������(True- ��, False - ���)
    bool isShah(int color);//�������� �� ���
    bool uMPCheck(int etr4[29], int ds); //�������� �� ���
    bool isPat(int ds); // �������� �� ���(������������ ������ ���� ������� ������ ������)
    public:

    void startPartiyaInitiation();//��������� ����� ������
    void loadPartiaInitiation(Partiya a);//��������� ����������� ������
    bool takeCoord(int n, int **entarray, int &arraySize);//��������� � ��������� �������� ����������

    Partiya(QObject* parent);//�����������
    ~Partiya();//����������
    int getWhichTurn();
    int getSchot(PieceColor n);
    int getKingStatusCode();
};

inline int Partiya::getKingStatusCode()
{
    int color=whichTurn;
    return KingStarusCode[color];
}
int inline Partiya::getWhichTurn()
{
    return whichTurn;
}

int inline Partiya::getSchot(PieceColor n)
{
    return schot[n];
}


/*float Partiya::map[64]= {
	0 ,1 ,2 ,3 ,4 ,5 ,6 ,7 ,
	8 ,9 ,10,11,12,13,14,15,
	-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,
	16,17,18,19,20,21,22,23,
	24,25,26,27,28,29,30,31};  */


#endif
