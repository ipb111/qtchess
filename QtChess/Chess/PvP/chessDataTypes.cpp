#include "chessDataTypes.h"

Piece::Piece() : coord_(0), piece_(NONE), color_(0), cost_(FREE_COST), isMove_(false), isEnable_(false)
{ }
Piece::Piece(const Piece& o) : coord_(o.coord_), piece_(o.piece_),
                               color_(o.color_), cost_(o.cost_), isMove_(o.isMove_), isEnable_(o.isEnable_)
{ }
Piece::Piece(uchar coord, uchar piece, uchar color, uchar cost) : coord_(coord), piece_(piece), color_(color), cost_(cost), isMove_(false), isEnable_(false)
{ }

void Piece::setPiece(uchar coord, uchar piece, uchar color)
{
	coord_ = coord;
	piece_ = piece;
	color_ = color;
	determineCost();
}

void Piece::determineCost()
{
	switch(piece_)
	{
		case KING:
			cost_ = KING_COST;
			break;
		case QUEEN:
			cost_ = QUEEN_COST;
			break;
		case BISHOP:
			cost_ = BISHOP_KNIGHT_COST;
			break;
		case KNIGHT:
			cost_ = BISHOP_KNIGHT_COST;
			break;
		case ROOK:
			cost_ = ROOK_COST;
			break;
		case PAWN:
			cost_ = PAWN_COST;
			break;
		case NONE:
			cost_ = FREE_COST;
			break;
	}
}
