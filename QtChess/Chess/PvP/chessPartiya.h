//---------------------------------------------------------------------------

#ifndef chessPartiyaH
#define chessPartiyaH

//---------------------------------------------------------------------------

#include "../../Chess/PvP/chessdatatypes.h"
#include <QObject>

 using namespace data;

class Partiya: public QObject
{
    Q_OBJECT
private:
Piece pieces[32];	// � ������� pieces ����������� ���������� ������ Piece.
int map[64];
int enableTurnArray[29];
//int ETR4[29];
int num1;// ����� ������, ����������� ��� ���� � ������� pieces[32]
uint whichTurn;//��� ����� � ���� ���(������ ��� �����)
Piece eatenChess;
Piece jmChess;

int schot[2];
int errorCode;
int enableTurnArraySize;
int KingStarusCode[2];// 0- ��������, 1- ���, 2- ���, 3-���.

int myLanColor; // ��� ���� ����� ��� ���� ����� ���
public:
int allPicesEnableTurnesArray[16][2][29];
	move move1;// ���

int partiyaTipe;

typedef enum GameMode_
{
    PVPLoc_Pvc=1, //  ���� �� 1 ����������
    PVPVLan=2    // PVP ���� �� 2 ���������� ����� ���
}GameMode;

signals:
    void switchPlayer(bool black, int score);
    void check();
private: 

//int * Partiya::isNSPArray(int n, int   NSPArray[]);//���������� ����. � ������ ����� � �����.
int *isEnableTurnArray(int n, int enableTurnArray[29]);//���������� ��������� �����
bool isThisTurnEnable(int enableTurnArray[], int n);//�������� �� ����������� ����
void goTurn(Piece & b,  int n2); // ����������� ������
void newTurn();//��������� ����� ���������� ����
bool perehodCherezStroku(int n, int i);// �������� �� ������� ��� ���� -> ��� <- ����� ������ 
int eatChess(int n, int n2);// ��������� ������

void isShahMatPat();// �������� �� ���, ���, ���
bool uSMPCheck(int r,int ds);// ���������, ��������� �� ������ ��� ������(True- ��, False - ���)
bool isShah(int color);//�������� �� ���
bool uMPCheck(int etr4[29], int ds); //�������� �� ���
bool isPat(int ds); // �������� �� ���(������������ ������ ���� ������� ������ ������)
void allPicesEnableTurnes();

public:
void NewLanPartiyaInitiation(int LanColor);//��������� �����  lan ������
void startPartiyaInitiation();//��������� ����� ������
void loadPartiaInitiation(Partiya a);//��������� ����������� ������
bool takeCoord(int n, int **entarray, int &arraySize);//��������� � ��������� �������� ����������
Partiya(QObject* parent);//�����������
~Partiya();//����������
int getWhichTurn();
int getSchot(PieceColor n);
int getKingStatusCode();
void unMove(move mv, Piece jmChess_, Piece eatenChess_);


const Piece *getPieces();
const int *getMap();
Piece *setPieces();
int *setMap();
void setTurn(bool p);
Piece getEatenChess();
Piece getJMChess();
};

inline int   Partiya::getKingStatusCode()
{int color=whichTurn;
	return KingStarusCode[color];
}
inline int Partiya::getWhichTurn()
{
	return whichTurn;
}

inline int Partiya::getSchot(PieceColor n)
{
	return schot[n];
}

inline const Piece* Partiya::getPieces()
{
    return pieces;
}

inline const int *Partiya::getMap()
{
    return map;
}

inline Piece* Partiya::setPieces()
{
    return pieces;
}

inline int *Partiya::setMap()
{
    return map;
}

inline void Partiya::setTurn(bool p)
{
    whichTurn = p;
}
inline Piece Partiya::getEatenChess()
{
    return eatenChess;
}

inline Piece Partiya::getJMChess()
{
    return jmChess;
}

/*float Partiya::map[64]= {
	0 ,1 ,2 ,3 ,4 ,5 ,6 ,7 ,
	8 ,9 ,10,11,12,13,14,15,
	-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,
	16,17,18,19,20,21,22,23,
	24,25,26,27,28,29,30,31};  */


#endif
