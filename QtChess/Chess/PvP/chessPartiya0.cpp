
#include "chessPartiya.h"
#include <QDebug>

Piece pieces[32]; 
// ��������� ����� � �� ���������� ������������ ��� ������ ����� ������

Partiya::Partiya(QObject* parent) : QObject(parent)
{}

Partiya::~Partiya()
{
}

void Partiya::startPartiyaInitiation()
{
	
	for(int i=0; i<32; i++)
	{   //������ ����
		if (i <= 15)
        {
			pieces[i].setColor(BLACK);
		}
        else
		pieces[i].setColor(WHITE);
	    //������ ��� ������ � � ��������
		if (i == 0 || i == 7 || i == 24 || i == 31)
		{ 
			pieces[i].setPiece(ROOK);
			pieces[i].setCost(5);
		}
		if (i == 1 || i == 6|| i == 25 || i == 30)
	    { 
			pieces[i].setPiece(KNIGHT);
	        pieces[i].setCost(3);
		}
	    if (i == 2 || i == 5 || i == 26 || i == 29)
		{
			pieces[i].setPiece(BISHOP);
			pieces[i].setCost(3);
		}
		if (i == 28 || i == 4)
	    {
			pieces[i].setPiece(KING);
	        pieces[i].setCost(9);
		}
	    if (i == 27 || i== 3)
		{ 
			pieces[i].setPiece(QUEEN);
			pieces[i].setCost(100);
		}
		if ((i >= 8 && i <= 15) || (i >= 16 && i <= 23))
		{
			pieces[i].setPiece(PAWN);
			pieces[i].setCost(1);
		}
	    //������ ��������� ����������
	    if(i <= 15)
			pieces[i].setCoord(i);
        else
		   pieces[i].setCoord(i + 32);
        // ������ ������� ��� ���
		pieces[i].setIsEnable(true);
	    // ������ ��������� ��� ���
		pieces[i].setIsMove(false);	
	 
    }
    //������ ��������� ������������ ����� �� �����
    for(int i=0; i<64; i++)
         Partiya::map[i] = -1;
    //int y = 0;
	for(int i=0; i<64; i++)
	    if(i<=15)
			map[i] = i;
        else if(i>=48)
		   map[i] = (i-32);

		else
		   map[i] = -1;

	for(int i=0; i<32; i++)
	{
        enableTurnArray[i]=-1;
        //y = enableTurnArray[i];
    }
	whichTurn = WHITE;
	schot[0] = 0;
	schot[1] = 0;
}
// ������������� ������ �� ����������
void Partiya::loadPartiaInitiation(Partiya a)
{
	for(int i=0; i<32; i++)
	{
		pieces[i].setColor(a.pieces[i].getColor());
		pieces[i].setCoord(a.pieces[i].getCoord());
		pieces[i].setPiece(a.pieces[i].getPiece());
		pieces[i].setCost(a.pieces[i].getCost());
		pieces[i].setIsEnable(a.pieces[i].isEnable());
		pieces[i].setIsMove(a.pieces[i].isMove());
	}
	for(int i=0; i<64; i++)
	{
		map[i] = a.map[i];
	}
	for(int i=0; i<30; i++)
		enableTurnArray[i] = -1;
}

//���������� FALSE � ������ �������� � TRUE ���� �� ���������
// i - ���������� �� ������ �� ������ � ������� ����������� �������
// n - ����������, ������� �����������
/*bool Partiya::perehodCherezStroku(int n, int i)
{
	if(i<0 || i>63)
		return false;
	if(n<0 || n>63)
	return false;
if(i>=0 && i<=7 && n>7 )
	return false;
else
	return true;
if(i>=8 && i<=15 && n>15 && n<8 )
	return false;
else
	return true;
if(i>=16 && i<=23 && n>23 && n<16 )
	return false;
else
	return true;
if(i>=24 && i<=31 && n>31 && n<24 )
	return false;
else
	return true;
if(i>=32 && i<=39 && n>39 && n<32 )
	return false;
else
	return true;
if(i>=40 && i<=47 && n>47 && n<40 )
	return false;
else
	return true;
if(i>=48 && i<=55 && n>55 && n<48 )
	return false;
else
	return true;
if(i>=56 && i<=63 && n<56 )
	return false;
else
	return true;

}*/


   // ��������� ������� ��������� ����� ��� ������ ������
int * Partiya::isEnableTurnArray(int n, int enableTurnArray[])
{
	int k = 0, l = 0;
	int desk[8][8];
	int etr1[32][2];
	enableTurnArraySize = 0;
	int i = 0, r;		
	   
	for(i=0; i<64; i++)
	{
		desk[k][l] = map[i];
		l++;
 		if(i==7 ||  i==15 || i==23 || i==31 || i==39 || i==47 || i==55 || i==63)
		{ 
			k++;
		    l = 0;
		}
	}
	
	int m = n%8;//������� �� �������
	int j = n/8;

	//-�����-
	if (pieces[(int)desk[j][m]].getPiece() == ROOK)
	{
		int p = 0;
		i = 0;
		i = m;
		for(i=m; i<8; i++)
		{ 
			if (i != m)
			{
				etr1[p][0] = i;	// No stolba
			    etr1[p][1] = j;	//No stroki
				p++;
				enableTurnArraySize++;
			}
			if (desk[j][i+1]!=-1 && (i+1)<8 && pieces[(int)desk[j][i+1]].getColor()==pieces[(int)desk[j][m]].getColor() )
				break;
			if (desk[j][i]!=-1 && i!=(m+1) && pieces[(int)desk[j][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}
		i = m;
		for(i=m; i>=0; i--)
		{
			if(i != m)
			{ 
				etr1[p][0] = i;	// No stolba
			    etr1[p][1] = j;	//No stroki
				p++; 
				enableTurnArraySize++;
			}
			if (desk[j][i-1]!=-1 && (i-1)>=0 && pieces[(int)desk[j][i-1]].getColor()==pieces[(int)desk[j][m]].getColor() )
				break;
			if (desk[j][i]!=-1 && i!=(m-1) && pieces[(int)desk[j][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;						
		}
				
		i = j;
		for(i=j; i>=0; i=i--)
		{
			if (i != j)
			{ 
				etr1[p][0] = m;// No stolba
			    etr1[p][1] = i;//No stroki
				p++;
				enableTurnArraySize++;
			}
			if (desk[i-1][m]!=-1 && (i-1)>=0 && pieces[(int)desk[i-1][m]].getColor()==pieces[(int)desk[j][m]].getColor() )
				break;
			if (desk[i][m]!=-1  && pieces[(int)desk[i][m]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}
	    i = j;
		for(i=j; i<8; i=i++)
		{
			if(i != j)
			{ 
				etr1[p][0] = m;	// No stolba
			    etr1[p][1] = i;	//No stroki
				p++;
				enableTurnArraySize++;
			}
			if(desk[i+1][m]!=-1  && (i+1)<8 && pieces[(int)desk[i+1][m]].getColor()==pieces[(int)desk[j][m]].getColor() )
				break;
			if(desk[i][m]!=-1  && i!=j && pieces[(int)desk[i][m]].getColor()!=pieces[(int)desk[j][m]].getColor() )
				break;
		}
	}

	//-�����-		
	if (pieces[(int)desk[j][m]].getPiece()==PAWN)
	{
		int p = 0;
		if (pieces[(int)desk[j][m]].getColor()==BLACK)
		{
			if (desk[j+1][m]==-1 && j<=8)
			{
				etr1[p][0] = m;		// No stolba
			    etr1[p][1] = j+1;	//No stroki
				p++;
				enableTurnArraySize++;
			}
			if (desk[j+2][m]==-1 && desk[j+1][m]==-1 && pieces[(int)desk[j][m]].isMove()==false   )
			{
				etr1[p][0] = m;		// No stolba
			    etr1[p][1] = j+2;	//No stroki
				p++;
				enableTurnArraySize++;
			}
			if (desk[j+1][m+1]!=-1 && pieces[(int)desk[j+1][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() && m>=0 && j<8)
			{
				etr1[p][0] = m+1;	// No stolba
			    etr1[p][1] = j+1;	//No stroki
				p++;
				enableTurnArraySize++;
			}
			if(desk[j+1][m-1]!=-1 && pieces[(int)desk[j-1][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor()  && m>=0 && j<8)
			{
				etr1[p][0] = m-1;		// No stolba
			    etr1[p][1] = j+1;		//No stroki
				p++;
				enableTurnArraySize++;
			}

		}
		
		if (pieces[(int)desk[j][m]].getColor()==WHITE)
		{
			if(desk[j-1][m]==-1 && j>=0)
			{
				etr1[p][0] = m;		// No stolba
			    etr1[p][1]=j-1;		//No stroki
				p++;
				enableTurnArraySize++;
			}
			if (desk[j-2][m]==-1 && desk[j-1][m]==-1 && pieces[(int)desk[j][m]].isMove()==false)
			{
				etr1[p][0] = m;			// No stolba
			    etr1[p][1] = j-2;		//No stroki
				p++;
				enableTurnArraySize++;
			}
			if(desk[j-1][m+1]!=-1 && pieces[(int)desk[j-1][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() && j>=0 && m<8)
			{
				etr1[p][0] = m+1;		// No stolba
			    etr1[p][1] = j-1;		//No stroki
				p++;
				enableTurnArraySize++;
			}
			if(desk[j-1][m-1]!=-1 && pieces[(int)desk[j-1][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor()  && j>=0 && m<8)
			{
				etr1[p][0] = m-1;// No stolba
			    etr1[p][1] = j-1;//No stroki
				p++;
				enableTurnArraySize++;
			}
		}		  
	}

	//-������-
	if (pieces[(int)desk[j][m]].getPiece()==BISHOP)
	{
		int p = 0;
		i = 0;
		r = 0;
		i = m;
		r = j;
		for(r=j; r<8; r++)
		{
			if((i)!=m && r!=j)
			{
				etr1[p][0] = i;		// No stolba
			    etr1[p][1] = r;		//No stroki
				p++;
				enableTurnArraySize++;
			}
			i++;
			if ((i)>=8 || (r+1)>=8) 
				break;
				
			if(desk[r+1][i]!=-1  && pieces[(int)desk[r+1][i]].getColor()==pieces[(int)desk[j][m]].getColor())
				break;
			if(desk[r][i-1]!=-1 && pieces[(int)desk[r][i-1]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}
		i = m;
		r = j;
		for(r=j; r<8; r--)
		{
			if(i!=m && j!=m)
			{
				etr1[p][0] = i;// No stolba
			    etr1[p][1] = r;//No stroki
				p++;
				enableTurnArraySize++;
			}
			i--;			           
				
			if ((i)<0 || (r-1)<0 || r<0) 
				break;
			if (desk[r-1][i]!=-1  && pieces[(int)desk[r-1][i]].getColor()==pieces[(int)desk[j][m]].getColor()) 
				break;
			if(desk[r][i+1]!=-1  && pieces[(int)desk[r][i+1]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}
				
		i = m;
		r = j;
		for(r=j; r<8; r++)
		{
			if(i!=m && j!=m)
			{
				etr1[p][0] = i;// No stolba
			    etr1[p][1] = r;//No stroki
				p++;
				enableTurnArraySize++;
			}
			i--;			           
			
			if ((i)<0 || (r+1)>=8) 
				break;
			if(desk[r+1][i]!=-1  && pieces[(int)desk[r+1][i]].getColor()==pieces[(int)desk[j][m]].getColor() )
				break;
			if(desk[r][i+1]!=-1 && pieces[(int)desk[r][i+1]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}
		i = m;
		r = j;
		for(r=j; r<8; r--)
		{
			if(i!=m && j!=m)
			{
				etr1[p][0] = i;		// No stolba
			    etr1[p][1] = r;		//No stroki
				p++;
				enableTurnArraySize++;
			}
			i++;		           
				
			if ((i+1)>=8 || (r-1)<0) 
				break;
			if (desk[r-1][i]!=-1  && pieces[(int)desk[r-1][i]].getColor()==pieces[(int)desk[j][m]].getColor())
				break;
			if (desk[r][i-1]!=-1  && pieces[(int)desk[r][i-1]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}
	}

	//-��������-
	if (pieces[(int)desk[j][m]].getPiece()==QUEEN)
	{	
		int p = 0;
		i = 0;
		r = 0;
		enableTurnArraySize = 0;
		i = m;
		r = j;
		for(r=j; r<8; r++)
		{
			if((i)!=m && r!=j)
			{
				etr1[p][0] = i;		// No stolba
			    etr1[p][1] = r;		//No stroki
				p++;
				enableTurnArraySize++;
			}
			i++;
			if ((i)>=8 || (r+1)>=8) 
				break;
				
			if (desk[r+1][i]!=-1  && pieces[(int)desk[r+1][i]].getColor()==pieces[(int)desk[j][m]].getColor() )
				break;
			if(desk[r][i-1]!=-1 && pieces[(int)desk[r][i-1]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}
		i = m;
		r = j;
		for(r=j; r<8; r--)
		{
			if(i!=m && j!=m)
			{
				etr1[p][0] = i;		// No stolba
			    etr1[p][1] = r;		//No stroki
				p++;
				enableTurnArraySize++;
			}
			i--;			           
				
			if ((i)<0 || (r-1)<0) 
				break;
			if (desk[r-1][i]!=-1  && pieces[(int)desk[r-1][i]].getColor()==pieces[(int)desk[j][m]].getColor()) 
				break;
			if(desk[r][i+1]!=-1  && pieces[(int)desk[r][i+1]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}
				
		i = m;
		r = j;
		for(r=j; r<8; r++)
		{
			if(i!=m && j!=m)
			{
				etr1[p][0] = i;		// No stolba
			    etr1[p][1] = r;		//No stroki
				p++;
				enableTurnArraySize++;
			}
			i--;			           
			
			if ((i)<0 || (r+1)>=8) 
				break;
			if(desk[r+1][i]!=-1  && pieces[(int)desk[r+1][i]].getColor()==pieces[(int)desk[j][m]].getColor())
				break;
			if(desk[r][i+1]!=-1 && pieces[(int)desk[r][i+1]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}
		i = m;
		r = j;
		for(r=j; r<8; r--)
		{
			if(i!=m && j!=m)
			{
				etr1[p][0] = i;// No stolba
			    etr1[p][1] = r;//No stroki
				p++;
				enableTurnArraySize++;
			}
			i++;			           
				
			if ((i)>=8 || (r-1)<0) 
				break;
			if(desk[r-1][i]!=-1  && pieces[(int)desk[r-1][i]].getColor()==pieces[(int)desk[j][m]].getColor())
				break;
			if(desk[r][i-1]!=-1  && pieces[(int)desk[r][i-1]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}

		//-������-
		i = m;
		for(i=m; i<8; i++)
		{ 
			if(i != m)
			{
				etr1[p][0] = i;		// No stolba
			    etr1[p][1] = j;		//No stroki
				p++;
				enableTurnArraySize++;
			}
			if (desk[j][i+1]!=-1 && (i+1)<8 && pieces[(int)desk[j][i+1]].getColor()==pieces[(int)desk[j][m]].getColor())
				break;
			if (desk[j][i]!=-1 && i!=(m+1) && pieces[(int)desk[j][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}
		i = m;
		for(i=m; i>=0; i--)
		{
			if (i != m)
			{ 
				etr1[p][0] = i;		// No stolba
			    etr1[p][1] = j;		//No stroki
				p++; 
				enableTurnArraySize++;
			}
			if (desk[j][i-1]!=-1 && (i-1)>=0 && pieces[(int)desk[j][i-1]].getColor()==pieces[(int)desk[j][m]].getColor() )
				break;
			if (desk[j][i]!=-1 && i!=(m-1) && pieces[(int)desk[j][i]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;						
		}
				
		i = j;
		for (i=j; i>=0; i=i--)
		{
			if (i != j)
			{ 
				etr1[p][0] = m;		// No stolba
			    etr1[p][1] = i;		//No stroki
				p++;
				enableTurnArraySize++;
			}
			if (desk[i-1][m]!=-1 && (i-1)>=0 && pieces[(int)desk[i-1][m]].getColor()==pieces[(int)desk[j][m]].getColor())
				break;
			if(desk[i][m]!=-1  && pieces[(int)desk[i][m]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}
		i = j;
		for(i=j; i<8; i=i++)
		{	
			if (i != j)
			{ 
				etr1[p][0] = m;		// No stolba
			    etr1[p][1] = i;		//No stroki
				p++;
				enableTurnArraySize++;
			}
			if (desk[i+1][m]!=-1  && (i+1)<8 && pieces[(int)desk[i+1][m]].getColor()==pieces[(int)desk[j][m]].getColor())
				break;
			if (desk[i][m]!=-1  && i!=j && pieces[(int)desk[i][m]].getColor()!=pieces[(int)desk[j][m]].getColor())
				break;
		}

	}

	//-������-
	if(pieces[(int)desk[j][m]].getPiece()==KING)
	{
		int p = 0;
		//������
		if((m+1)<8 &&(desk[j][m+1]==-1 ||(desk[j][m+1]!=-1 &&  pieces[(int)desk[j][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
		{ 
			etr1[p][0] = m+1;		// No stolba
			etr1[p][1] = j;			//No stroki
			p++;
			enableTurnArraySize++;
		}

		if ((m-1)>=0 &&(desk[j][m-1]==-1 ||(desk[j][m-1]!=-1 &&  pieces[(int)desk[j][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
		{ 
			etr1[p][0] = m-1;// No stolba
			etr1[p][1] = j;//No stroki
			p++;
			enableTurnArraySize++;
		}

		if ((j+1)<8 &&(desk[j+1][m]==-1 ||(desk[j+1][m]!=-1 &&  pieces[(int)desk[j+1][m]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
		{ 
			etr1[p][0] = m;		// No stolba
			etr1[p][1] = j+1;	//No stroki
			p++;
			enableTurnArraySize++;
		}

		if((j-1)>=0 &&(desk[j-1][m]==-1 ||(desk[j-1][m]!=-1 &&  pieces[(int)desk[j-1][m]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
		{ 
			etr1[p][0] = m;		// No stolba
			etr1[p][1] = j-1;	//No stroki
			p++;
			enableTurnArraySize++;
		}
		//���������
			if((m+3)<8)
			if(pieces[(int)desk[j][m]].isMove()==false &&  pieces[(int)desk[j][m+3]].getPiece()==ROOK && pieces[(int)desk[j][m+3]].isMove()==false)
				if(desk[j][m-1]==-1 && desk[j][m-2]==-1)
					{etr1[p][0]=m+2;// No stolba
			        	etr1[p][1]=j;//No stroki
						p++;
						enableTurnArraySize++;
			}

			if((m-4)>=0)
			if(pieces[(int)desk[j][m]].isMove()==false &&  pieces[(int)desk[j][m-4]].getPiece()==ROOK && pieces[(int)desk[j][m-4]].isMove()==false)
				if(desk[j][m-1]==-1 && desk[j][m-2]==-1)
					{etr1[p][0]=m-3;// No stolba
			        	etr1[p][1]=j;//No stroki
						p++;
						enableTurnArraySize++;
			}
			//���������
			if((j-1)>=0 && (m-1)>=0 &&(desk[j-1][m-1]==-1 ||(desk[j-1][m-1]!=-1 &&  pieces[(int)desk[j-1][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
				{ etr1[p][0]=m-1;// No stolba
			        	etr1[p][1]=j-1;//No stroki
						p++;
						enableTurnArraySize++;
				}

			if((j-1)>=0 && (m+1)<8 &&(desk[j-1][m+1]==-1 ||(desk[j-1][m+1]!=-1 &&  pieces[(int)desk[j-1][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
				{ etr1[p][0]=m+1;// No stolba
			        	etr1[p][1]=j-1;//No stroki
						p++;
						enableTurnArraySize++;
				}

			
			if((j+1)<8 && (m+1)<8 &&(desk[j+1][m+1]==-1 ||(desk[j+1][m+1]!=-1 &&  pieces[(int)desk[j+1][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
				{ etr1[p][0]=m+1;// No stolba
			        	etr1[p][1]=j+1;//No stroki
						p++;
						enableTurnArraySize++;
				}

			if((j+1)<8 && (m-1)>=0 &&(desk[j+1][m-1]==-1 ||(desk[j+1][m-1]!=-1 &&  pieces[(int)desk[j+1][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
				{ etr1[p][0]=m-1;// No stolba
			        	etr1[p][1]=j+1;//No stroki
						p++;
						enableTurnArraySize++;
				}
		}
    //-����-

    if(pieces[(int)desk[j][m]].getPiece()==KNIGHT)
		{int p=0;
           if((m+2)<8)
			   { if((j+1)<8  &&(desk[j+1][m+2]==-1 ||(desk[j+1][m+2]!=-1 &&  pieces[(int)desk[j+1][m+2]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m+2;// No stolba
			        	etr1[p][1]=j+1;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   if((j-1)>=0  &&(desk[j-1][m+2]==-1 ||(desk[j-1][m+2]!=-1 &&  pieces[(int)desk[j-1][m+2]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m+2;// No stolba
			        	etr1[p][1]=j-1;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   
		   }

		   if((m-2)>=0)
			   { if((j+1)<8  &&(desk[j+1][m-2]==-1 ||(desk[j+1][m-2]!=-1 &&  pieces[(int)desk[j+1][m-2]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m-2;// No stolba
			        	etr1[p][1]=j+1;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   if((j-1)>=0  &&(desk[j-1][m-2]==-1 ||(desk[j-1][m-2]!=-1 &&  pieces[(int)desk[j-1][m-2]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m-2;// No stolba
			        	etr1[p][1]=j-1;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   
		   }

		   if((j+2)<8)
			   { if((m+1)<8  &&(desk[j+2][m+1]==-1 ||(desk[j+2][m+1]!=-1 &&  pieces[(int)desk[j+2][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m+1;// No stolba
			        	etr1[p][1]=j+2;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   if((m-1)>=0  &&(desk[j+2][m-1]==-1 ||(desk[j+2][m-1]!=-1 &&  pieces[(int)desk[j+2][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m-1;// No stolba
			        	etr1[p][1]=j+2;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   
		   }

		   if((j-2)>=0)
			   { if((m+1)<8  &&(desk[j-2][m+1]==-1 ||(desk[j-2][m+1]!=-1 &&  pieces[(int)desk[j-2][m+1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m+1;// No stolba
			        	etr1[p][1]=j-2;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   if((m-1)>=0  &&(desk[j-2][m-1]==-1 ||(desk[j-2][m-1]!=-1 &&  pieces[(int)desk[j-2][m-1]].getColor()!=pieces[(int)desk[j][m]].getColor() )) )
					{ etr1[p][0]=m-1;// No stolba
			        	etr1[p][1]=j-2;//No stroki
						p++;
						enableTurnArraySize++;
				}
		   
		   }
}



		
		


	// �������� �����������
		int	p=0;
		while(p<enableTurnArraySize)
		{
n = etr1[p][1] * 8 + etr1[p][0];
enableTurnArray[p]=n;
p++;}
		for(p=enableTurnArraySize;p<29;p++)
			enableTurnArray[p]=-1;
return enableTurnArray;
		
} 



// �������� �� ����������� ����
bool Partiya::isThisTurnEnable(int enableTurnArray[], int n)
{
 for(int i=0;i<29;i++)
        if (enableTurnArray[i]==n)
            return true;
return false;

}

// n- ��, ��� ����
// n2- ��� ����
int Partiya::eatChess(int n, int n2)
{
    if (pieces[ map[n]].getColor()!=pieces[ map[n2]].getColor())
    {
        pieces[ map[n]].setIsEnable(false);
        schot[whichTurn]=schot[whichTurn]+pieces[map[n]].getCost();  // ���������� ����� ������
        return 1;
    }
    return 0;
}

void Partiya::goTurn(Piece & b,  int n2)
{
	isEnableTurnArray(b.getCoord(),enableTurnArray);
	
		if(b.getPiece()==KING && (b.getCoord()+2  || n2==b.getCoord()-3) && b.isMove()==false )
		{
			if(n2==b.getCoord()+2 && pieces[(int) map[n2+1]].getPiece()==ROOK && pieces[(int) map[n2+1]].isMove()==false)
		   {
			   map[n2]=map[b.getCoord()];
			    map[b.getCoord()]=-1;
		   b.setCoord(n2);
		   b.setIsMove(true);
		   pieces[(int)map[n2+1]].setIsMove(true);
		   pieces[(int)map[n2+1]].setCoord(n2-1);
		   map[n2-1]=map[n2+1];
		   map[n2+1]=-1;
		  }
		 
			if((n2==pieces[(int) map[n2]].getCoord()-3)  && pieces[(int) map[n2-1]].getPiece()==ROOK && pieces[(int) map[n2-1]].isMove()==false)
		  {
          
		   map[n2]=map[b.getCoord()];
		    map[b.getCoord()]=-1;
		  b.setCoord(n2);
		   b.setIsMove(true);
		   pieces[(int)map[n2-1]].setIsMove(true);
		   pieces[(int)map[n2-1]].setCoord(n2-1);;
		  map[n2+1]=map[n2-1];
		  map[n2-1]=-1;
		  }
		  }
		else
		{  if(map[n2]!=-1 && pieces[(int)map[n2]].getColor()!=b.getColor())
		   if( eatChess(n2,b.getCoord()) == 1)
	         
               {
				   map[n2]=map[b.getCoord()];
					map[b.getCoord()]=-1;
					b.setCoord(n2);
			   }

		 if(map[n2]==-1)
			 { map[n2]=map[b.getCoord()];
	         map[b.getCoord()]=-1;
			 b.setCoord(n2);}

		  }
	b.setIsMove(true);

}

void Partiya::newTurn()
{
    if (whichTurn==BLACK)
        whichTurn=WHITE;
    else
        whichTurn=BLACK;

    num1=-1;
	/*for(int i=0;i<29;i++)
        enableTurnArray[i]=-1;*/
    bool colorBlack = whichTurn == BLACK;
    emit switchPlayer(colorBlack, getSchot(colorBlack ? BLACK : WHITE));
}

bool Partiya::takeCoord(int n, int **entarray, int &arraySize)
{
	int i;
	int *ptr = 0;
	if ((num1==-1 ||  whichTurn==pieces[ map[n]].getColor() )&& map[n]!=-1 )
		
			if(whichTurn==pieces[map[n]].getColor() && map[n]!=-1)
				 { num1=map[n];
					isEnableTurnArray(n, enableTurnArray);
					arraySize = enableTurnArraySize;
					*entarray = new int [enableTurnArraySize];
					ptr = *entarray;
					for(i=0;i<enableTurnArraySize;i++)
					{
						*ptr=enableTurnArray[i];
						++ptr;
					}

					return true;
				 }
			 else 
				 if(map[n]==-1)
				  { 
						errorCode=-3;//������ � ���, ��� ������� ������ ����
						return false;
					 }
				 else
					 {
						  errorCode=-4; //������ � ���, ��� ������� ������ ��������� ����
						return false;
					 }
	   

	else
	{
		if(errorCode==-6 )
			isEnableTurnArray(pieces[num1].getCoord(),enableTurnArray);
		if(isThisTurnEnable( enableTurnArray,  n))
			{
				move1.n1=pieces[num1].getCoord();
				move1.n2=n;
				goTurn(pieces[num1],n);//���������� ����
	       
			newTurn();
			return true;
			
	      }
		else
		{
			//num1=-1;
			errorCode=-6;//������ � ���, ��� ��� ����������
			return false;
		}
	}
				
}


//---------------------------------------------------------------------------
// debug------------------------------------------------------------------

//#include <iostream>
//#include <math.h>
//#include <stdlib.h>
//#include <iomanip>
//using namespace std;
///*void main()
//{      Partiya e,q;
//Piece b;
//     
//       e.startPartiyaInitiation();
//
//       for (int i=0;i<64;i++)
//       {
//         cout<<setw(2)<<e.map[i]<< " "<<setw(2);
//         if(i==7 ||  i==15 || i==23 || i==31 || i==39 || i==47 || i==55 || i==63)
//              cout<<endl;
//        }
//	   cout<<endl;
//	   for (int i=0;i<64;i++)
//       {
//          cout<<setw(2)<<i<<" "<<setw(2);
//         if(i==7 ||  i==15 || i==23 || i==31 || i==39 || i==47 || i==55 || i==63)
//              cout<<endl;
//        }
//
//       int n;
//	   while(true)
//		{
//	   
//       cout<<"n : ";
//		cin>>n;
//		e.takeCoord(n);	
//		if(e.enableTurnArray[0]!=-5 &&  e.enableTurnArray[0]!=-4 &&  e.enableTurnArray[0]!=-3 &&  e.enableTurnArray[0]!=-6 )
//				for(int i=0;i<29;i++)
//			{
//		cout<<setw(2)<<" "<<e.enableTurnArray[i];}
//		cout<<endl;
//
//		if(e.enableTurnArray[0]==-6 )
//			cout<<"  Hod nevozmozhen "<<endl;
//		if(e.enableTurnArray[0]==-4  )
//			cout<<"  Ne tot cvet figur "<<endl;
//		if(e.enableTurnArray[0]==-3  )
//			cout<<"  Vybrano pustoe pole vmesto figury dla hoda "<<endl;
//		if(e.enableTurnArray[0]==-5)
//		{cout<<" hod sovershon"<<endl;
//			cout<<"WichTurn  : "<<e.whichTurn<<endl;
//		for (int i=0;i<64;i++)
//       {
//          cout<<setw(2)<<e.map[i]<<" "<<setw(2);
//         if(i==7 ||  i==15 || i==23 || i==31 || i==39 || i==47 || i==55 || i==63)
//              cout<<endl;
//        }
//		/*	for (int i=0;i<64;i++)
//       {
//		   cout<<setw(2)<<e.pieces[e.map[i]].getPiece()<< " "<<setw(2);
//         if(i==7 ||  i==15 || i==23 || i==31 || i==39 || i==47 || i==55 || i==63)
//              cout<<endl;
//		}*/
//		
//		}
//		
//		cout<<" num1 :"<<e.num1<<endl;
//		cout<<" n2 :";
//		cin>>n;
//		e.takeCoord(n);
//		if(e.enableTurnArray[0]!=-5 &&  e.enableTurnArray[0]!=-4 &&  e.enableTurnArray[0]!=-3 &&  e.enableTurnArray[0]!=-6 )
//				for(int i=0;i<29;i++)
//			{
//		cout<<setw(2)<<" "<<e.enableTurnArray[i];}
//		cout<<endl;
//
//		if(e.enableTurnArray[0]==-6 )
//			cout<<"  Hod nevozmozhen "<<endl;
//		if(e.enableTurnArray[0]==-4  )
//			cout<<"  Ne tot cvet figur "<<endl;
//		if(e.enableTurnArray[0]==-3  )
//			cout<<"  Vybrano pustoe pole vmesto figury dla hoda "<<endl;
//		if(e.enableTurnArray[0]==-5)
//		{cout<<" hod sovershon"<<endl;
//			cout<<"WichTurn  : "<<e.whichTurn<<endl;
//		for (int i=0;i<64;i++)
//       {
//          cout<<setw(2)<<e.map[i]<<" "<<setw(2);
//         if(i==7 ||  i==15 || i==23 || i==31 || i==39 || i==47 || i==55 || i==63)
//              cout<<endl;
//        }
//		cout<<endl;
//			/*for (int i=0;i<64;i++)
//       {
//		   cout<<setw(2)<<e.pieces[e.map[i]].getPiece()<< " "<<setw(2);
//         if(i==7 ||  i==15 || i==23 || i==31 || i==39 || i==47 || i==55 || i==63)
//              cout<<endl;
//		}*/
//		
//		}
//		cout<<" num1 :"<<e.num1<<endl;
//		}
//
//	
//
//		//b=e.pieces[(int)e.map[62]];
//		
//
//	
//
//
//      for(;;);
//
//}
//*/
