#include "chessengine.h"

#include <stdlib.h>
#include <time.h>
#include <QDebug>

ChessEngine::ChessEngine(QWidget *parent) : IPVEChessEngine(parent)
{
}

ChessEngine::~ChessEngine()
{
    EndGame();
}

struct IPVEChessEngine *CreateChessEngine()
{
    return new ChessEngine();
}

int ChessEngine::mailbox_[120] = {
     -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
     -1,  0,  1,  2,  3,  4,  5,  6,  7, -1,
     -1,  8,  9, 10, 11, 12, 13, 14, 15, -1,
     -1, 16, 17, 18, 19, 20, 21, 22, 23, -1,
     -1, 24, 25, 26, 27, 28, 29, 30, 31, -1,
     -1, 32, 33, 34, 35, 36, 37, 38, 39, -1,
     -1, 40, 41, 42, 43, 44, 45, 46, 47, -1,
     -1, 48, 49, 50, 51, 52, 53, 54, 55, -1,
     -1, 56, 57, 58, 59, 60, 61, 62, 63, -1,
     -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};

int ChessEngine::mailbox64_[64] = {
    21, 22, 23, 24, 25, 26, 27, 28,
    31, 32, 33, 34, 35, 36, 37, 38,
    41, 42, 43, 44, 45, 46, 47, 48,
    51, 52, 53, 54, 55, 56, 57, 58,
    61, 62, 63, 64, 65, 66, 67, 68,
    71, 72, 73, 74, 75, 76, 77, 78,
    81, 82, 83, 84, 85, 86, 87, 88,
    91, 92, 93, 94, 95, 96, 97, 98
};

bool ChessEngine::slide_[6] = {
    false, false, true, true, true, false
};

int ChessEngine::offsets_[6] = {
    0, 8, 4, 4, 8, 8
};

int ChessEngine::offset_[6][8] = {
    { 0, 0, 0, 0, 0, 0, 0, 0 },
    { -21, -19, -12, -8, 8, 12, 19, 21 },
    { -11, -9, 9, 11, 0, 0, 0, 0 },
    { -10, -1, 1, 10, 0, 0, 0, 0 },
    { -11, -10, -9, -1, 1, 9, 10, 11 },
    { -11, -10, -9, -1, 1, 9, 10, 11 }
};

int ChessEngine::castle_mask_[64] = {
     7, 15, 15, 15,  3, 15, 15, 11,
    15, 15, 15, 15, 15, 15, 15, 15,
    15, 15, 15, 15, 15, 15, 15, 15,
    15, 15, 15, 15, 15, 15, 15, 15,
    15, 15, 15, 15, 15, 15, 15, 15,
    15, 15, 15, 15, 15, 15, 15, 15,
    15, 15, 15, 15, 15, 15, 15, 15,
    13, 15, 15, 15, 12, 15, 15, 14
};

/* the piece_ letters, for print_board() */
char ChessEngine::piece_char_[6] = {
    'P', 'N', 'B', 'R', 'Q', 'K'
};
//-------------------------------------------------------------------
// the initial board state
//-------------------------------------------------------------------
int ChessEngine::init_color_[64] = {
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0
};
//-------------------------------------------------------------------
int ChessEngine::init_piece_[64] = {
    3, 1, 2, 4, 5, 2, 1, 3,
    0, 0, 0, 0, 0, 0, 0, 0,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    0, 0, 0, 0, 0, 0, 0, 0,
    3, 1, 2, 4, 5, 2, 1, 3
};
/////////////////////////////////////////////////////////////////
/// IPVEChessEngine
/////////////////////////////////////////////////////////////////
void ChessEngine::SetupMaxDepth(unsigned int depth)
{
    max_depth_ = depth;
}
void ChessEngine::SetupMaxTime(unsigned int time)
{
    max_time_ = time;
}
bool ChessEngine::ComputerTurn(char *outMove)
{
    // think about the move and make it
    Think(1);
    if (!pv_[0][0].u) {
        qDebug() << "no legal moves\n";
        return false;
    }
    char *tmp = MoveStr(pv_[0][0].b);   // memory leak?
    strcpy(outMove, tmp);
    QString str = QString(tr("Computer's move: ")) + QString(outMove);
    qDebug() << str;
    emit SendData(str.toStdString(), 0);
    MakeMove(pv_[0][0].b);
    ply_ = 0;
    Generate();
    PrintResult();

    return true;
}
void ChessEngine::PrintBoardString()
{
    PrintBoard();
}
bool ChessEngine::OpponentTurn(const char *move)
{
    // maybe the user entered a move?
    const int m = ParseMove(move);
    if (m == -1 || !MakeMove(gen_dat_[m].m.b))
    {
        qDebug() << "Illegal move.\n";
        emit SendData(tr("Illegal move.").toStdString(), 0);
    }
    else {
        ply_ = 0;
        Generate();
        PrintResult();
        return true;
    }
    return false;
}
void ChessEngine::EndGame()
{
    CloseBook();
}
void ChessEngine::Unmove()
{
    if (!hply_)
        return;
    UnMakeMove();
    ply_ = 0;
    Generate();
}
void ChessEngine::Init()
{
    InitHash();
    InitBoard();
    OpenBook();
    Generate();
}
void ChessEngine::New()
{
    InitBoard();
    Generate();
}

void ChessEngine::Sync()
{
      emit SyncBoard(piece_, color_);
}

/////////////////////////////////////////////////////////////////
/// BOARD
/////////////////////////////////////////////////////////////////
//-------------------------------------------------------------------
// init_board() sets the board to the initial game state.
void ChessEngine::InitBoard()
{
    int i;

    for (i = 0; i < 64; ++i) {
        color_[i] = init_color_[i];
        piece_[i] = init_piece_[i];
    }
    side_ = LIGHT;
    xside_ = DARK;
    castle_ = 15;
    ep_ = -1;
    fifty_ = 0;
    ply_ = 0;
    hply_ = 0;
    SetHash();  /* InitHash() must be called before this function */
    first_move_[0] = 0;
}
//-------------------------------------------------------------------
// InitHash() initializes the random numbers used by SetHash().
void ChessEngine::InitHash()
{
    int i, j, k;

    srand(0);
    for (i = 0; i < 2; ++i)
        for (j = 0; j < 6; ++j)
            for (k = 0; k < 64; ++k)
                hash_piece_[i][j][k] = HashRand();
    hash_side_ = HashRand();
    for (i = 0; i < 64; ++i)
        hash_ep_[i] = HashRand();
}
//-------------------------------------------------------------------
// HashRand() XORs some shifted random numbers together to make sure
// we have good coverage of all 32 bits. (rand() returns 16-bit numbers
// on some systems.)
int ChessEngine::HashRand()
{
    int i;
    int r = 0;

    for (i = 0; i < 32; ++i)
        r ^= rand() << i;
    return r;
}
//-------------------------------------------------------------------
/* SetHash() uses the Zobrist method of generating a unique number (hash_)
   for the current chess position. Of course, there are many more chess
   positions than there are 32 bit numbers, so the numbers generated are
   not really unique, but they're unique enough for our purposes (to detect
   repetitions of the position).
   The way it works is to XOR random numbers that correspond to features of
   the position, e.g., if there's a black knight on B8, hash_ is XORed with
   hash_piece_[BLACK][KNIGHT][B8]. All of the pieces are XORed together,
   hash_side__ is XORed if it's black's move, and the en passant square is
   XORed if there is one. (A chess technicality is that one position can't
   be a repetition of another if the en passant state is different.) */
void ChessEngine::SetHash()
{
    int i;

    hash_ = 0;
    for (i = 0; i < 64; ++i)
        if (color_[i] != EMPTY)
            hash_ ^= hash_piece_[color_[i]][piece_[i]][i];
    if (side_ == DARK)
        hash_ ^= hash_side_;
    if (ep_ != -1)
        hash_ ^= hash_ep_[ep_];
}
//-------------------------------------------------------------------
// InCheck() returns TRUE if side_ s is in check and FALSE
// otherwise. It just scans the board to find side_ s's king
// and calls Attack() to see if it's being attacked.
bool ChessEngine::InCheck(int s)
{
    int i;

    for (i = 0; i < 64; ++i)
        if (piece_[i] == KING && color_[i] == s)
            return Attack(i, s ^ 1);
    emit SendData("in check", 0);
    return true;  /* shouldn't get here */
}
//-------------------------------------------------------------------
// Attack() returns TRUE if square sq is being attacked by side_
// s and FALSE otherwise.
bool ChessEngine::Attack(int sq, int s)
{
    int i, j, n;

    for (i = 0; i < 64; ++i)
        if (color_[i] == s) {
            if (piece_[i] == PAWN) {
                if (s == LIGHT) {
                    if (COL(i) != 0 && i - 9 == sq)
                        return true;
                    if (COL(i) != 7 && i - 7 == sq)
                        return true;
                }
                else {
                    if (COL(i) != 0 && i + 7 == sq)
                        return true;
                    if (COL(i) != 7 && i + 9 == sq)
                        return true;
                }
            }
            else
                for (j = 0; j < offsets_[piece_[i]]; ++j)
                    for (n = i;;) {
                        n = mailbox_[mailbox64_[n] + offset_[piece_[i]][j]];
                        if (n == -1)
                            break;
                        if (n == sq)
                            return true;
                        if (color_[n] != EMPTY)
                            break;
                        if (!slide_[piece_[i]])
                            break;
                    }
        }
    return false;
}
//-------------------------------------------------------------------
// Generate() generates pseudo-legal moves for the current position.
// It scans the board to find friendly pieces and then determines
// what squares they Attack. When it finds a piece_/square
// combination, it calls GeneratePush to put the move on the "move
// stack."
void ChessEngine::Generate()
{
    int i, j, n;

    /* so far, we have no moves for the current ply_ */
    first_move_[ply_ + 1] = first_move_[ply_];

    for (i = 0; i < 64; ++i)
        if (color_[i] == side_) {
            if (piece_[i] == PAWN) {
                if (side_ == LIGHT) {
                    if (COL(i) != 0 && color_[i - 9] == DARK)
                        GeneratePush(i, i - 9, 17);
                    if (COL(i) != 7 && color_[i - 7] == DARK)
                        GeneratePush(i, i - 7, 17);
                    if (color_[i - 8] == EMPTY) {
                        GeneratePush(i, i - 8, 16);
                        if (i >= 48 && color_[i - 16] == EMPTY)
                            GeneratePush(i, i - 16, 24);
                    }
                }
                else {
                    if (COL(i) != 0 && color_[i + 7] == LIGHT)
                        GeneratePush(i, i + 7, 17);
                    if (COL(i) != 7 && color_[i + 9] == LIGHT)
                        GeneratePush(i, i + 9, 17);
                    if (color_[i + 8] == EMPTY) {
                        GeneratePush(i, i + 8, 16);
                        if (i <= 15 && color_[i + 16] == EMPTY)
                            GeneratePush(i, i + 16, 24);
                    }
                }
            }
            else
                for (j = 0; j < offsets_[piece_[i]]; ++j)
                    for (n = i;;) {
                        n = mailbox_[mailbox64_[n] + offset_[piece_[i]][j]];
                        if (n == -1)
                            break;
                        if (color_[n] != EMPTY) {
                            if (color_[n] == xside_)
                                GeneratePush(i, n, 1);
                            break;
                        }
                        GeneratePush(i, n, 0);
                        if (!slide_[piece_[i]])
                            break;
                    }
        }

    /* generate castle_ moves */
    if (side_ == LIGHT) {
        if (castle_ & 1)
            GeneratePush(E1, G1, 2);
        if (castle_ & 2)
            GeneratePush(E1, C1, 2);
    }
    else {
        if (castle_ & 4)
            GeneratePush(E8, G8, 2);
        if (castle_ & 8)
            GeneratePush(E8, C8, 2);
    }

    /* generate en passant moves */
    if (ep_ != -1) {
        if (side_ == LIGHT) {
            if (COL(ep_) != 0 && color_[ep_ + 7] == LIGHT && piece_[ep_ + 7] == PAWN)
                GeneratePush(ep_ + 7, ep_, 21);
            if (COL(ep_) != 7 && color_[ep_ + 9] == LIGHT && piece_[ep_ + 9] == PAWN)
                GeneratePush(ep_ + 9, ep_, 21);
        }
        else {
            if (COL(ep_) != 0 && color_[ep_ - 9] == DARK && piece_[ep_ - 9] == PAWN)
                GeneratePush(ep_ - 9, ep_, 21);
            if (COL(ep_) != 7 && color_[ep_ - 7] == DARK && piece_[ep_ - 7] == PAWN)
                GeneratePush(ep_ - 7, ep_, 21);
        }
    }
}
//-------------------------------------------------------------------
// GenerateCaptures() is basically a copy of Generate() that's modified to
// only generate capture and promote moves. It's used by the
// quiescence search.
void ChessEngine::GenerateCaptures()
{
    int i, j, n;

    first_move_[ply_ + 1] = first_move_[ply_];
    for (i = 0; i < 64; ++i)
        if (color_[i] == side_) {
            if (piece_[i]==PAWN) {
                if (side_ == LIGHT) {
                    if (COL(i) != 0 && color_[i - 9] == DARK)
                        GeneratePush(i, i - 9, 17);
                    if (COL(i) != 7 && color_[i - 7] == DARK)
                        GeneratePush(i, i - 7, 17);
                    if (i <= 15 && color_[i - 8] == EMPTY)
                        GeneratePush(i, i - 8, 16);
                }
                if (side_ == DARK) {
                    if (COL(i) != 0 && color_[i + 7] == LIGHT)
                        GeneratePush(i, i + 7, 17);
                    if (COL(i) != 7 && color_[i + 9] == LIGHT)
                        GeneratePush(i, i + 9, 17);
                    if (i >= 48 && color_[i + 8] == EMPTY)
                        GeneratePush(i, i + 8, 16);
                }
            }
            else
                for (j = 0; j < offsets_[piece_[i]]; ++j)
                    for (n = i;;) {
                        n = mailbox_[mailbox64_[n] + offset_[piece_[i]][j]];
                        if (n == -1)
                            break;
                        if (color_[n] != EMPTY) {
                            if (color_[n] == xside_)
                                GeneratePush(i, n, 1);
                            break;
                        }
                        if (!slide_[piece_[i]])
                            break;
                    }
        }
    if (ep_ != -1) {
        if (side_ == LIGHT) {
            if (COL(ep_) != 0 && color_[ep_ + 7] == LIGHT && piece_[ep_ + 7] == PAWN)
                GeneratePush(ep_ + 7, ep_, 21);
            if (COL(ep_) != 7 && color_[ep_ + 9] == LIGHT && piece_[ep_ + 9] == PAWN)
                GeneratePush(ep_ + 9, ep_, 21);
        }
        else {
            if (COL(ep_) != 0 && color_[ep_ - 9] == DARK && piece_[ep_ - 9] == PAWN)
                GeneratePush(ep_ - 9, ep_, 21);
            if (COL(ep_) != 7 && color_[ep_ - 7] == DARK && piece_[ep_ - 7] == PAWN)
                GeneratePush(ep_ - 7, ep_, 21);
        }
    }
}
//-------------------------------------------------------------------
// GeneratePush() puts a move on the move stack, unless it's a
// pawn promotion that needs to be handled by GeneratePromote().
// It also assigns a score to the move for alpha-beta move
// ordering. If the move is a capture, it uses MVV/LVA
// (Most Valuable Victim/Least Valuable Attacker). Otherwise,
// it uses the move's history_ heuristic value. Note that
// 1,000,000 is added to a capture move's score, so it
// always gets ordered above a "normal" move.
void ChessEngine::GeneratePush(int from, int to, int bits)
{
    gen_t *g;

    if (bits & 16) {
        if (side_ == LIGHT) {
            if (to <= H8) {
                GeneratePromote(from, to, bits);
                return;
            }
        }
        else {
            if (to >= A1) {
                GeneratePromote(from, to, bits);
                return;
            }
        }
    }
    g = &gen_dat_[first_move_[ply_ + 1]++];
    g->m.b.from = (char)from;
    g->m.b.to = (char)to;
    g->m.b.promote = 0;
    g->m.b.bits = (char)bits;
    if (color_[to] != EMPTY)
        g->score = 1000000 + (piece_[to] * 10) - piece_[from];
    else
        g->score = history_[from][to];
}
//-------------------------------------------------------------------
// GeneratePromote() is just like GeneratePush(), only it puts 4 moves
// on the move stack, one for each possible promotion piece_
void ChessEngine::GeneratePromote(int from, int to, int bits)
{
    int i;
    gen_t *g;

    for (i = KNIGHT; i <= QUEEN; ++i) {
        g = &gen_dat_[first_move_[ply_ + 1]++];
        g->m.b.from = (char)from;
        g->m.b.to = (char)to;
        g->m.b.promote = (char)i;
        g->m.b.bits = (char)(bits | 32);
        g->score = 1000000 + (i * 10);
    }
}
//-------------------------------------------------------------------
// MakeMove() makes a move. If the move is illegal, it
//   undoes whatever it did and returns FALSE. Otherwise, it
//   returns TRUE.
bool ChessEngine::MakeMove(move_bytes m)
{
    /* test to see if a castle_ move is legal and move the rook
       (the king is moved with the usual move code later) */
    if (m.bits & 2) {
        int from, to;

        if (InCheck(side_))
            return false;
        switch (m.to) {
            case 62:
                if (color_[F1] != EMPTY || color_[G1] != EMPTY ||
                        Attack(F1, xside_) || Attack(G1, xside_))
                    return false;
                from = H1;
                to = F1;
                break;
            case 58:
                if (color_[B1] != EMPTY || color_[C1] != EMPTY || color_[D1] != EMPTY ||
                        Attack(C1, xside_) || Attack(D1, xside_))
                    return false;
                from = A1;
                to = D1;
                break;
            case 6:
                if (color_[F8] != EMPTY || color_[G8] != EMPTY ||
                        Attack(F8, xside_) || Attack(G8, xside_))
                    return false;
                from = H8;
                to = F8;
                break;
            case 2:
                if (color_[B8] != EMPTY || color_[C8] != EMPTY || color_[D8] != EMPTY ||
                        Attack(C8, xside_) || Attack(D8, xside_))
                    return false;
                from = A8;
                to = D8;
                break;
            default:  /* shouldn't get here */
                from = -1;
                to = -1;
                break;
        }
        color_[to] = color_[from];
        piece_[to] = piece_[from];
        color_[from] = EMPTY;
        piece_[from] = EMPTY;
    }

    /* back up information so we can take the move back later. */
    hist_dat_[hply_].m.b = m;
    hist_dat_[hply_].capture = piece_[(int)m.to];
    hist_dat_[hply_].castle_ = castle_;
    hist_dat_[hply_].ep_ = ep_;
    hist_dat_[hply_].fifty_ = fifty_;
    hist_dat_[hply_].hash_ = hash_;
    ++ply_;
    ++hply_;

    /* update the castle_, en passant, and
       fifty_-move-draw variables */
    castle_ &= castle_mask_[(int)m.from] & castle_mask_[(int)m.to];
    if (m.bits & 8) {
        if (side_ == LIGHT)
            ep_ = m.to + 8;
        else
            ep_ = m.to - 8;
    }
    else
        ep_ = -1;
    if (m.bits & 17)
        fifty_ = 0;
    else
        ++fifty_;

    /* move the piece_ */
    color_[(int)m.to] = side_;
    if (m.bits & 32)
        piece_[(int)m.to] = m.promote;
    else
        piece_[(int)m.to] = piece_[(int)m.from];
    color_[(int)m.from] = EMPTY;
    piece_[(int)m.from] = EMPTY;

    /* erase the pawn if this is an en passant move */
    if (m.bits & 4) {
        if (side_ == LIGHT) {
            color_[m.to + 8] = EMPTY;
            piece_[m.to + 8] = EMPTY;
        }
        else {
            color_[m.to - 8] = EMPTY;
            piece_[m.to - 8] = EMPTY;
        }
    }

    /* switch sides and test for legality (if we can capture
       the other guy's king, it's an illegal position and
       we need to take the move back) */
    side_ ^= 1;
    xside_ ^= 1;
    if (InCheck(xside_)) {
        UnMakeMove();
        return false;
    }
    SetHash();
    return true;
}
//-------------------------------------------------------------------
// UnMakeMove() is very similar to MakeMove(), only backwards :)
void ChessEngine::UnMakeMove()
{
    move_bytes m;

    side_ ^= 1;
    xside_ ^= 1;
    --ply_;
    --hply_;
    m = hist_dat_[hply_].m.b;
    castle_ = hist_dat_[hply_].castle_;
    ep_ = hist_dat_[hply_].ep_;
    fifty_ = hist_dat_[hply_].fifty_;
    hash_ = hist_dat_[hply_].hash_;
    color_[(int)m.from] = side_;
    if (m.bits & 32)
        piece_[(int)m.from] = PAWN;
    else
        piece_[(int)m.from] = piece_[(int)m.to];
    if (hist_dat_[hply_].capture == EMPTY) {
        color_[(int)m.to] = EMPTY;
        piece_[(int)m.to] = EMPTY;
    }
    else {
        color_[(int)m.to] = xside_;
        piece_[(int)m.to] = hist_dat_[hply_].capture;
    }
    if (m.bits & 2) {
        int from, to;

        switch(m.to) {
            case 62:
                from = F1;
                to = H1;
                break;
            case 58:
                from = D1;
                to = A1;
                break;
            case 6:
                from = F8;
                to = H8;
                break;
            case 2:
                from = D8;
                to = A8;
                break;
            default:  /* shouldn't get here */
                from = -1;
                to = -1;
                break;
        }
        color_[to] = side_;
        piece_[to] = ROOK;
        color_[from] = EMPTY;
        piece_[from] = EMPTY;
    }
    if (m.bits & 4) {
        if (side_ == LIGHT) {
            color_[m.to + 8] = xside_;
            piece_[m.to + 8] = PAWN;
        }
        else {
            color_[m.to - 8] = xside_;
            piece_[m.to - 8] = PAWN;
        }
    }
}
//-------------------------------------------------------------------
/////////////////////////////////////////////////////////////////
/// SEARCH
/////////////////////////////////////////////////////////////////
//-------------------------------------------------------------------
// Think() calls Search() iteratively. Search statistics
// are printed depending on the value of output:
// 0 = no output
// 1 = normal output
// 2 = xboard format output
void ChessEngine::Think(int output)
{
    int i, j, x;

    /* try the opening book first */
    pv_[0][0].u = BookMove();
    if (pv_[0][0].u != -1)
        return;

    /* some code that lets us longjmp back here and return
       from Think() when our time is up */
    stop_search_ = false;
    setjmp(env_);
    if (stop_search_) {

        /* make sure to take back the line we were searching */
        while (ply_)
            UnMakeMove();
        return;
    }

    start_time_ = GetMS();
    stop_time_ = start_time_ + max_time_;

    ply_ = 0;
    nodes_ = 0;

    memset(pv_, 0, sizeof(pv_));
    memset(history_, 0, sizeof(history_));
    if (output == 1)
        printf("ply_      nodes_  score  pv_\n");
    for (i = 1; i <= max_depth_; ++i) {
        follow_pv_ = true;
        x = Search(-10000, 10000, i);
        if (output == 1)
            printf("%3d  %9d  %5d ", i, nodes_, x);
        else if (output == 2)
            printf("%d %d %d %d",
                    i, x, (GetMS() - start_time_) / 10, nodes_);
        if (output) {
            for (j = 0; j < pv_length_[0]; ++j)
                printf(" %s", MoveStr(pv_[0][j].b));
            printf("\n");
            fflush(stdout);
        }
        if (x > 9000 || x < -9000)
            break;
    }
}
//-------------------------------------------------------------------
// Search() does just that, in negamax fashion
int ChessEngine::Search(int alpha, int beta, int depth)
{
    int i, j, x;
    bool c, f;

    /* we're as deep as we want to be; call Quiesce() to get
       a reasonable score and return it. */
    if (!depth)
        return Quiesce(alpha,beta);
    ++nodes_;

    /* do some housekeeping every 1024 nodes_ */
    if ((nodes_ & 1023) == 0)
        Checkup();

    pv_length_[ply_] = ply_;

    /* if this isn't the root of the Search tree (where we have
       to pick a move and can't simply return 0) then check to
       see if the position is a repeat. if so, we can assume that
       this line is a draw and return 0. */
    if (ply_ && Repetitions())
        return 0;

    /* are we too deep? */
    if (ply_ >= MAX_PLY - 1)
        return Evaluate();
    if (hply_ >= HIST_STACK - 1)
        return Evaluate();

    /* are we in check? if so, we want to Search deeper */
    c = InCheck(side_);
    if (c)
        ++depth;
    Generate();
    if (follow_pv_)  /* are we following the PV? */
        SortPrincipalVariation();
    f = false;

    /* loop through the moves */
    for (i = first_move_[ply_]; i < first_move_[ply_ + 1]; ++i) {
        Sort(i);
        if (!MakeMove(gen_dat_[i].m.b))
            continue;
        f = true;
        x = -Search(-beta, -alpha, depth - 1);
        UnMakeMove();
        if (x > alpha) {

            /* this move caused a cutoff, so increase the history_
               value so it gets ordered high next time we can
               Search it */
            history_[(int)gen_dat_[i].m.b.from][(int)gen_dat_[i].m.b.to] += depth;
            if (x >= beta)
                return beta;
            alpha = x;

            /* update the PV */
            pv_[ply_][ply_] = gen_dat_[i].m;
            for (j = ply_ + 1; j < pv_length_[ply_ + 1]; ++j)
                pv_[ply_][j] = pv_[ply_ + 1][j];
            pv_length_[ply_] = pv_length_[ply_ + 1];
        }
    }

    /* no legal moves? then we're in checkmate or stalemate */
    if (!f) {
        if (c)
            return -10000 + ply_;
        else
            return 0;
    }

    /* fifty_ move draw rule */
    if (fifty_ >= 100)
        return 0;
    return alpha;
}
//-------------------------------------------------------------------
// Quiesce() is a recursive minimax Search function with
// alpha-beta cutoffs. In other words, negamax. It basically
// only searches capture sequences and allows the evaluation
// function to cut the Search off (and set alpha). The idea
// is to find a position where there isn't a lot going on
// so the static evaluation function will work.
//-------------------------------------------------------------------
int ChessEngine::Quiesce(int alpha,int beta)
{
    int i, j, x;

    ++nodes_;

    /* do some housekeeping every 1024 nodes_ */
    if ((nodes_ & 1023) == 0)
        Checkup();

    pv_length_[ply_] = ply_;

    /* are we too deep? */
    if (ply_ >= MAX_PLY - 1)
        return Evaluate();
    if (hply_ >= HIST_STACK - 1)
        return Evaluate();

    /* check with the evaluation function */
    x = Evaluate();
    if (x >= beta)
        return beta;
    if (x > alpha)
        alpha = x;

    GenerateCaptures();
    if (follow_pv_)  /* are we following the PV? */
        SortPrincipalVariation();

    /* loop through the moves */
    for (i = first_move_[ply_]; i < first_move_[ply_ + 1]; ++i) {
        Sort(i);
        if (!MakeMove(gen_dat_[i].m.b))
            continue;
        x = -Quiesce(-beta, -alpha);
        UnMakeMove();
        if (x > alpha) {
            if (x >= beta)
                return beta;
            alpha = x;

            /* update the PV */
            pv_[ply_][ply_] = gen_dat_[i].m;
            for (j = ply_ + 1; j < pv_length_[ply_ + 1]; ++j)
                pv_[ply_][j] = pv_[ply_ + 1][j];
            pv_length_[ply_] = pv_length_[ply_ + 1];
        }
    }
    return alpha;
}
//-------------------------------------------------------------------
// Repetitions() returns the number of times the current position
// has been repeated. It compares the current value of hash_
// to previous values.
int ChessEngine::Repetitions()
{
    int i;
    int r = 0;

    for (i = hply_ - fifty_; i < hply_; ++i)
        if (hist_dat_[i].hash_ == hash_)
            ++r;
    return r;
}
//-------------------------------------------------------------------
// SortPrincipalVariation() is called when the Search function is following
// the PV (Principal Variation). It looks through the current
// ply_'s move list to see if the PV move is there. If so,
// it adds 10,000,000 to the move's score so it's played first
// by the Search function. If not, follow_pv_ remains FALSE and
// Search() stops calling SortPrincipalVariation().
void ChessEngine::SortPrincipalVariation()
{
    int i;

    follow_pv_ = false;
    for(i = first_move_[ply_]; i < first_move_[ply_ + 1]; ++i)
        if (gen_dat_[i].m.u == pv_[0][ply_].u) {
            follow_pv_ = true;
            gen_dat_[i].score += 10000000;
            return;
        }
}
//-------------------------------------------------------------------
// Sort() searches the current ply_'s move list from 'from'
// to the end to find the move with the highest score. Then it
// swaps that move and the 'from' move so the move with the
// highest score gets searched next, and hopefully produces
// a cutoff.
void ChessEngine::Sort(int from)
{
    int i;
    int bs;  /* best score */
    int bi;  /* best i */
    gen_t g;

    bs = -1;
    bi = from;
    for (i = from; i < first_move_[ply_ + 1]; ++i)
        if (gen_dat_[i].score > bs) {
            bs = gen_dat_[i].score;
            bi = i;
        }
    g = gen_dat_[from];
    gen_dat_[from] = gen_dat_[bi];
    gen_dat_[bi] = g;
}
//-------------------------------------------------------------------
// Checkup() is called once in a while during the Search.
void ChessEngine::Checkup()
{
    /* is the engine's time up? if so, longjmp back to the
       beginning of Think() */
    if (GetMS() >= stop_time_) {
        stop_search_ = true;
        longjmp(env_, 0);

    }
}
//-------------------------------------------------------------------
/////////////////////////////////////////////////////////////////
/// EVALUATION
/////////////////////////////////////////////////////////////////
//-------------------------------------------------------------------
// the values of the pieces */
int ChessEngine::piece_value_[6] = {
    100, 300, 300, 500, 900, 0
};
//-------------------------------------------------------------------
// The "pcsq" arrays are piece_/square tables. They're values
// added to the material value of the piece_ based on the
// location of the piece_.
int ChessEngine::pawn_pcsq_[64] = {
      0,   0,   0,   0,   0,   0,   0,   0,
      5,  10,  15,  20,  20,  15,  10,   5,
      4,   8,  12,  16,  16,  12,   8,   4,
      3,   6,   9,  12,  12,   9,   6,   3,
      2,   4,   6,   8,   8,   6,   4,   2,
      1,   2,   3, -10, -10,   3,   2,   1,
      0,   0,   0, -40, -40,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0
};
//-------------------------------------------------------------------
int ChessEngine::knight_pcsq_[64] = {
    -10, -10, -10, -10, -10, -10, -10, -10,
    -10,   0,   0,   0,   0,   0,   0, -10,
    -10,   0,   5,   5,   5,   5,   0, -10,
    -10,   0,   5,  10,  10,   5,   0, -10,
    -10,   0,   5,  10,  10,   5,   0, -10,
    -10,   0,   5,   5,   5,   5,   0, -10,
    -10,   0,   0,   0,   0,   0,   0, -10,
    -10, -30, -10, -10, -10, -10, -30, -10
};
//-------------------------------------------------------------------
int ChessEngine::bishop_pcsq_[64] = {
    -10, -10, -10, -10, -10, -10, -10, -10,
    -10,   0,   0,   0,   0,   0,   0, -10,
    -10,   0,   5,   5,   5,   5,   0, -10,
    -10,   0,   5,  10,  10,   5,   0, -10,
    -10,   0,   5,  10,  10,   5,   0, -10,
    -10,   0,   5,   5,   5,   5,   0, -10,
    -10,   0,   0,   0,   0,   0,   0, -10,
    -10, -10, -20, -10, -10, -20, -10, -10
};
//-------------------------------------------------------------------
int ChessEngine::king_pcsq_[64] = {
    -40, -40, -40, -40, -40, -40, -40, -40,
    -40, -40, -40, -40, -40, -40, -40, -40,
    -40, -40, -40, -40, -40, -40, -40, -40,
    -40, -40, -40, -40, -40, -40, -40, -40,
    -40, -40, -40, -40, -40, -40, -40, -40,
    -40, -40, -40, -40, -40, -40, -40, -40,
    -20, -20, -20, -20, -20, -20, -20, -20,
      0,  20,  40, -20,   0, -20,  40,  20
};
//-------------------------------------------------------------------
int ChessEngine::king_endgame_pcsq_[64] = {
      0,  10,  20,  30,  30,  20,  10,   0,
     10,  20,  30,  40,  40,  30,  20,  10,
     20,  30,  40,  50,  50,  40,  30,  20,
     30,  40,  50,  60,  60,  50,  40,  30,
     30,  40,  50,  60,  60,  50,  40,  30,
     20,  30,  40,  50,  50,  40,  30,  20,
     10,  20,  30,  40,  40,  30,  20,  10,
      0,  10,  20,  30,  30,  20,  10,   0
};
//-------------------------------------------------------------------
// The flip_ array is used to calculate the piece_/square
// values for DARK pieces. The piece_/square value of a
// LIGHT pawn is pawn_pcsq_[sq] and the value of a DARK
// pawn is pawn_pcsq_[flip_[sq]]
int ChessEngine::flip_[64] = {
     56,  57,  58,  59,  60,  61,  62,  63,
     48,  49,  50,  51,  52,  53,  54,  55,
     40,  41,  42,  43,  44,  45,  46,  47,
     32,  33,  34,  35,  36,  37,  38,  39,
     24,  25,  26,  27,  28,  29,  30,  31,
     16,  17,  18,  19,  20,  21,  22,  23,
      8,   9,  10,  11,  12,  13,  14,  15,
      0,   1,   2,   3,   4,   5,   6,   7
};
//-------------------------------------------------------------------
int ChessEngine::Evaluate()
{
    int i;
    int f;  /* file */
    int score[2];  /* each side_'s score */

    /* this is the first pass: set up pawn_rank_, piece_mat_, and pawn_mat_. */
    for (i = 0; i < 10; ++i) {
        pawn_rank_[LIGHT][i] = 0;
        pawn_rank_[DARK][i] = 7;
    }
    piece_mat_[LIGHT] = 0;
    piece_mat_[DARK] = 0;
    pawn_mat_[LIGHT] = 0;
    pawn_mat_[DARK] = 0;
    for (i = 0; i < 64; ++i) {
        if (color_[i] == EMPTY)
            continue;
        if (piece_[i] == PAWN) {
            pawn_mat_[color_[i]] += piece_value_[PAWN];
            f = COL(i) + 1;  /* add 1 because of the extra file in the array */
            if (color_[i] == LIGHT) {
                if (pawn_rank_[LIGHT][f] < ROW(i))
                    pawn_rank_[LIGHT][f] = ROW(i);
            }
            else {
                if (pawn_rank_[DARK][f] > ROW(i))
                    pawn_rank_[DARK][f] = ROW(i);
            }
        }
        else
            piece_mat_[color_[i]] += piece_value_[piece_[i]];
    }

    /* this is the second pass: evaluate each piece_ */
    score[LIGHT] = piece_mat_[LIGHT] + pawn_mat_[LIGHT];
    score[DARK] = piece_mat_[DARK] + pawn_mat_[DARK];
    for (i = 0; i < 64; ++i) {
        if (color_[i] == EMPTY)
            continue;
        if (color_[i] == LIGHT) {
            switch (piece_[i]) {
                case PAWN:
                    score[LIGHT] += EvaluateLightPawn(i);
                    break;
                case KNIGHT:
                    score[LIGHT] += knight_pcsq_[i];
                    break;
                case BISHOP:
                    score[LIGHT] += bishop_pcsq_[i];
                    break;
                case ROOK:
                    if (pawn_rank_[LIGHT][COL(i) + 1] == 0) {
                        if (pawn_rank_[DARK][COL(i) + 1] == 7)
                            score[LIGHT] += ROOK_OPEN_FILE_BONUS;
                        else
                            score[LIGHT] += ROOK_SEMI_OPEN_FILE_BONUS;
                    }
                    if (ROW(i) == 1)
                        score[LIGHT] += ROOK_ON_SEVENTH_BONUS;
                    break;
                case KING:
                    if (piece_mat_[DARK] <= 1200)
                        score[LIGHT] += king_endgame_pcsq_[i];
                    else
                        score[LIGHT] += EvaluateLightKing(i);
                    break;
            }
        }
        else {
            switch (piece_[i]) {
                case PAWN:
                    score[DARK] += EvaluateDarkPawn(i);
                    break;
                case KNIGHT:
                    score[DARK] += knight_pcsq_[flip_[i]];
                    break;
                case BISHOP:
                    score[DARK] += bishop_pcsq_[flip_[i]];
                    break;
                case ROOK:
                    if (pawn_rank_[DARK][COL(i) + 1] == 7) {
                        if (pawn_rank_[LIGHT][COL(i) + 1] == 0)
                            score[DARK] += ROOK_OPEN_FILE_BONUS;
                        else
                            score[DARK] += ROOK_SEMI_OPEN_FILE_BONUS;
                    }
                    if (ROW(i) == 6)
                        score[DARK] += ROOK_ON_SEVENTH_BONUS;
                    break;
                case KING:
                    if (piece_mat_[LIGHT] <= 1200)
                        score[DARK] += king_endgame_pcsq_[flip_[i]];
                    else
                        score[DARK] += EvaluateDarkKing(i);
                    break;
            }
        }
    }

    /* the score[] array is set, now return the score relative
       to the side_ to move */
    if (side_ == LIGHT)
        return score[LIGHT] - score[DARK];
    return score[DARK] - score[LIGHT];
}
//-------------------------------------------------------------------
int ChessEngine::EvaluateLightPawn(int sq)
{
    int r;  /* the value to return */
    int f;  /* the pawn's file */

    r = 0;
    f = COL(sq) + 1;

    r += pawn_pcsq_[sq];

    /* if there's a pawn behind this one, it's doubled */
    if (pawn_rank_[LIGHT][f] > ROW(sq))
        r -= DOUBLED_PAWN_PENALTY;

    /* if there aren't any friendly pawns on either side_ of
       this one, it's isolated */
    if ((pawn_rank_[LIGHT][f - 1] == 0) &&
            (pawn_rank_[LIGHT][f + 1] == 0))
        r -= ISOLATED_PAWN_PENALTY;

    /* if it's not isolated, it might be backwards */
    else if ((pawn_rank_[LIGHT][f - 1] < ROW(sq)) &&
            (pawn_rank_[LIGHT][f + 1] < ROW(sq)))
        r -= BACKWARDS_PAWN_PENALTY;

    /* add a bonus if the pawn is passed */
    if ((pawn_rank_[DARK][f - 1] >= ROW(sq)) &&
            (pawn_rank_[DARK][f] >= ROW(sq)) &&
            (pawn_rank_[DARK][f + 1] >= ROW(sq)))
        r += (7 - ROW(sq)) * PASSED_PAWN_BONUS;

    return r;
}
//-------------------------------------------------------------------
int ChessEngine::EvaluateDarkPawn(int sq)
{
    int r;  /* the value to return */
    int f;  /* the pawn's file */

    r = 0;
    f = COL(sq) + 1;

    r += pawn_pcsq_[flip_[sq]];

    /* if there's a pawn behind this one, it's doubled */
    if (pawn_rank_[DARK][f] < ROW(sq))
        r -= DOUBLED_PAWN_PENALTY;

    /* if there aren't any friendly pawns on either side_ of
       this one, it's isolated */
    if ((pawn_rank_[DARK][f - 1] == 7) &&
            (pawn_rank_[DARK][f + 1] == 7))
        r -= ISOLATED_PAWN_PENALTY;

    /* if it's not isolated, it might be backwards */
    else if ((pawn_rank_[DARK][f - 1] > ROW(sq)) &&
            (pawn_rank_[DARK][f + 1] > ROW(sq)))
        r -= BACKWARDS_PAWN_PENALTY;

    /* add a bonus if the pawn is passed */
    if ((pawn_rank_[LIGHT][f - 1] <= ROW(sq)) &&
            (pawn_rank_[LIGHT][f] <= ROW(sq)) &&
            (pawn_rank_[LIGHT][f + 1] <= ROW(sq)))
        r += ROW(sq) * PASSED_PAWN_BONUS;

    return r;
}
//-------------------------------------------------------------------
int ChessEngine::EvaluateLightKing(int sq)
{
    int r;  /* the value to return */
    int i;

    r = king_pcsq_[sq];

    /* if the king is castled, use a special function to evaluate the
       pawns on the appropriate side_ */
    if (COL(sq) < 3) {
        r += EvaluatelLkp(1);
        r += EvaluatelLkp(2);
        r += EvaluatelLkp(3) / 2;  /* problems with pawns on the c & f files
                                  are not as severe */
    }
    else if (COL(sq) > 4) {
        r += EvaluatelLkp(8);
        r += EvaluatelLkp(7);
        r += EvaluatelLkp(6) / 2;
    }

    /* otherwise, just assess a penalty if there are open files near
       the king */
    else {
        for (i = COL(sq); i <= COL(sq) + 2; ++i)
            if ((pawn_rank_[LIGHT][i] == 0) &&
                    (pawn_rank_[DARK][i] == 7))
                r -= 10;
    }

    /* scale the king safety value according to the opponent's material;
       the premise is that your king safety can only be bad if the
       opponent has enough pieces to Attack you */
    r *= piece_mat_[DARK];
    r /= 3100;

    return r;
}
//-------------------------------------------------------------------
// EvaluatelLkp(f) evaluates the Light King Pawn on file f
int ChessEngine::EvaluatelLkp(int f)
{
    int r = 0;

    if (pawn_rank_[LIGHT][f] == 6);  /* pawn hasn't moved */
    else if (pawn_rank_[LIGHT][f] == 5)
        r -= 10;  /* pawn moved one square */
    else if (pawn_rank_[LIGHT][f] != 0)
        r -= 20;  /* pawn moved more than one square */
    else
        r -= 25;  /* no pawn on this file */

    if (pawn_rank_[DARK][f] == 7)
        r -= 15;  /* no enemy pawn */
    else if (pawn_rank_[DARK][f] == 5)
        r -= 10;  /* enemy pawn on the 3rd rank */
    else if (pawn_rank_[DARK][f] == 4)
        r -= 5;   /* enemy pawn on the 4th rank */

    return r;
}
//-------------------------------------------------------------------
int ChessEngine::EvaluateDarkKing(int sq)
{
    int r;
    int i;

    r = king_pcsq_[flip_[sq]];
    if (COL(sq) < 3) {
        r += EvaluateDkp(1);
        r += EvaluateDkp(2);
        r += EvaluateDkp(3) / 2;
    }
    else if (COL(sq) > 4) {
        r += EvaluateDkp(8);
        r += EvaluateDkp(7);
        r += EvaluateDkp(6) / 2;
    }
    else {
        for (i = COL(sq); i <= COL(sq) + 2; ++i)
            if ((pawn_rank_[LIGHT][i] == 0) &&
                    (pawn_rank_[DARK][i] == 7))
                r -= 10;
    }
    r *= piece_mat_[LIGHT];
    r /= 3100;
    return r;
}
//-------------------------------------------------------------------
int ChessEngine::EvaluateDkp(int f)
{
    int r = 0;

    if (pawn_rank_[DARK][f] == 1);
    else if (pawn_rank_[DARK][f] == 2)
        r -= 10;
    else if (pawn_rank_[DARK][f] != 7)
        r -= 20;
    else
        r -= 25;

    if (pawn_rank_[LIGHT][f] == 0)
        r -= 15;
    else if (pawn_rank_[LIGHT][f] == 2)
        r -= 10;
    else if (pawn_rank_[LIGHT][f] == 3)
        r -= 5;

    return r;
}
//-------------------------------------------------------------------
int ChessEngine::GetMS()
{
    struct timeb timebuffer;
    ftime(&timebuffer);
    if (timebuffer.millitm != 0)
        ftime_ok_ = true;
    return (timebuffer.time * 1000) + timebuffer.millitm;
}
/////////////////////////////////////////////////////////////////
/// BOOK
/////////////////////////////////////////////////////////////////
//-------------------------------------------------------------------
// OpenBook() opens the opening book file and initializes the random number
// generator so we play random book moves.
void ChessEngine::OpenBook()
{
    srand(time(NULL));
    book_file_ = fopen("book.txt", "r");
    if (!book_file_)
        printf("Opening book missing.\n");
}
//-------------------------------------------------------------------
// CloseBook() closes the book file. This is called when the program exits.
void ChessEngine::CloseBook()
{
    if (book_file_)
        fclose(book_file_);
    book_file_ = NULL;
}
//-------------------------------------------------------------------
// BookMove() returns a book move (in integer format) or -1 if there is no
// book move.
int ChessEngine::BookMove()
{
    char line[256];
    char book_line[256];
    int i, j, m;
    int move[50];  /* the possible book moves */
    int count[50];  /* the number of occurrences of each move */
    int moves = 0;
    int total_count = 0;

    if (!book_file_ || hply_ > 25)
        return -1;

    /* line is a string with the current line, e.g., "e2e4 e7e5 g1f3 " */
    line[0] = '\0';
    j = 0;
    for (i = 0; i < hply_; ++i)
        j += sprintf(line + j, "%s ", MoveStr(hist_dat_[i].m.b));

    /* compare line to each line in the opening book */
    fseek(book_file_, 0, SEEK_SET);
    while (fgets(book_line, 256, book_file_)) {
        if (BookMatch(line, book_line)) {

            /* parse the book move that continues the line */
            m = ParseMove(&book_line[strlen(line)]);
            if (m == -1)
                continue;
            m = gen_dat_[m].m.u;

            /* add the book move to the move list, or update the move's
               count */
            for (j = 0; j < moves; ++j)
                if (move[j] == m) {
                    ++count[j];
                    break;
                }
            if (j == moves) {
                move[moves] = m;
                count[moves] = 1;
                ++moves;
            }
            ++total_count;
        }
    }

    /* no book moves? */
    if (moves == 0)
        return -1;

    /* Think of total_count as the set of matching book lines.
       Randomly pick one of those lines (j) and figure out which
       move j "corresponds" to. */
    j = rand() % total_count;
    for (i = 0; i < moves; ++i) {
        j -= count[i];
        if (j < 0)
            return move[i];
    }
    return -1;  /* shouldn't get here */
}
//-------------------------------------------------------------------
// BookMatch() returns TRUE if the first part of s2 matches s1.
bool ChessEngine::BookMatch(char *s1, char *s2)
{
    int i;

    for (i = 0; i < (signed int)strlen(s1); ++i)
        if (s2[i] == '\0' || s2[i] != s1[i])
            return false;
    return true;
}
//-------------------------------------------------------------------
/////////////////////////////////////////////////////////////////
/// OUTPUT
/////////////////////////////////////////////////////////////////
char *ChessEngine::MoveStr(move_bytes m)
{
    static char str[6];

    char c;

    if (m.bits & 32) {
        switch (m.promote) {
            case KNIGHT:
                c = 'n';
                break;
            case BISHOP:
                c = 'b';
                break;
            case ROOK:
                c = 'r';
                break;
            default:
                c = 'q';
                break;
        }
        sprintf(str, "%c%d%c%d%c",
                COL(m.from) + 'a',
                8 - ROW(m.from),
                COL(m.to) + 'a',
                8 - ROW(m.to),
                c);
    }
    else
        sprintf(str, "%c%d%c%d",
                COL(m.from) + 'a',
                8 - ROW(m.from),
                COL(m.to) + 'a',
                8 - ROW(m.to));
    return str;
}
//-------------------------------------------------------------------
// parse the move s (in coordinate notation) and return the move's
// index in gen_dat_, or -1 if the move is illegal
int ChessEngine::ParseMove(const char *s)
{
    int from, to, i;

    /* make sure the string looks like a move */
    if (s[0] < 'a' || s[0] > 'h' ||
            s[1] < '0' || s[1] > '9' ||
            s[2] < 'a' || s[2] > 'h' ||
            s[3] < '0' || s[3] > '9')
        return -1;

    from = s[0] - 'a';
    from += 8 * (8 - (s[1] - '0'));
    to = s[2] - 'a';
    to += 8 * (8 - (s[3] - '0'));

    for (i = 0; i < first_move_[1]; ++i)
        if (gen_dat_[i].m.b.from == from && gen_dat_[i].m.b.to == to) {

            /* if the move is a promotion, handle the promotion piece_;
               assume that the promotion moves occur consecutively in
               gen_dat_. */
            if (gen_dat_[i].m.b.bits & 32)
                switch (s[4]) {
                    case 'N':
                        return i;
                    case 'B':
                        return i + 1;
                    case 'R':
                        return i + 2;
                    default:  /* assume it's a queen */
                        return i + 3;
                }
            return i;
        }

    /* didn't find the move */
    return -1;
}
//-------------------------------------------------------------------
// PrintBoard() prints the board
void ChessEngine::PrintBoard()
{
    int i;
    char buf[500];
    char tmp[100];
    sprintf(buf, "\n8 ");
    for (i = 0; i < 64; ++i) {
        switch (color_[i]) {
            case EMPTY:
                strcat(buf, " .");
                break;
            case LIGHT:
                sprintf(tmp, " %c", piece_char_[piece_[i]]);
                strcat(buf, tmp);
                break;
            case DARK:
                sprintf(tmp, " %c", piece_char_[piece_[i]] + ('a' - 'A'));
                strcat(buf, tmp);
                break;
        }
        if ((i + 1) % 8 == 0 && i != 63)
        {
            sprintf(tmp, "\n%d ", 7 - ROW(i));
            strcat(buf, tmp);
        }
    }
    strcat(buf, "\n\n   a b c d e f g h\n\n");
    qDebug() << QString(buf);
    emit SendData(std::string(buf), 1);
}
//-------------------------------------------------------------------
// print_result() checks to see if the game is over, and if so,
// prints the result.
void ChessEngine::PrintResult()
{
    int i;
    std::string str = "";

    /* is there a legal move? */
    for (i = 0; i < first_move_[1]; ++i)
        if (MakeMove(gen_dat_[i].m.b)) {
            UnMakeMove();
            break;
        }
    if (i == first_move_[1]) {
        if (InCheck(side_)) {
            if (side_ == LIGHT)
            {
                 str.append("0-1 {Black mates}\n");
                 emit SendData("mate", 2);
            }
            else
            {
                 str.append("1-0 {White mates}\n");
                 emit SendData("mate", 3);
            }
        }
        else
             str.append("1/2-1/2 {Stalemate}\n");
    }
    else if (Repetitions() == 3)
         str.append("1/2-1/2 {Draw by repetition}\n");
    else if (fifty_ >= 100)
         str.append("1/2-1/2 {Draw by fifty move rule}\n");

    qDebug() << QString::fromStdString(str);
    emit SendData(str, 0);
}
//-------------------------------------------------------------------
/////////////////////////////////////////////////////////////////
