#ifndef CHESSENGINE_H
#define CHESSENGINE_H

#include <stdio.h>
#include <string.h>
#include <sys/timeb.h>
#include <setjmp.h>         //  see the beginning of think()

#include "IPVEChessEngine.h"


#define GEN_STACK		1120
#define MAX_PLY			32
#define HIST_STACK		400

#define LIGHT			0
#define DARK			1

#define PAWN			0
#define KNIGHT			1
#define BISHOP			2
#define ROOK			3
#define QUEEN			4
#define KING			5

#define EMPTY			6

/* useful squares */
#define A1				56
#define B1				57
#define C1				58
#define D1				59
#define E1				60
#define F1				61
#define G1				62
#define H1				63
#define A8				0
#define B8				1
#define C8				2
#define D8				3
#define E8				4
#define F8				5
#define G8				6
#define H8				7

#define ROW(x)			(x >> 3)
#define COL(x)			(x & 7)

/////////////////////////////////////////////////////////////////
/// EVALUATION
/////////////////////////////////////////////////////////////////
#define DOUBLED_PAWN_PENALTY		10
#define ISOLATED_PAWN_PENALTY		20
#define BACKWARDS_PAWN_PENALTY		8
#define PASSED_PAWN_BONUS			20
#define ROOK_SEMI_OPEN_FILE_BONUS	10
#define ROOK_OPEN_FILE_BONUS		15
#define ROOK_ON_SEVENTH_BONUS		20
/////////////////////////////////////////////////////////////////

/* This is the basic description of a move. promote is what
   piece_ to promote the pawn to, if the move is a pawn
   promotion. bits is a bitfield that describes the move,
   with the following bits:

   1	capture
   2	castle_
   4	en passant capture
   8	pushing a pawn 2 squares
   16	pawn move
   32	promote

   It's union'ed with an integer so two moves can easily
   be compared with each other. */

typedef struct {
    char from;
    char to;
    char promote;
    char bits;
} move_bytes;

typedef union {
    move_bytes b;
    int u;
} move;

/* an element of the move stack. it's just a move with a
   score, so it can be sorted by the Search functions. */
typedef struct {
    move m;
    int score;
} gen_t;

/* an element of the history_ stack, with the information
   necessary to take a move back. */
typedef struct {
    move m;
    int capture;
    int castle_;
    int ep_;
    int fifty_;
    int hash_;
} hist_t;

class ChessEngine : public IPVEChessEngine
{
    Q_OBJECT
    Q_INTERFACES(IPVEChessEngine)
public:
    explicit ChessEngine(QWidget *parent = 0);
    ~ChessEngine();
public:
    /////////////////////////////////////////////////////////////////
    /// IPVEChessEngine
    /////////////////////////////////////////////////////////////////
    virtual void SetupMaxDepth(unsigned int depth) override;
    virtual void SetupMaxTime(unsigned int time) override;
    virtual bool ComputerTurn(char *outMove) override;
    virtual void PrintBoardString() override;
    virtual bool OpponentTurn(const char *move) override;
    virtual void EndGame() override;
    virtual void Unmove() override;
    virtual void Init() override;
    virtual void New() override;

    virtual void Sync() override;
signals:
    void SendData(std::string str, int dataType);
    void SyncBoard(int *, int *);
private:
    /////////////////////////////////////////////////////////////////
    /// BOARD
    /////////////////////////////////////////////////////////////////
    // InitBoard() sets the board to the initial game state.
    void InitBoard();
    // Hashrand() initializes the random numbers used by SetHash().
    void InitHash();
    // HashRand() XORs some shifted random numbers together to make sure
    // we have good coverage of all 32 bits. (rand() returns 16-bit numbers
    // on some systems.)
    int HashRand();
    /* SetHash() uses the Zobrist method of generating a unique number (hash_)
       for the current chess position. Of course, there are many more chess
       positions than there are 32 bit numbers, so the numbers generated are
       not really unique, but they're unique enough for our purposes (to detect
       repetitions of the position).
       The way it works is to XOR random numbers that correspond to features of
       the position, e.g., if there's a black knight on B8, hash_ is XORed with
       hash_piece_[BLACK][KNIGHT][B8]. All of the pieces are XORed together,
       hash_side is XORed if it's black's move, and the en passant square is
       XORed if there is one. (A chess technicality is that one position can't
       be a repetition of another if the en passant state is different.) */
    void SetHash();
    // InCheck() returns TRUE if side_ s is in check and FALSE
    // otherwise. It just scans the board to find side_ s's king
    // and calls Attack() to see if it's being attacked.
    bool InCheck(int s);
    // Attack() returns TRUE if square sq is being attacked by side_
    // s and FALSE otherwise.
    bool Attack(int sq, int s);
    /* Generate() generates pseudo-legal moves for the current position.
       It scans the board to find friendly pieces and then determines
       what squares they Attack. When it finds a piece_/square
       combination, it calls GeneratePush to put the move on the "move
       stack." */
    void Generate();
    // GenerateCaptures() is basically a copy of Generate() that's modified to
    // only generate capture and promote moves. It's used by the
    // quiescence Search.
    void GenerateCaptures();
    /* GeneratePush() puts a move on the move stack, unless it's a
       pawn promotion that needs to be handled by GeneratePromote().
       It also assigns a score to the move for alpha-beta move
       ordering. If the move is a capture, it uses MVV/LVA
       (Most Valuable Victim/Least Valuable Attacker). Otherwise,
       it uses the move's history_ heuristic value. Note that
       1,000,000 is added to a capture move's score, so it
       always gets ordered above a "normal" move. */
    void GeneratePush(int from, int to, int bits);
    // GeneratePromote() is just like GeneratePush(), only it puts 4 moves
    //   on the move stack, one for each possible promotion piece_
    void GeneratePromote(int from, int to, int bits);
    // MakeMove() makes a move. If the move is illegal, it
    // undoes whatever it did and returns FALSE. Otherwise, it
    // returns TRUE.
    bool MakeMove(move_bytes m);
    void UnMakeMove();
    /////////////////////////////////////////////////////////////////
    /// SEARCH
    /////////////////////////////////////////////////////////////////
    // Think() calls Search() iteratively. Search statistics
    // are printed depending on the value of output:
    // 0 = no output
    // 1 = normal output
    // 2 = xboard format output
    void Think(int output);
    // Search() does just that, in negamax fashion
    int Search(int alpha, int beta, int depth);
    // Quiesce() is a recursive minimax Search function with
    // alpha-beta cutoffs. In other words, negamax. It basically
    // only searches capture sequences and allows the evaluation
    // function to cut the Search off (and set alpha). The idea
    // is to find a position where there isn't a lot going on
    // so the static evaluation function will work.
    int Quiesce(int alpha,int beta);
    // Repetitions() returns the number of times the current position
    // has been repeated. It compares the current value of hash_
    // to previous values.
    int Repetitions();
    // SortPrincipalVariation() is called when the Search function is following
    // the PV (Principal Variation). It looks through the current
    // ply_'s move list to see if the PV move is there. If so,
    // it adds 10,000,000 to the move's score so it's played first
    // by the Search function. If not, follow_pv_ remains FALSE and
    // Search() stops calling SortPrincipalVariation().
    void SortPrincipalVariation();
    // Sort() searches the current ply_'s move list from 'from'
    // to the end to find the move with the highest score. Then it
    // swaps that move and the 'from' move so the move with the
    // highest score gets searched next, and hopefully produces
    // a cutoff.
    void Sort(int from);
    // Checkup() is called once in a while during the Search.
    void Checkup();
    /////////////////////////////////////////////////////////////////
    /// EVALUATION
    /////////////////////////////////////////////////////////////////
    int Evaluate();
    int EvaluateLightPawn(int sq);
    int EvaluateDarkPawn(int sq);
    int EvaluateLightKing(int sq);
    // EvaluatelLkp(f) evaluates the Light King Pawn on file f
    int EvaluatelLkp(int f);
    int EvaluateDarkKing(int sq);
    int EvaluateDkp(int f);

    int GetMS();
    /////////////////////////////////////////////////////////////////
    /// BOOK
    /////////////////////////////////////////////////////////////////
    // OpenBook() opens the opening book file and initializes the random number
    // generator so we play random book moves.
    void OpenBook();
    // CloseBook() closes the book file. This is called when the program exits.
    void CloseBook();
    // BookMove() returns a book move (in integer format) or -1 if there is no
    // book move.
    int BookMove();
    // BookMatch() returns TRUE if the first part of s2 matches s1.
    bool BookMatch(char *s1, char *s2);
    /////////////////////////////////////////////////////////////////
    /// OUTPUT
    /////////////////////////////////////////////////////////////////
    char *MoveStr(move_bytes m);
    // parse the move s (in coordinate notation) and return the move's
    // index in gen_dat_, or -1 if the move is illegal
    int ParseMove(const char *s);
    // print_board() prints the board
    void PrintBoard();
    // print_result() checks to see if the game is over, and if so,
    // prints the result.
    void PrintResult();
    /////////////////////////////////////////////////////////////////
private:
    int color_[64];  // LIGHT, DARK, or EMPTY
    int piece_[64];  // PAWN, KNIGHT, BISHOP, ROOK, QUEEN, KING, or EMPTY
    int side_;       // the side_ to move
    int xside_;      // the side_ not to move
    // a bitfield with the castle_ permissions. if 1 is set,
    // white can still castle_ kingside. 2 is white queenside.
    // 4 is black kingside. 8 is black queenside.
    int castle_;
    // the en passant square. if white moves e2e4, the en passant
    // square is set to e3, because that's where a pawn would move
    // in an en passant capture
    int ep_;
    int fifty_;      // the number of moves since a capture or pawn move, used
                     // to handle the fifty_-move-draw rule
    int hash_;       // a (more or less) unique number that corresponds to the
                     //  position
    int ply_;        // the number of half-moves (ply_) since the
                     // root of the Search tree
    int hply_;       // h for history_; the number of ply_ since the beginning
                     // of the game

    // gen_dat_ is some memory for move lists that are created by the move
    // generators. The move list for ply_ n starts at first_move_[n] and ends
    // at first_move_[n + 1].
    gen_t gen_dat_[GEN_STACK];
    int first_move_[MAX_PLY];
    // the history_ heuristic array (used for move ordering)
    int history_[64][64];
    // we need an array of hist_t's so we can take back the
    // moves we make
    hist_t hist_dat_[HIST_STACK];

    // the engine will search for max_time_ milliseconds or until it finishes
    // searching max_depth_ ply_.
    int max_time_;
    int max_depth_;

    // the time when the engine starts searching, and when it should stop
    int start_time_;
    int stop_time_;

    int nodes_;  // the number of nodes_ we've searched
    // a "triangular" PV array; for a good explanation of why a triangular
    // array is needed, see "How Computers Play Chess" by Levy and Newborn.
    move pv_[MAX_PLY][MAX_PLY];
    int pv_length_[MAX_PLY];
    bool follow_pv_;


    // random numbers used to compute hash_; see SetHash() in board.c
    int hash_piece_[2][6][64];  // indexed by piece_ [color_][type][square]
    int hash_side_;
    int hash_ep_[64];

    // Now we have the mailbox_ array, so called because it looks like a
    // mailbox_, at least according to Bob Hyatt. This is useful when we
    // need to figure out what pieces can go where. Let's say we have a
    // rook on square a4 (32) and we want to know if it can move one
    // square to the left. We subtract 1, and we get 31 (h5). The rook
    // obviously can't move to h5, but we don't know that without doing
    // a lot of annoying work. Sooooo, what we do is figure out a4's
    // mailbox_ number, which is 61. Then we subtract 1 from 61 (60) and
    // see what mailbox_[60] is. In this case, it's -1, so it's out of
    // bounds and we can forget it. You can see how mailbox_[] is used
    // in Attack() in board.c.
    static int mailbox_[120];
    static int mailbox64_[64];
    // slide_, offsets_, and offset_ are basically the vectors that
    // pieces can move in. If slide_ for the piece_ is FALSE, it can
    // only move one square in any one direction. offsets_ is the
    // number of directions it can move in, and offset_ is an array
    // of the actual directions.
    static bool slide_[6];
    static int offsets_[6];
    static int offset_[6][8];
    // This is the castle_mask_ array. We can use it to determine
    // the castling permissions after a move. What we do is
    // logical-AND the castle_ bits with the castle_mask_ bits for
    // both of the move's squares. Let's say castle_ is 1, meaning
    // that white can still castle_ kingside. Now we play a move
    // where the rook on h1 gets captured. We AND castle_ with
    // castle_mask_[63], so we have 1&14, and castle_ becomes 0 and
    // white can't castle_ kingside anymore.
    static int castle_mask_[64];
    // the piece_ letters, for print_board()
    static char piece_char_[6];
    // the initial board state
    static int init_color_[64];
    static int init_piece_[64];
    /////////////////////////////////////////////////////////////////
    /// EVALUATION
    /////////////////////////////////////////////////////////////////
    // the values of the pieces
    static int piece_value_[6];
    // The "pcsq" arrays are piece_/square tables. They're values
    // added to the material value of the piece_ based on the
    // location of the piece_.
    static int pawn_pcsq_[64];
    static int knight_pcsq_[64];
    static int bishop_pcsq_[64];
    static int king_pcsq_[64];
    static int king_endgame_pcsq_[64];

    // The flip_ array is used to calculate the piece_/square
    // values for DARK pieces. The piece_/square value of a
    // LIGHT pawn is pawn_pcsq_[sq] and the value of a DARK
    // pawn is pawn_pcsq_[flip_[sq]]
    static int flip_[64];

    // pawn_rank_[x][y] is the rank of the least advanced pawn of color_ x on file
    // y - 1. There are "buffer files" on the left and right to avoid special-case
    // logic later. If there's no pawn on a rank, we pretend the pawn is
    // impossibly far advanced (0 for LIGHT and 7 for DARK). This makes it easy to
    // test for pawns on a rank and it simplifies some pawn evaluation code.
    int pawn_rank_[2][10];
    int piece_mat_[2];  // the value of a side_'s pieces
    int pawn_mat_[2];   // the value of a side_'s pawns
    bool ftime_ok_;     // does ftime return milliseconds?
    jmp_buf env_;
    bool stop_search_;
    /////////////////////////////////////////////////////////////////
    /// BOOK
    /////////////////////////////////////////////////////////////////
    FILE *book_file_;
    /////////////////////////////////////////////////////////////////
};

#endif // CHESSENGINE_H
