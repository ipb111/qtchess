#include "VertexBuffer.h"

VertexBuffer::VertexBuffer(void)
{
	glGenBuffersARB(1, &id_);
	target_ = 0;
}

VertexBuffer::~VertexBuffer(void)
{
	glDeleteBuffersARB(1, &id_);
}

GLenum VertexBuffer::getId() const
{
	return id_;
}

void VertexBuffer::bind(GLenum target)
{
	glBindBufferARB(target_ = target, id_);
}

void VertexBuffer::unbind()
{
	glBindBufferARB(target_, 0);
}

void VertexBuffer::setData(unsigned size, const void* ptr, GLenum usage)
{
	glBufferDataARB(target_, size, ptr, usage);
}

void VertexBuffer::setSubData(unsigned offs, unsigned size, const void* ptr)
{
	glBufferSubDataARB(target_, offs, size, ptr);
}

void VertexBuffer::getSubData(unsigned offs, unsigned size, void* ptr)
{
	glGetBufferSubDataARB(target_, offs, size, ptr);
}

GLvoid* VertexBuffer::map(GLenum access)
{
	return glMapBufferARB(target_, access);
}

bool VertexBuffer::unmap()
{
	return glUnmapBufferARB(target_) == GL_TRUE;
}

void VertexBuffer::clear()
{
	glBufferDataARB(target_, 0, NULL, 0);
}


