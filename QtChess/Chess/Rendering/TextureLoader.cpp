#include "TextureLoader.h"
#include <Windows.h>		// ToDo: shouldn't use it in future

TextureLoader::TextureLoader(void)
{
    textureID_ = 0;
    textureWidth_ = 0;
    textureHeight_ = 0;
}


TextureLoader::~TextureLoader(void)
{
	freeTexture();
}

bool TextureLoader::loadTextureFromFile( std::string  path )
{
	// ���������� ��������.
    bool textureLoaded = false;

    // ��������� � ��������� ��������������.
    ILuint imgID = 0;
    ilGenImages( 1, &imgID );
    ilBindImage( imgID );

	// ����������� � wchar*
	const char* c    = path.c_str();
	const wchar_t *wc;
	int nChars = MultiByteToWideChar(CP_ACP, 0, c, -1, NULL, 0);
	wc = new wchar_t[nChars];
	MultiByteToWideChar(CP_ACP, 0, c, -1, (LPWSTR)wc, nChars);

	// �������� �����������.
	ILboolean success = ilLoadImage( wc );
	delete wc;

	// ���� �� ��...
    if( success == IL_TRUE )
    {
        // ����������� � RGBA (��������, ��� ���������� ������ � RGBA �������).
        success = ilConvertImage( IL_RGBA, IL_UNSIGNED_BYTE );

		if( success == IL_TRUE )
            textureLoaded = loadTextureFromPixels32( (GLuint*)ilGetData(), (GLuint)ilGetInteger( IL_IMAGE_WIDTH ), (GLuint)ilGetInteger( IL_IMAGE_HEIGHT ) );
		
		// �������� ����� �� ������ DevIL.
        ilDeleteImages( 1, &imgID );
    }

    if( !textureLoaded )
        printf( "Unable to load %s\n", path.c_str() );

    return textureLoaded;
}

bool TextureLoader::loadTextureFromPixels32( GLuint* pixels, GLuint width, GLuint height )
{
	// �������� ��������, ���� ��� ������� ��� ������� id.
    freeTexture();

    textureWidth_ = width;
    textureHeight_ = height;

    // ��������� �������������� ��������.
    glGenTextures( 1, &textureID_ );

    // �������� ID  (��������� ������ ���� ��������).
    glBindTexture( GL_TEXTURE_2D, textureID_ );

    // ���������.
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels );

    // ��������� ����������.
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );

    glBindTexture( GL_TEXTURE_2D, NULL );

    // �������� ������.
    GLenum error = glGetError();
    if( error != GL_NO_ERROR )
    {
        printf( "Error loading texture from %p pixels! %s\n", pixels, gluErrorString( error ) );
        return false;
    }

    return true;
}

void TextureLoader::freeTexture()
{
	// �������� ��������.
    if( textureID_ != 0 )
    {
        glDeleteTextures( 1, &textureID_ );
        textureID_ = 0;
    }

    textureWidth_ = 0;
    textureHeight_ = 0;
}

GLuint TextureLoader::getTextureID()
{
	return textureID_;
}
GLuint TextureLoader::textureWidth()
{
	return textureWidth_;
}
GLuint TextureLoader::textureHeight()
{
	return textureHeight_;
}
