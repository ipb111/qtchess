/*
Copyright (c) <2013>, <Ilya Shoshin>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include "IRenderingEngine.h"

#include <QGLWidget>

#define VP_OFFSET 20
#define TEXTURE_COUNT 14

class RenderingEngine : public IRenderingEngine
{
	Q_OBJECT
    Q_INTERFACES(IRenderingEngine)
public:
	explicit RenderingEngine(QWidget *parent = 0);
	~RenderingEngine() 
	{
		for(int i = 0; i < TEXTURE_COUNT; ++i)
		{
			deleteTexture(textures_[i]); 
		}
		delete[] textures_;
		delete[] possibleMoves_;
	}
signals:
	void startAnimation();
	void stopAnimation();
	void recievePiecePosition(int n1);
	void recieveNewPosition(int n2, bool sendViaLan);
public slots:
	void validationSucceed(const int *, int);
    void performMove(int n1, int n2);
    void SyncBoard(int *, int *);
    void SyncBoardFromPvP(const int *, const  Piece *);
	//---------------------------------------------------------------
	// �������������
	//---------------------------------------------------------------
public:
	void initialize() override;
	/// ������� ������������� ���������
	/// >> �������� ����������
	void setupRelativeCoordCoeff(unsigned int realWH) override;
private:
	/// �������� �������
	void initializeTextures();
	/// ��������� ���������
	void generateChess();
	//---------------------------------------------------------------
	// ���������
	//---------------------------------------------------------------
public:
	void render() override;
    void selectPieceWithColor(bool);
public slots:
    void selectComputerPiece();
    void cancelComputerSelect();
private:
	/// ��������� ���������������� ������ (��������� ������)
	/// >>�������� � �������>>��� ������
	void drawTexturedArray(int i, int piece);
	/// ��������� ���������������� ������ (��������� ������)
	/// >>�������� � �������>>��� ������
	void drawMultiTexturedArray(int i, int piece, bool bcell);
	/// ��������� ������
	/// >>�������� � �������>>���� ������
	void drawArray(int i, bool bcell);
	/// ��������� ��������� ������
	void drawSelectedPiece();
	/// ��������� ��������� ��������� ������
	void selectPiece();
	/// �������� ������ / ����� ���������
	/// >> ��������?
	void performSelection(bool select);
	//---------------------------------------------------------------
	// ��������
	//---------------------------------------------------------------
public:
	/// ���������� ��������
    void update() override;
private:
	void setupAnimation();
	void endAnimation();
	/// �������� ��������
	void animateMovement();
	/// ���������� ���������� ����
	void nextMove();
	//---------------------------------------------------------------
	// �����������
	//---------------------------------------------------------------
public:
	void reshape(int width, int height) override;
	void mousePressed(int x, int y) override;
private:
	//---------------------------------------------------------------
	// Helpers
	//---------------------------------------------------------------
	/// ��������� �������������� �������� �� ���� ������
	/// >> ������
	/// << id ��������
	uint getPieceTexture(char piece);
	/// �������� �� ������ ��������� �����
	bool isPossibleMove(uint n);
private:

	int width_;
	int height_;
	//---------------------------------------------------------------
	struct Vertex				// ��������� �������.
	{
		float x;
		float y;
		float r,g,b,a;			// ���� �������
	};

	Vertex vert_[264];			// (64 �������� * 4 ������� + ������� ��� �������� + 4 ����� ��� ��������� ������)
	static char board[];		// �����

	int* textures_;				// ������ ���������� �� id �������.

	data::move mv_;				// ��������� ���.
	//---------------------------------------------------------------
	struct AnimationData		// ��������� ��������.
	{
		AnimationData() : mvTypeEnum(HORIZONTAL_MOVE), cx(0), cy(0), xn(0), yn(0), dx(0), dy(0), 
						  speedX(0.0f), speedY(0.0f), piece(0) { }
		enum MoveType
		{
			HORIZONTAL_MOVE,
			VERTICAAL_MOVE,
			KNIGHT_MOVE,
			DIAGONAL_MOVE
		};
		MoveType mvTypeEnum;
		float cx;
		float cy;
		int xn;
		int yn;
		int dx;
		int dy;
		float speedX;
		float speedY;
		char piece;
	};

	AnimationData animation_;

	float relativeCoordCoeff_;	// ������������ ��� �������� � ������������� ����������.
	//---------------------------------------------------------------
	typedef enum : uint {
		SELECT,
		MOVE,
		NONE
	} State;

	State state_;				// ������� ���������.

	//---------------------------------------------------------------
	// PvP
	//---------------------------------------------------------------
	int* possibleMoves_;		// ������ ��������� �����
	int  arraySize_;
    //---------------------------------------------------------------
    // PvE
    //---------------------------------------------------------------
    int currentMove_;
};

