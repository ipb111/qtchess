#pragma once

#define NULL 0

#include <qopengl.h>

class VertexBuffer
{

public:
	VertexBuffer(void);
	~VertexBuffer(void);

	GLenum getId() const;

	void bind(GLenum target);
	void unbind();
	void setData(unsigned size, const void* ptr, GLenum usage);
	void setSubData(unsigned offs, unsigned size, const void* ptr);
	void getSubData(unsigned offs, unsigned size, void* ptr);
	GLvoid* map(GLenum access);
	bool unmap();
	void clear();

private:
	GLuint id_;
	GLenum target_;
};

