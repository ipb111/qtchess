#pragma once



class TextureLoader
{
public:
	TextureLoader(void);
	~TextureLoader(void);

	bool loadTextureFromFile( std::string path );
	void freeTexture();

	GLuint getTextureID();
    GLuint textureWidth();
    GLuint textureHeight();
   
private:

    GLuint textureID_;
	        
    GLuint textureWidth_;
    GLuint textureHeight_;
};


static std::string GetExePath() 
{
	return QDir::currentPath().toStdString();
}

