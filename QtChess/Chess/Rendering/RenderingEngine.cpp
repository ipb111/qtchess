/*
Copyright (c) <2013>, <Ilya Shoshin>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "RenderingEngine.h"
#include "../../Chess/IO/HistoryIO.h"

#include <QDebug>
#include <math.h>

//=================================================================================================
char RenderingEngine::board[64] = {
		-ROOK, -KNIGHT, -BISHOP, -QUEEN, -KING, -BISHOP, -KNIGHT, -ROOK,
		-PAWN, -PAWN  , -PAWN  , -PAWN, -PAWN , -PAWN  , -PAWN  , -PAWN,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		PAWN, PAWN  , PAWN  , PAWN , PAWN, PAWN  , PAWN  , PAWN,
		ROOK, KNIGHT, BISHOP, QUEEN, KING, BISHOP, KNIGHT, ROOK
	};  
//=================================================================================================
//==============================�������������======================================================
//=================================================================================================
/// ��������� ������ ��� ������
/// << ��������� �� ���������� ������� ������
IRenderingEngine* CreateRenderer()
{
    return new RenderingEngine();
}

RenderingEngine::RenderingEngine(QWidget *parent) : IRenderingEngine(parent)
{
	state_ = NONE;
	mv_.n1 = mv_.n2 = 0;
	possibleMoves_ = 0;
	generateChess();
}

void RenderingEngine::initialize()
{
	glClearColor ( 0.4, 0.7, 0.7, 1.0 );

	glHint ( GL_POLYGON_SMOOTH_HINT,         GL_NICEST );
	glHint ( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
	glShadeModel(GL_SMOOTH);
	
	initializeTextures();
}	
/// ������� ������������� ���������
/// >> �������� ����������
void RenderingEngine::setupRelativeCoordCoeff(unsigned int realWH)
{
	relativeCoordCoeff_ = 8.0 / realWH;
}

/// �������� �������
void RenderingEngine::initializeTextures()
{
	std::string pieceNames_[8];
	std::string piecesDirs_[3];

	pieceNames_[0] = "King.png";
	pieceNames_[1] = "Queen.png";
	pieceNames_[2] = "Bishop.png";  
	pieceNames_[3] = "Knight.png";
	pieceNames_[4] = "Rook.png";
	pieceNames_[5] = "Pawn.png";
	pieceNames_[6] = "bCell.png";
	pieceNames_[7] = "wCell.png";

	piecesDirs_[0] = ":/White/";
	piecesDirs_[1] = ":/Black/";
	piecesDirs_[2] = ":/cell/";

	textures_ = new int[14];

	for (int j=0; j < 6; ++j) {
		QString resW((piecesDirs_[0] + pieceNames_[j]).c_str());
		QPixmap pixW(resW);
		textures_[j] = bindTexture(pixW, GL_TEXTURE_2D, GL_RGBA, QGLContext::LinearFilteringBindOption);
		QString resB((piecesDirs_[1] + pieceNames_[j]).c_str());
		QPixmap pixB(resB);
		textures_[j + 6] = bindTexture(pixB, GL_TEXTURE_2D, GL_RGBA, QGLContext::LinearFilteringBindOption);
	}
	QString resbC((piecesDirs_[2] + pieceNames_[6]).c_str());
	textures_[12] = bindTexture(resbC, GL_TEXTURE_2D, GL_RGBA, QGLContext::LinearFilteringBindOption);
	QString reswC((piecesDirs_[2] + pieceNames_[7]).c_str());
	textures_[13] = bindTexture(reswC, GL_TEXTURE_2D, GL_RGBA, QGLContext::LinearFilteringBindOption);
}
/// ��������� ���������
void RenderingEngine::generateChess()
{
	float r1 = 1.0, g1 = 1.0, b1 = 1.0;
	float r2 = 0.5, g2 = 0.5, b2 = 0.5;
	float rs1 = 0.0, gs1 = 0.0, bs1 = 0.0, as1 = 0.0f;
    float rs2 = 1.0, gs2 = 0.0, bs2 = 1.0, as2 = 1.0f;
	bool f = false;
	float r,g,b;
	int x,y;
	for(int i = 0, n = 0; i < 64; ++i)
	{
		x = i & 7;
		y = i >> 3;
		vert_[n].x = x;			vert_[n].y = y;
		vert_[n + 1].x = x;		vert_[n + 1].y = y + 1;
		vert_[n + 2].x = x + 1;	vert_[n + 2].y = y + 1;
		vert_[n + 3].x = x + 1;	vert_[n + 3].y = y;
		f = (x + y) % 2;
		r = f ? r2 : r1;
		g = f ? g2 : g1;
		b = f ? b2 : b1;
        vert_[n].r= r;  vert_[n].g = g;
        vert_[n].b = r; vert_[n].a = 1.0;
		vert_[n+1].r = r; vert_[n+1].g = g;
		vert_[n+1].b = b; vert_[n+1].a = 1.0;
        vert_[n+2].r = r; vert_[n+2].g = g;
        vert_[n+2].b = b; vert_[n+2].a = 1.0;
		vert_[n+3].r = r; vert_[n+3].g = g;
		vert_[n+3].b = b; vert_[n+3].a = 1.0;
		n+=4;
	}

	// ������� ��� ��������
	vert_[256].x = 0;	vert_[256].y = 0;
	vert_[257].x = 0;	vert_[257].y = 1;
	vert_[258].x = 1;	vert_[258].y = 1;
	vert_[259].x = 1;	vert_[259].y = 0;
	vert_[256].r = rs1;	vert_[256].g = gs1;
	vert_[256].b = bs1;	vert_[256].a = as1;
	vert_[257].r = rs1;	vert_[257].g = gs1;
	vert_[257].b = bs1;	vert_[257].a = as1;
	vert_[258].r = rs1;	vert_[258].g = gs1;
	vert_[258].b = bs1;	vert_[258].a = as1;
	vert_[259].r = rs1;	vert_[259].g = gs1;
	vert_[259].b = bs1;	vert_[259].a = as1;
	// 4 ����� ��� ��������� ������
	const float offset = 0.1;
	vert_[256].x = offset;		vert_[256].y = offset;
	vert_[257].x = offset;		vert_[257].y = 1 - offset;
	vert_[258].x = 1 - offset;	vert_[258].y = 1 - offset;
	vert_[259].x = 1 - offset;	vert_[259].y = offset;
	vert_[256].r = rs2;	vert_[256].g = gs2;
	vert_[256].b = bs2;	vert_[256].a = as2;
	vert_[257].r = rs2;	vert_[257].g = gs2;
	vert_[257].b = bs2;	vert_[257].a = as2;
	vert_[258].r = rs2;	vert_[258].g = gs2;
	vert_[258].b = bs2;	vert_[258].a = as2;
	vert_[259].r = rs2;	vert_[259].g = gs2;
	vert_[259].b = bs2;	vert_[259].a = as2;
}
//=================================================================================================
//==============================�������� � ���������===============================================
//=================================================================================================
/// ���������
void RenderingEngine::render()
{
	glClear ( GL_COLOR_BUFFER_BIT );
	//------------------------------

    glViewport     ( 0, 0, (GLsizei)height_, (GLsizei)height_ );
    glMatrixMode   ( GL_PROJECTION );
    glLoadIdentity ();
    gluOrtho2D(0, 8, 8, 0);
    glMatrixMode   ( GL_MODELVIEW );
    glLoadIdentity ();

	char p = 0;
	int x = 0;
	int y = 0;
	bool f = false;
    // ��������� �����
    for(int n = 0, i = 0; n < 64; ++n, i+=4)
    {
		x = i & 7;
		y = i >> 3;
		f = (x + y) % 2;
        p = board[n];
        if(p != 0)
        {
            if(mv_.n1 == n && state_ == MOVE)
            {
                drawArray(i, f);
                continue;
            }
			//drawArray(i, f);
            drawTexturedArray(i, p);
        }
        else
            drawArray(i, f);
    }

    if(state_ == MOVE)
            animateMovement();

}

void RenderingEngine::selectPieceWithColor(bool select)
{
    if(mv_.n1 >= 0 && mv_.n1 < 64)
    {
        int n = mv_.n1;
        const int x = n & 7;
        const int y = n >> 3;
        const bool f = (x + y) % 2;
        const float rs1 = 1.0, gs1 = 0.0, bs1 = 0.0, as1 = 0.5f;
        const float r1 = 1.0, g1 = 1.0, b1 = 1.0;
        const float r2 = 0.5, g2 = 0.5, b2 = 0.5;
        const float r = select ? rs1 : ( f ? r2 : r1);
        const float g = select ? gs1 : ( f ? g2 : g1);
        const float b = select ? bs1 : ( f ? b2 : b1);
        const float a = select ? as1 : 1;
        n *= 4;
        vert_[n].r = r;	vert_[n].g = g;
        vert_[n].b = b;	vert_[n].a = a;
        vert_[n + 1].r = r;	vert_[n + 1].g = g;
        vert_[n + 1].b = b;	vert_[n + 1].a = a;
        vert_[n + 2].r = r;	vert_[n + 2].g = g;
        vert_[n + 2].b = b;	vert_[n + 2].a = a;
        vert_[n + 3].r = r;	vert_[n + 3].g = g;
        vert_[n + 3].b = b;	vert_[n + 3].a = a;
    }
}

/// ��������� ���������������� ������ (��������� ������)
/// >>�������� � �������>>��� ������
void RenderingEngine::drawTexturedArray(int i, int piece)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, getPieceTexture(piece));
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, state_ == MOVE && i == 256 ? GL_REPLACE : GL_DECAL);
	glBegin(GL_QUADS);
	glTexCoord2d(0, 0); glColor4f(vert_[i].r, vert_[i].g, vert_[i].b, vert_[i].a);
    glVertex2f(vert_[i].x,		vert_[i].y);
	glTexCoord2d(0, 1); glColor4f(vert_[i + 1].r, vert_[i + 1].g, vert_[i + 1].b, vert_[i + 1].a);
    glVertex2f(vert_[i + 1].x,	vert_[i + 1].y);
	glTexCoord2d(1, 1); glColor4f(vert_[i + 2].r, vert_[i + 2].g, vert_[i + 2].b, vert_[i + 2].a);
    glVertex2f(vert_[i + 2].x,	vert_[i + 2].y);
	glTexCoord2d(1, 0); glColor4f(vert_[i + 3].r, vert_[i + 3].g, vert_[i + 3].b, vert_[i + 3].a);
    glVertex2f(vert_[i + 3].x,	vert_[i + 3].y);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

/// ��������� ���������������� ������ (��������� ������)
/// >>�������� � �������>>��� ������
void RenderingEngine::drawMultiTexturedArray(int i, int piece, bool bcell)
{
    bcell = false;
/*
	glActiveTextureARB ( GL_TEXTURE0_ARB );
	glEnable           ( GL_TEXTURE_2D );
	glBindTexture      ( GL_TEXTURE_2D, texture1 );
	glTexEnvi          ( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );

	glActiveTextureARB ( GL_TEXTURE1_ARB );
	glEnable           ( GL_TEXTURE_2D );
	glBindTexture      ( GL_TEXTURE_2D, texture2 );
	glTexEnvi          ( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
*/
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, getPieceTexture(piece));
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, state_ == MOVE && i == 256 ? GL_REPLACE : GL_DECAL);
	glBegin(GL_QUADS);
	glTexCoord2d(0, 0); //glColor4f(vert_[i].r, vert_[i].g, vert_[i].b, vert_[i].a);
	glVertex2f(vert_[i].x,		vert_[i].y);
	glTexCoord2d(0, 1); //glColor4f(vert_[i + 1].r, vert_[i + 1].g, vert_[i + 1].b, vert_[i + 1].a);
	glVertex2f(vert_[i + 1].x,	vert_[i + 1].y);
	glTexCoord2d(1, 1); //glColor4f(vert_[i + 2].r, vert_[i + 2].g, vert_[i + 2].b, vert_[i + 2].a);
	glVertex2f(vert_[i + 2].x,	vert_[i + 2].y);
	glTexCoord2d(1, 0); //glColor4f(vert_[i + 3].r, vert_[i + 3].g, vert_[i + 3].b, vert_[i + 3].a);
	glVertex2f(vert_[i + 3].x,	vert_[i + 3].y);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

/// ��������� ������
/// >>�������� � �������
void RenderingEngine::drawArray(int i, bool bcell)
{
    bcell = false;
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glEnable(GL_TEXTURE_2D);
	//glBindTexture(GL_TEXTURE_2D, textures_[bcell ? 12 : 13]);
	//glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	///
	glBegin(GL_QUADS);
	//glTexCoord2d(0, 0); 
	glColor4f(vert_[i].r, vert_[i].g, vert_[i].b, vert_[i].a);
	glVertex2f(vert_[i].x,		vert_[i].y);
	//glTexCoord2d(0, 1); 
	glColor4f(vert_[i + 1].r, vert_[i + 1].g, vert_[i + 1].b, vert_[i + 1].a);
	glVertex2f(vert_[i + 1].x,	vert_[i + 1].y);
	//glTexCoord2d(1, 1); 
	glColor4f(vert_[i + 2].r, vert_[i + 2].g, vert_[i + 2].b, vert_[i + 2].a);
	glVertex2f(vert_[i + 2].x,	vert_[i + 2].y);
	//glTexCoord2d(1, 0); 
	glColor4f(vert_[i + 3].r, vert_[i + 3].g, vert_[i + 3].b, vert_[i + 3].a);
	glVertex2f(vert_[i + 3].x,	vert_[i + 3].y);
	glEnd();
	///
	//glBindTexture(GL_TEXTURE_2D, 0);
	//glDisable(GL_TEXTURE_2D);
	//glDisable(GL_BLEND);
}
/// ��������� ��������� ������
void RenderingEngine::drawSelectedPiece()
{
	const int i = 260;
	glBegin(GL_LINES);
	glColor4f(vert_[i].r, vert_[i].g, vert_[i].b, vert_[i].a);
	glVertex2f(vert_[i].x,		vert_[i].y);
	glColor4f(vert_[i + 1].r, vert_[i + 1].g, vert_[i + 1].b, vert_[i + 1].a);
	glVertex2f(vert_[i + 1].x,	vert_[i + 1].y);
	glColor4f(vert_[i + 2].r, vert_[i + 2].g, vert_[i + 2].b, vert_[i + 2].a);
	glVertex2f(vert_[i + 2].x,	vert_[i + 2].y);
	glColor4f(vert_[i + 3].r, vert_[i + 3].g, vert_[i + 3].b, vert_[i + 3].a);
	glVertex2f(vert_[i + 3].x,	vert_[i + 3].y);
	glEnd();
}

/// ��������� ��������� ��������� ������
void RenderingEngine::selectPiece()
{
	int n = mv_.n1;
	if(n <= 64)
	{
		float x = n & 7;
		float y = n >> 3;
		const float offset = 0.1;
		vert_[260].x = offset + x;		vert_[260].y = offset + y;
		vert_[261].x = offset + x;		vert_[261].y = 1 - offset + y;
		vert_[262].x = 1 - offset + x;	vert_[262].y = 1 - offset + y;
        vert_[263].x = 1 - offset + x;	vert_[263].y = offset + y;
	}
}
/// �������� ������ / ����� ���������
/// >> ��������?
void RenderingEngine::performSelection(bool select)
{
	float r,g,b,a;
	float r1 = 1.0, g1 = 1.0, b1 = 1.0;
	float r2 = 0.5, g2 = 0.5, b2 = 0.5;
	bool f = false;
	int x,y;
	r = select ? 0.4 : 1.0;
	g = select ? 0.7 : 1.0;
	b = select ? 0.7 : 1.0;
	a = select ? 0.7 : 1.0;

	int i = 0;
	for(int k = 0; k < arraySize_; ++k)
	{
		int n = possibleMoves_[k];
		i = n * 4;
		if(!select)
		{
			x = n & 7;
			y = n >> 3;
			f = (x + y) % 2;
			r = f ? r2 : r1;
			g = f ? g2 : g1;
			b = f ? b2 : b1;
		}

		vert_[i].r = r; vert_[i].g = g; vert_[i].b = b; vert_[i].a = a;	
		vert_[i + 1].r = r; vert_[i + 1].g = g; vert_[i + 1].b = b; vert_[i + 1].a = a;	
		vert_[i + 2].r = r; vert_[i + 2].g = g; vert_[i + 2].b = b; vert_[i + 2].a = a;	
		vert_[i + 3].r = r; vert_[i + 3].g = g; vert_[i + 3].b = b; vert_[i + 3].a = a;			
	}
}
//---------------------------------------------------------------
// ��������
//---------------------------------------------------------------
/// ���������� ��������
void RenderingEngine::update()
{
	if(state_ != MOVE) return;

	nextMove();
}
/// ��������� ��������
void RenderingEngine::setupAnimation()
{
	animation_.piece = board[mv_.n1];
	animation_.speedX = animation_.speedY = 0.1f;
	animation_.cx = mv_.n1 & 7;
	animation_.cy = mv_.n1 >> 3;
	const uint offset = 256;
	// ������� ���������� ���������
	vert_[offset].x = animation_.cx;			vert_[offset].y = animation_.cy;
	vert_[offset + 1].x = animation_.cx;		vert_[offset + 1].y = animation_.cy + 1;
	vert_[offset + 2].x = animation_.cx + 1;	vert_[offset + 2].y = animation_.cy + 1;
	vert_[offset + 3].x = animation_.cx + 1;	vert_[offset + 3].y = animation_.cy;
	animation_.xn = mv_.n2 & 7;
	animation_.yn = mv_.n2 >> 3;
	if(animation_.xn < animation_.cx)
		animation_.speedX = -animation_.speedX;
	if(animation_.yn < animation_.cy)
		animation_.speedY = -animation_.speedY;
	animation_.dx = animation_.xn - animation_.cx;
	animation_.dy = animation_.yn - animation_.cy;
	unsigned dx = abs(animation_.dx);
	unsigned dy = abs(animation_.dy);
	// ����������� ���� �����������
	if(dx == 0 && dy != 0)
		animation_.mvTypeEnum = animation_.VERTICAAL_MOVE;
	else if(dx != 0 && dy == 0)
		animation_.mvTypeEnum = animation_.HORIZONTAL_MOVE;
	else if(dx != dy != 0)
		animation_.mvTypeEnum = animation_.KNIGHT_MOVE;
	else
		animation_.mvTypeEnum = animation_.DIAGONAL_MOVE;

	emit startAnimation();
}
/// ����� ��������
inline void RenderingEngine::endAnimation()
{
	board[mv_.n2] = board[mv_.n1];
	board[mv_.n1] = 0;
	state_ = NONE;

	emit stopAnimation();
}
/// �������� ��������
void RenderingEngine::animateMovement()
{
	if(animation_.piece != 0)
	{
		drawTexturedArray(256, animation_.piece);
	}
}
/// ���������� ���������� ����
void RenderingEngine::nextMove()
{
	const uint offset = 256;
	
	switch(animation_.mvTypeEnum)
	{
		case AnimationData::HORIZONTAL_MOVE:
			animation_.cx += animation_.speedX;
			vert_[offset].x = animation_.cx;
			vert_[offset + 1].x = animation_.cx;
			vert_[offset + 2].x = animation_.cx + 1;
			vert_[offset + 3].x = animation_.cx + 1;

            if( (animation_.dx < 0 && animation_.cx <= animation_.xn) ||
                (animation_.dx > 0 && animation_.cx >= animation_.xn) )
				endAnimation();

			break;
		case AnimationData::VERTICAAL_MOVE:
			animation_.cy += animation_.speedY;
			vert_[offset].y = animation_.cy;
			vert_[offset + 1].y = animation_.cy + 1;
			vert_[offset + 2].y = animation_.cy + 1;
			vert_[offset + 3].y = animation_.cy;

            if( (animation_.dy > 0 && animation_.cy >= animation_.yn) ||
                (animation_.dy < 0 && animation_.cy <= animation_.yn) )
				endAnimation();

			break;
		case AnimationData::KNIGHT_MOVE:

            if( (animation_.dx > 0 && animation_.cx >= animation_.xn - animation_.speedX) ||
                (animation_.dx < 0 && animation_.cx <= animation_.xn - animation_.speedX) )
			{
				animation_.cy += animation_.speedY;
				vert_[offset].y = animation_.cy;
				vert_[offset + 1].y = animation_.cy + 1;
				vert_[offset + 2].y = animation_.cy + 1;
				vert_[offset + 3].y = animation_.cy;
			}
			else
			{
				animation_.cx += animation_.speedX;
				vert_[offset].x = animation_.cx;
				vert_[offset + 1].x = animation_.cx;
				vert_[offset + 2].x = animation_.cx + 1;
				vert_[offset + 3].x = animation_.cx + 1;
			}

            if( (animation_.dy > 0 && animation_.cy >= animation_.yn) ||
                (animation_.dy < 0 && animation_.cy <= animation_.yn) )
				endAnimation();

			break;
		case AnimationData::DIAGONAL_MOVE:
			animation_.cx += animation_.speedX;
			animation_.cy += animation_.speedY;

			vert_[offset].x = animation_.cx;			vert_[offset].y = animation_.cy;
			vert_[offset + 1].x = animation_.cx;		vert_[offset + 1].y = animation_.cy + 1;
			vert_[offset + 2].x = animation_.cx + 1;	vert_[offset + 2].y = animation_.cy + 1;
			vert_[offset + 3].x = animation_.cx + 1;	vert_[offset + 3].y = animation_.cy;

            if( (animation_.dx < 0 && animation_.cx <= animation_.xn) ||
                (animation_.dx > 0 && animation_.cx >= animation_.xn) )
				endAnimation();

			break;
	}
}

//=================================================================================================
//==============================Helpers============================================================
//=================================================================================================
/// ��������� �������������� �������� �� ���� ������
/// >> ������
/// << id ��������
inline uint RenderingEngine::getPieceTexture(char piece)
{
    //if(piece >= 0 && piece < TEXTURE_COUNT && textures_)
        return textures_[ piece >= 0 ? piece - 1 : 5 - piece];
    //else
    //    return 0;
}
/// �������� �� ������ ��������� �����
inline bool RenderingEngine::isPossibleMove(uint n)
{
	for(int i = 0; i < arraySize_; ++i)
        if(possibleMoves_[i] == (int)n)
			return true;
	return false;
}
//=================================================================================================
//==============================�����������========================================================
//=================================================================================================
/// ��������� ��������
/// >>������>>������
void RenderingEngine::reshape( int width, int height )
{
	width_ = width;
	height_ = height;
	relativeCoordCoeff_ = 8.0 / height;
}
/// ���������� ������� ����
/// >>x>>y
void RenderingEngine::mousePressed(int x, int y)
{
	x *= relativeCoordCoeff_;
	y *= relativeCoordCoeff_;
    selectPieceWithColor(false);
	if(state_ == NONE)
	{
		mv_.n1 = y * 8 + x;
		arraySize_ = 0;
		emit recievePiecePosition(mv_.n1);
        qDebug() << "selected piece at: "<<mv_.n1;
	}
	else if(state_ == SELECT)
	{
		mv_.n2 = y * 8 + x;
		if(mv_.n1 == mv_.n2 && mv_.n1 != 0) return;
        if(possibleMoves_ && !isPossibleMove(mv_.n2))
		{
			performSelection(false);
			state_ = NONE;
			double newX = (double)x / relativeCoordCoeff_;
			double newY = (double)y / relativeCoordCoeff_;
            mousePressed(ceil(newX), ceil(newY));
			return;
		}
		emit recieveNewPosition(mv_.n2, true);
        qDebug() << "selected new position: "<<mv_.n2;
	}
}

void RenderingEngine::validationSucceed(const int *possibleMoves, int size)
{
	if(state_ == NONE)
	{
		if(possibleMoves_)
		{
			delete[] possibleMoves_;
			possibleMoves_ = 0;
		}
		// it's safer to create a copy (array size shouldn't go beyond 40)
		arraySize_ = size;
		possibleMoves_ = new int[arraySize_];
		for(int i = 0; i < arraySize_; ++i)
		{
			possibleMoves_[i] = possibleMoves[i];
		}
		performSelection(true);
		selectPiece();
        selectPieceWithColor(true);
		state_ = SELECT;
	}
	else
		if(state_ == SELECT)
		{
			performSelection(false);
            selectPieceWithColor(false);
			setupAnimation();
			state_ = MOVE;
		}
}

void RenderingEngine::performMove(int n1, int n2)
{
    qDebug() << "show move";
    mv_.n1 = n1;
    mv_.n2 = n2;
    performSelection(false);
    selectPieceWithColor(false);
    setupAnimation();
    state_ = MOVE;
}

void RenderingEngine::selectComputerPiece()
{
    qDebug() << "display selection\n";
    state_ = SELECT;
    selectPieceWithColor(true);
}

void RenderingEngine::cancelComputerSelect()
{
    selectPieceWithColor(false);
    state_ = NONE;
}

void RenderingEngine::SyncBoard(int *piece, int *color)
{
    const int match[7] = {PieceType::PAWN, PieceType::KNIGHT, PieceType::BISHOP, PieceType::ROOK, PieceType::QUEEN, PieceType::KING, PieceType::NONE};
    auto checkSync = [&](int n, int p, int c) -> void {
        c = !c ? 1 : -1; // 0 - light
        int right = c > 0 ? match[p] : -match[p];
        if(board[n] != right)
        {
            board[n] = right;
            qDebug() << "synchronization!\n";
        }
    };
    for(int n = 0; n < 64; ++n)
        checkSync(n, piece[n], color[n]);
}

void RenderingEngine::SyncBoardFromPvP(const int *map, const Piece *pieces)
{
    if(!map || !pieces)
        return;
    for(int n = 0; n < 64; ++n)
    {
        if(map[n] != -1)
        {
            Piece p = pieces[map[n]];
            board[n] = p.getColor() == BLACK ? -p.getPiece() : p.getPiece();
        }
        else
        {
            board[n] = 0;
        }
    }
    render();
    glFlush();
    qDebug() << "baord synced with pvp\n";
}
