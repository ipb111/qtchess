#include "IRenderingEngine.h"

/*
	INFINISHED 
	Optimize possible moves to store only possible ones
*/

// !!! ��������� � Partiya
#define MOVE_COUNT 29	// ���������� ��������� �����

/// ��������� ��� ������ OpenGL, �������������� vertex buffer object
class RenderingEngineVBO : public IRenderingEngine
{
public:
	RenderingEngineVBO();
	~RenderingEngineVBO() 
	{
		// ������� ���������
		vboVert_->clear();
		delete vboVert_;
		// clear texture array
		for(int i = 0; i < 12; ++i)
			delete textures_[i];
		delete[] textures_;
		delete[] possibleMoves_;
		delete partiya_;
	}

	void initialize();
	void render();
	void update();
	void reshape(int width, int height){ }
	void performSelection(bool select);
	void mousePressed(int x, int y);
	void setupRelativeCoordCoeff(unsigned int realWH);
private:
	void drawTexturedArray(int i, const uint stride, int piece);
	void animateMovement();
	void initializeTextures();
	void drawText(char* s, int x, int y, float scaleX, float scaleY);
	void generateChess();
	uint getPieceTexture(char piece);
	uint getVertexOffset(char piece);
	uint getNextPosition();
	bool isPossibleMove(uint n);
private:

	struct Vertex				// ��������� �������.
	{
		float x;
		float y;
		float r,g,b,a;	// ���� �������
	};

	VertexBuffer *vboVert_;		// Vbo ��� ����� � ��������.
	static char board[];		// �����

	TextureLoader** textures_;	// ������ ���������� �� ��������� �������.

	move mv_;					// ��������� ���.
	uint currentMove_;			// ������������� ��� ��� ��������.
	float speed_;				// �������� ����������� ������
	float animTime_;			

	float relativeCoordCoeff_;	// ������������ ��� �������� � ������������� ����������.

	typedef enum : uint {
		SELECT,
		MOVE,
		NONE
	} State;

	State state_;				// ������� ���������.

	//---------------
	// PvP
	//---------------
	Partiya* partiya_;
	int* possibleMoves_;		// ������ ��������� �����
};

//=================================================================================================

char RenderingEngineVBO::board[64] = {
		-ROOK, -BISHOP, -KNIGHT, -KING, -QUEEN, -KNIGHT, -BISHOP, -ROOK,
		-PAWN, -PAWN  , -PAWN  , -PAWN, -PAWN , -PAWN  , -PAWN  , -PAWN,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		PAWN, PAWN  , PAWN  , PAWN , PAWN, PAWN  , PAWN  , PAWN,
		ROOK, BISHOP, KNIGHT, QUEEN, KING, KNIGHT, BISHOP, ROOK
	};  

//=================================================================================================
//==============================�������������======================================================
//=================================================================================================

/// ��������� ������ ��� ������
/// << ��������� �� ���������� ������� ������
IRenderingEngine* CreateRendererVBO()
{
    return new RenderingEngineVBO();
}

RenderingEngineVBO::RenderingEngineVBO()
{
	vboVert_ =  new VertexBuffer();
	speed_ = 0.1;
	animTime_ = 1.0 / speed_;
	generateChess();
}

/// �������� �������
void RenderingEngineVBO::initializeTextures()
{
	std::string pieceNames_[6];
	std::string piecesDirs_[2];

	pieceNames_[0] = "King.png";
	pieceNames_[1] = "Queen.png";
	pieceNames_[2] = "Bishop.png";  
	pieceNames_[3] = "Knight.png";
	pieceNames_[4] = "Rook.png";
	pieceNames_[5] = "Pawn.png";

	std::string s = GetExePath();
	piecesDirs_[0] = s + "\\White\\";
	piecesDirs_[1] = s + "\\Black\\";

	textures_ = new TextureLoader*[12];
	for(int i = 0; i < 6; ++i)
	{
		textures_[i] = new TextureLoader();
		textures_[i]->loadTextureFromFile(piecesDirs_[0] + pieceNames_[i]);
		textures_[i + 6] = new TextureLoader();
		textures_[i + 6]->loadTextureFromFile(piecesDirs_[1] + pieceNames_[i]);
	}
}

/// ������� ������������� ���������
/// >> �������� ����������
void RenderingEngineVBO::setupRelativeCoordCoeff(unsigned int realWH)
{
	relativeCoordCoeff_ = 8.0 / realWH;
}

void RenderingEngineVBO::initialize()
{
	ilInit();

	glClearColor ( 0.5, 0.5, 0.5, 1.0 );

	glHint ( GL_POLYGON_SMOOTH_HINT,         GL_NICEST );
	glHint ( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );

	glAlphaFunc ( GL_ALWAYS, 1 );
	glEnable ( GL_ALPHA_TEST );
	//---
	initializeTextures();
	state_ = State::NONE;

	partiya_ = new Partiya("test.txt");
	partiya_->startPartiyaInitiation();
	possibleMoves_ = new int[MOVE_COUNT];
}

//=================================================================================================

/// ��������� ��������� ������������� �������
/// << ��������� �������
uint RenderingEngineVBO::getNextPosition()
{
	// to do...
	return currentMove_ < mv_.n2 ? ++currentMove_ : --currentMove_;
}

/// ���������� ��������
void RenderingEngineVBO::update()
{
	if(state_ != State::MOVE) return;

	const uint stride = sizeof( Vertex );	
	Vertex vert[4];
	vboVert_->bind(GL_ARRAY_BUFFER_ARB);
	vboVert_->getSubData(256 * stride, 4 * stride, vert);
	uint n;
	if(animTime_ == 0.0)
	{
		n = getNextPosition();
		animTime_ = 1.0 / speed_;
	}
	else
		n = currentMove_;

	uint x = n & 7;
	uint y = n >> 3;
	x+=speed_;
	y+=speed_;
	vert[0].x = x;			vert[0].y = y;
	vert[1].x = x;			vert[1].y = y + 1;
	vert[2].x = x + 1;		vert[2].y = y + 1;
	vert[3].x = x + 1;		vert[3].y = y;
	vboVert_->setSubData(256 * stride, 4 * stride, vert);
	vboVert_->unbind();
	--animTime_;

	if(currentMove_ == mv_.n2)
	{
		board[mv_.n2] = board[mv_.n1];
		board[mv_.n1] = 0;
		state_ = State::NONE;
	}
}

void RenderingEngineVBO::performSelection(bool select)
{
	const uint stride = sizeof( Vertex );	
	Vertex vert[4];
	float r,g,b,a;
	r = g = 1.0;
	b = select ? 0.0 : 1.0;
	a = 1.0;
	for(int n = 0; n < 64; ++n)
	{
		if(!isPossibleMove(n)) continue;

		vboVert_->bind(GL_ARRAY_BUFFER_ARB);
		vboVert_->getSubData(256 * stride, 4 * stride, vert);

		vert[0].r = r;			
		vert[1].g = g;			
		vert[2].b = b;		
		vert[3].a = a;		
		vboVert_->setSubData(256 * stride, 4 * stride, vert);
		vboVert_->unbind();
	}
}

/// ������� ������� ���������
void RenderingEngineVBO::render()
{
	glClear ( GL_COLOR_BUFFER_BIT );

	//------------------------------
	// �������� �����
	bool f = state_ == State::MOVE;
	// board
	const uint stride = sizeof( Vertex );					
	vboVert_->bind(GL_ARRAY_BUFFER_ARB);
	glEnableClientState(GL_VERTEX_ARRAY); 
	glVertexPointer(2, GL_FLOAT, stride, 0);
	glEnableClientState(GL_COLOR_ARRAY);
	glColorPointer(4, GL_FLOAT, stride, reinterpret_cast<void*>(8));
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnable(GL_TEXTURE_2D);
	
	char p = 0;

	// ToDo glDrawRangeElements 
	for(int n = 0, i = 0; n < 64; ++n, i+=4)
	{
		
		p = board[n];
		if(p != 0)
			drawTexturedArray(i, stride, p);
		else
		{
			glDrawArrays(GL_QUADS, i, 4);
		}	
	}

	if(f)
		animateMovement();

	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY); 
	vboVert_->unbind();
}

/// ��������� ���������������� ������ (��������� ������)
/// >>�������� � �������>>������ ���������>>��� ������
void RenderingEngineVBO::drawTexturedArray(int i, const uint stride, int piece)
{
	glBindTexture(GL_TEXTURE_2D, getPieceTexture(piece));
	glTexCoordPointer(2, GL_FLOAT, stride, 0);
	glDrawArrays(GL_QUADS, i, 4);
	glBindTexture(GL_TEXTURE_2D, 0);
}

/// �������� ��������
void RenderingEngineVBO::animateMovement()
{
	const uint stride = sizeof( Vertex );					

	char p = 0;
	p = board[currentMove_];
	if(p != 0)
		drawTexturedArray(256, stride, p);
}

/// ��������� �������������� �������� �� ���� ������
/// >> ������
/// << id ��������
uint RenderingEngineVBO::getPieceTexture(char piece)
{
	return textures_[ piece >= 0 ? piece - 1 : 5 - piece]->getTextureID();
}

/// �������� �� ������ ��������� �����
bool RenderingEngineVBO::isPossibleMove(uint n)
{
	for(int i = 0; i < MOVE_COUNT; ++i)
		if(possibleMoves_[i] == n)
			return true;
	return false;
}

/// ����� ������ �� �����
void RenderingEngineVBO::drawText(char* s, int x, int y, float scaleX, float scaleY)
{
	char* c;
	glPushMatrix();
	glTranslatef(x, y, 0);
	glScalef(scaleX, -scaleY, 1);
	for(c = s; *c != '\0'; ++c)
		glutStrokeCharacter(GLUT_STROKE_ROMAN, *c);
	glPopMatrix();
}

/// ��������� ���������
void RenderingEngineVBO::generateChess()
{
	Vertex vertices[256 + 4];	// (64 �������� * 4 ������� + ������� ��� ��������)
	float r1 = 1.0, g1 = 1.0, b1 = 1.0;
	float r2 = 0.5, g2 = 0.5, b2 = 0.5;
	bool f = false;
	float r,g,b;
	int x,y;
	for(int i = 0, n = 0; i < 64; ++i)
	{
		x = i & 7;
		y = i >> 3;
		vertices[n].x = x;			vertices[n].y = y;
		vertices[n + 1].x = x;		vertices[n + 1].y = y + 1;
		vertices[n + 2].x = x + 1;	vertices[n + 2].y = y + 1;
		vertices[n + 3].x = x + 1;	vertices[n + 3].y = y;
		f = (x + y) % 2;
		r = f ? r1 : r2;
		g = f ? g1 : g2;
		b = f ? b1 : b2;
		vertices[n].r= r;  vertices[n].g = g;
		vertices[n].b = b; vertices[n].a = 1.0;
		vertices[n+1].r = r; vertices[n+1].g = g;
		vertices[n+1].b = b; vertices[n+1].a = 1.0;
		vertices[n+2].r = r; vertices[n+2].g = g;
		vertices[n+2].b = b; vertices[n+2].a = 1.0;
		vertices[n+3].r = r; vertices[n+3].g = g;
		vertices[n+3].b = b; vertices[n+3].a = 1.0;
		n+=4;
	}

	// ��������� ������� ��� ��������
	vertices[256].x = 0;	vertices[256].y = 0;
	vertices[257].x = 0;	vertices[257].y = 1;
	vertices[258].x = 1;	vertices[258].y = 1;
	vertices[259].x = 1;	vertices[259].y = 0;
	vertices[256].r = r1;	vertices[256].g = g1;
	vertices[256].b = b1;	vertices[256].a = 1.0;
	vertices[257].r = r1;	vertices[257].g = g1;
	vertices[257].b = b1;	vertices[257].a = 1.0;
	vertices[258].r = r1;	vertices[258].g = g1;
	vertices[258].b = b1;	vertices[258].a = 1.0;
	vertices[259].r = r1;	vertices[259].g = g1;
	vertices[259].b = b1;	vertices[259].a = 1.0;

	vboVert_->bind(GL_ARRAY_BUFFER_ARB);
	vboVert_->setData(sizeof(Vertex) * 300, vertices, GL_DYNAMIC_DRAW_ARB);
	vboVert_->unbind();
}

/// ���������� ������� ����
/// >>x>>y
void RenderingEngineVBO::mousePressed(int x, int y)
{
	x *= relativeCoordCoeff_;
	y *= relativeCoordCoeff_;
	if(state_ == State::NONE)
	{
		mv_.n1 = y * 8 + x;
		currentMove_ = mv_.n1;
		//possibleMoves_ = partiya_->takeCoord(mv_.n1);
		for(int i = 0; i < MOVE_COUNT; ++i)
			std::cout<<possibleMoves_[i];
		performSelection(true);
		state_ = State::SELECT;
	}
	else if(state_ == State::SELECT)
	{
		mv_.n2 = x * 8 + y;
		state_ = State::MOVE;
	}
	std::cout<<"x: "<<x<<" y: "<<y<<" n1: "<<mv_.n1<<" n2: "<<mv_.n2<<std::endl;
}