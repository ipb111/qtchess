#include <iostream>
#include <stdio.h>
#include <conio.h>

#include "Lan\server.h"
#include "Lan\client.h"

#include "IRenderingEngine.h"

#define w 750
#define h 500

IRenderingEngine* engine;
server s;
client c;

static int window;
static bool isVbo;
static unsigned width;
static unsigned height;

//-------------------------------------------------------------------------------------------------
// Setup
//-------------------------------------------------------------------------------------------------

void Timer(int t)
{
	glutTimerFunc(1, Timer, 1);
	engine->update();
}

void display ()
{
	engine->render();
	//------------------------------
	glutSwapBuffers ();
}

void reshape(int width, int height)
{
	engine->reshape(width, height);
}

void key ( unsigned char key, int x, int y )
{
	if ( key == 27 || key == 'q' || key == 'Q' )	//	quit requested
	{
		glutDestroyWindow(window);
    	exit (0);
	}
	else
	if( key == 'S' || key == 's' )
	{
		s.server_start();
	}
	else
	if( key == 'C' || key == 'c' )
	{
		c.client_start();
	}
	else
	if( key == 'W' || key == 'w' )
	{

	}
	else
	if( key == 'E' || key == 'e' )
	{

	}
		else
	if( key == 'M' || key == 'm' )
	{

	}
	else
	if( key == 'N' || key == 'n' )
	{

	}
}

void mouse(int button, int state, int x, int y)
{
	if(state == GLUT_DOWN)
		engine->mousePressed(x, y);
}

void idle( ) {
    
	display();
    glutPostRedisplay();
}

void menu(int num)
{
	key((char)num, 0, 0);
}

void menustate(int state)
{
    state == GLUT_MENU_IN_USE ? glutIdleFunc(NULL) : glutIdleFunc(idle);
}

void createMenu()
{
	glutMenuStateFunc(menustate);
	glutCreateMenu(menu);
	glutAddMenuEntry("             Chess              ", '\0');
	glutAddMenuEntry("--------------------------------", '\0');
	glutAddMenuEntry("[S] Start server                ", 'S');
	glutAddMenuEntry("[C] Start client                ", 'C');
	glutAddMenuEntry("[W] Save  game                  ", 'W');
	glutAddMenuEntry("[E] Load  game                  ", 'E');
	glutAddMenuEntry("[M] Save  move                  ", 'M');
	glutAddMenuEntry("[N] Load  move                  ", 'N');
	glutAddMenuEntry("[Q] Quit                        ", 27);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main ( int argc, char* argv [] )
{	
	glutInit            ( &argc, argv );
	glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB );
	glutInitWindowSize  ( w, h );

	// create window
	window = glutCreateWindow ( "chess" );
	SetConsoleTitle(L"chess");
	std::cout<<"OpenGL version "<<glGetString( GL_VERSION )<<std::endl;

	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		std::cout<<fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		std::cout<<std::endl;
		getch();
		return 0;
	}
	isVbo = true; 
	if (!glewIsSupported("GL_VERSION_1_4  GL_ARB_vertex_buffer_object"))
	{
		std::cout<<"VBO is not supported!"<<std::endl;
		isVbo = false;
	}

	// !!!
	isVbo = false;

	if(isVbo)
		engine = CreateRendererVBO();
	else
		engine = CreateRenderer();
	
	engine->initialize();
	engine->setupRelativeCoordCoeff(h);

	// register handlers
	glutDisplayFunc  ( display );
	glutReshapeFunc  ( reshape );
	glutKeyboardFunc ( key     );
	glutMouseFunc    ( mouse   );
	createMenu();
	glutTimerFunc(1, Timer, 1);

	glutIdleFunc( idle );
    glutMainLoop ();

	return EXIT_SUCCESS;
}