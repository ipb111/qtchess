/*
Copyright (c) <2013>, <Ilya Shoshin>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "glwidget.h"
#include <QMouseEvent>
#include <QMessageBox>
#include <QTimer>
#include <QDebug>

GLWidget::GLWidget(QWidget *parent)
	: QGLWidget(parent)
{
	// ��������� ������������� � ������� OpenGL 1.1
	if (!(QGLFormat::openGLVersionFlags() & QGLFormat::OpenGL_Version_1_1))
	{
		// ���� ������ OpenGL 1.1 �� �������������� ����������, 
		// �� ������� ����������� ��������� � ��������� ������ ����������
		QMessageBox::critical(0, "Message", "Your platform does not support OpenGL 1.1"); 
		close(); // ���������� ����������
	}
	_engine = CreateRenderer();
	if(!_engine)
	{
		QMessageBox::critical(0, "Message", "Couldn't create renderer"); 
		close(); // ���������� ����������
	}
	_timer = new QTimer(this);
    connect(_timer, SIGNAL(timeout()), this, SLOT(update()));
	connect(_engine, SIGNAL(startAnimation()), this, SLOT(startAnimation()));
	connect(_engine, SIGNAL(stopAnimation()), this, SLOT(stopAnimation()));
}

GLWidget::~GLWidget()
{
	delete _engine;
}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
    return QSize(400, 400);
}

void GLWidget::initializeGL()
{
	_engine->initialize();
	_engine->setupRelativeCoordCoeff(qobject_cast<QWidget*>(this->parent())->height());
}

void GLWidget::resizeGL(int width, int height)
{
	_engine->reshape(width, height);
	updateGL();
}

void GLWidget::paintGL()
{
	_engine->render();
}

void GLWidget::mousePressEvent(QMouseEvent *ev)
{
	_engine->mousePressed(ev->pos().x(), ev->pos().y());
	updateGL();
}

void GLWidget::startAnimation()
{
	 qDebug() << "start timer\n";
	_timer->start(20);
}

void GLWidget::update()
{
	_engine->update();
	updateGL();
}

void GLWidget::stopAnimation()
{
	 qDebug() << "stop timer\n";
	_timer->stop();
}
