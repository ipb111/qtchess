<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AddressDialog</name>
    <message>
        <location filename="addressdialog.cpp" line="21"/>
        <source>Input the ip address of the host.</source>
        <translation>Введите ip адрес компьютера, который выступает в роли хоста.</translation>
    </message>
</context>
<context>
    <name>ChessEngine</name>
    <message>
        <location filename="Chess/PvE/chessengine.cpp" line="124"/>
        <source>Computer&apos;s move: </source>
        <translation>Ход компьютера:</translation>
    </message>
    <message>
        <location filename="Chess/PvE/chessengine.cpp" line="145"/>
        <source>Illegal move.</source>
        <oldsource>Illegal move.
</oldsource>
        <translation>Неверный ход.</translation>
    </message>
</context>
<context>
    <name>ChessWindow</name>
    <message>
        <location filename="chesswindow.cpp" line="133"/>
        <source>&amp;New</source>
        <translation>&amp;Создать</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="134"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="135"/>
        <source>Create a new file</source>
        <translation>Создать новый файл</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="139"/>
        <source>&amp;Open...</source>
        <translation>&amp;Открыть...</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="140"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="141"/>
        <source>Open an existing file</source>
        <translation>Открыть существующий файл...</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="145"/>
        <source>&amp;Save</source>
        <translation>&amp;Сохранить</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="146"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="147"/>
        <source>Save the document</source>
        <translation>Сохранить игру</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="151"/>
        <source>Save &amp;As...</source>
        <translation>&amp;Сохранить как...</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="152"/>
        <source>Save the document as new file</source>
        <translation>Сохранение документа под новым именем</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="155"/>
        <source>&amp;Exit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="156"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="157"/>
        <source>Exit the application</source>
        <translation>Закрыть приложение</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="160"/>
        <source>&amp;Unmove</source>
        <translation>&amp;Отменить ход</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="161"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="162"/>
        <source>Undo the latest move</source>
        <translation>Отменить последний ход</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="166"/>
        <source>&amp;Remove</source>
        <translation>&amp;Восстановить ход</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="167"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="168"/>
        <source>Redo the latest move</source>
        <translation>Восстановить отмененный ход</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="172"/>
        <location filename="chesswindow.cpp" line="195"/>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="173"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="174"/>
        <source>Help...</source>
        <translation>После активации, навести на любой элемент управления для получения справки.</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="181"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="189"/>
        <source>&amp;Edit</source>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="193"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="231"/>
        <source>Game navigation</source>
        <translation>Навигация</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="232"/>
        <source>Shows all moves.</source>
        <translation>Поле &quot;Навигация&quot; показывает все сделанные ходы, а кнопки управления &lt;- И -&gt; , которые в свою очередь позволяют отменить или вернуть отмененный ход. </translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="239"/>
        <source>&lt;-</source>
        <translation>&lt;-</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="240"/>
        <source>-&gt;</source>
        <translation>-&gt;</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="242"/>
        <source>Cancel move.</source>
        <translation>Отменить ход</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="249"/>
        <source>Displaying the last move.</source>
        <translation>Поле Game navigation показывает все сделанные ходы, а кнопки управления &lt;- И -&gt; , которые в свою очередь позволяют отменить или вернуть отмененный ход. </translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="263"/>
        <source>Game score</source>
        <translation>Счёт</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="264"/>
        <source>Shows score.</source>
        <translation>Поле &quot;Счёт&quot; поле отображает сколько фигур было съедено каждой из сторон, а так же чей ход на данный момент. </translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="270"/>
        <source>Black      </source>
        <translation>Чёрные</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="271"/>
        <source>White</source>
        <translation>Белые</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="272"/>
        <source>The current move belongs to black, if selected</source>
        <translation>Если кнопка отмечена, то ход принадлежит чёрным фигурам.</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="273"/>
        <source>The current move belongs to white, if selected.</source>
        <translation>Если кнопка отмечена, то ход принадлежит белым фигурам.</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="274"/>
        <location filename="chesswindow.cpp" line="275"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="294"/>
        <source>Lan settings</source>
        <translation>Сетевая игра</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="295"/>
        <source>Shows lan settings.</source>
        <translation>Поле &quot;Сетевая игра&quot; позволяет создать игру при нажатии кнопки Create game и подключиться к созданной игре с помощью кнопки Join game, введя IP адрес оппонента.</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="299"/>
        <source>Create game</source>
        <translation>Создать игру</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="300"/>
        <source>Create a new game and become a host.</source>
        <translation>Создать игру и стать серверной стороной.</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="301"/>
        <source>Join game</source>
        <translation>Присоединиться к игре</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="302"/>
        <source>Join the game.</source>
        <translation>Подключение к созданной игре, потребуется ввести ip адрес сервера</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="307"/>
        <source>View ip address list.</source>
        <translation>Просмотр списка ip адресов.</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="324"/>
        <source>PVE info</source>
        <translation>Игра с компьютером</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="325"/>
        <source>Shows pve info.</source>
        <translation>Поле &quot;Игра с компьютером&quot; отображает глубину просчета с возможностью изменения этой глубины (от 4 до 7) также режим игры Autoplay позволяющий компьютеру делать ходы вместо игрока. А также отображается Ходы компьютера и символьное отображение доски. </translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="331"/>
        <source>Additional info from the chess engine.</source>
        <translation>Дополнительная информация</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="335"/>
        <source>Board representation in chess engine.</source>
        <translation>Символьное представление доски</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="340"/>
        <source>Depth:</source>
        <translation>Глубина</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="343"/>
        <source>Setup max depth.</source>
        <translation>Установка максимальной глубины просчёта.</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="348"/>
        <source>Autoplay</source>
        <translation>Автоигра</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="349"/>
        <source>Make the computer to play with itself.</source>
        <translation>&quot;Автоигра&quot; позволяет компьютеру делать ходы вместо игрока.</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="388"/>
        <source>Open File</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="389"/>
        <location filename="chesswindow.cpp" line="422"/>
        <source>Text Files (*.txt)</source>
        <translation>Text Files (*.txt)</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="421"/>
        <source>Save File</source>
        <translation>Сохранить файл</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="431"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="431"/>
        <source>Could not open file</source>
        <translation>Не удалось открыть файл.</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="690"/>
        <source>The server is running!</source>
        <translation>Сервер уже запущен!</translation>
    </message>
    <message>
        <location filename="chesswindow.cpp" line="733"/>
        <source>The client is connected!</source>
        <translation>Клиент уже подключён!</translation>
    </message>
</context>
<context>
    <name>StartWindow</name>
    <message>
        <location filename="startwindow.cpp" line="41"/>
        <source>New game</source>
        <translation>Новая игра</translation>
    </message>
    <message>
        <location filename="startwindow.cpp" line="42"/>
        <source>Create new game.</source>
        <translation>Создание новой игры</translation>
    </message>
    <message>
        <location filename="startwindow.cpp" line="43"/>
        <source>Load game</source>
        <translation>Загрузка игры</translation>
    </message>
    <message>
        <location filename="startwindow.cpp" line="44"/>
        <source>Load existing game.</source>
        <translation>Загрузка существующей игры.</translation>
    </message>
    <message>
        <location filename="startwindow.cpp" line="49"/>
        <source>Chess</source>
        <translation>Шахматы</translation>
    </message>
    <message>
        <location filename="startwindow.cpp" line="52"/>
        <source> Game mode:</source>
        <translation>Режим игры</translation>
    </message>
    <message>
        <location filename="startwindow.cpp" line="57"/>
        <source>PvP local</source>
        <translation>Против человека на одном компьютере</translation>
    </message>
    <message>
        <location filename="startwindow.cpp" line="58"/>
        <source>PvP via network</source>
        <translation>Против человека по сети</translation>
    </message>
    <message>
        <location filename="startwindow.cpp" line="59"/>
        <source>PvE</source>
        <translation>Против компьютера</translation>
    </message>
    <message>
        <location filename="startwindow.cpp" line="61"/>
        <source>(c) 2013 ipb-1-11 (MIREA)</source>
        <translation>(c) 2013 ипб-1-11 (МИРЭА)</translation>
    </message>
    <message>
        <location filename="startwindow.cpp" line="85"/>
        <source>Open File</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="startwindow.cpp" line="86"/>
        <source>Text Files (*.txt)</source>
        <translation>Text Files (*.txt)</translation>
    </message>
</context>
</TS>
