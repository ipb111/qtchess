/*
Copyright (c) <2013>, <Ilya Shoshin>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef CHESSWINDOW_H
#define CHESSWINDOW_H

#include "addressdialog.h"
#include <QMainWindow>
#include "Chess/PvP/chessPartiya.h"
#include "Chess/PvP/chessdatatypes.h"
#include "Chess/Lan/client.h"
#include "Chess/Lan/server.h"
#include "Chess/IO/GameIO.h"
#include "Chess/IO/HistoryIO.h"
#include "IPVEChessEngine.h"
#include "glwidget.h"

using namespace NetworkArdic;

class GLWidget;
class QDockWidget;
class QAction;
class QMenu;
class QResizeEvent;
class QRadioButton;
class QLineEdit;
class QPushButton;
class QTextEdit;

class ChessWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit ChessWindow(QMainWindow *parent, int mode, QString gameName = 0);
	~ChessWindow();

	QSize minimumSizeHint() const;
    QSize sizeHint() const;

protected:
	void resizeEvent(QResizeEvent *e) override;

private slots:
	//---------------------------------------------------------------
	// menu slots
    void newGame();
    void openGame();
    void saveGame();
    void saveGameAs();
	void unmove();
	void remove();
    void help();
	// 
	// rendering slots
	void recievePiecePosition(int n1);
	void recieveNewPosition(int n2, bool sendViaLan);
	// lan slots
	void createGame();
	void joinGame();
	void recievedMessage(QString str);
    void infoRecieved(std::string, int);
    // pvp slots
    void switchPlayer(bool black, int score);
    void check();
    // pve slots
    void computerTurn();
    void changeDepth(int);
    void autoMode(bool);
signals:
    void newGameSignal();
	// rendering signals
	void validationSucceed(const int *, int);
    void syncPvPBoard(const int *, const  Piece *);
    // rendering & pve
    void performMove(int, int);
    void selectComputerPiece();
    void cancelComputerSelect();
	//---------------------------------------------------------------
private:	// Menu & Actions
	void createActions();
    void createMenus();
	void createGLWidget();
	void createUI(int mode);
	void createGameNavigation();
	void createGameScore();
	void createGameLan();
    void createGamePVE();
    void saveAs();
	//---------------------------------------------------------------
	QMenu *fileMenu;
    QMenu *editMenu;
	QMenu *viewMenu;
    QMenu *helpMenu;
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
	QAction *unmoveAct;
	QAction *removeAct;
    QAction *helpAct;
    QAction *exitAct;
private:
	// ������ ��� ��������� ��������� �����, ����� � �������� ����
	GLWidget *glWidget_;
	// ����������� ������ (�������� glWidget_ � ������������ ����������� ���������������)
	QWidget *centralWidget_;
	// ������� ������ 
	QDockWidget *dockWidget_;
	// ��� ������� ����
	QString currentGameFile_;
	// score gui objects
	QRadioButton *black_;
	QRadioButton *white_;
	QLineEdit *blackScore_;
	QLineEdit *whiteScore_;
	// navigation gui objects
	QTextEdit *navEdit_;
	// lan gui objects
	QPushButton *buttonCreate_;
	QPushButton *buttonJoin_;
	QTextEdit *lanEdit_;
    // pve gui widgets
    QTextEdit *pveEditInfo_;
    QTextEdit *pveEditBoard_;

private:
    int mode_;
    IRenderingEngine *renderObject_;
	//---------------------------------------------------------------
	// PvP
	//---------------------------------------------------------------
    Partiya *game_;
    int *possibleMoves_;		// ������ ��������� �����
	int  arraySize_;
	uint currentMove_;
    //---------------------------------------------------------------
    // PvE
    //---------------------------------------------------------------
    IPVEChessEngine *pveGame_;
    bool automode_;
	//---------------------------------------------------------------
	// Lan
	//---------------------------------------------------------------
	Server *server_;
	Client *client_;
	//---------------------------------------------------------------
	// IO
	//---------------------------------------------------------------
	HistoryIO *historyIO_;
};

#endif // CHESSWINDOW_H
