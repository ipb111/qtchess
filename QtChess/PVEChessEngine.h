#ifndef PVECHESSENGINE_H
#define PVECHESSENGINE_H

/* see the beginning of think() */
#include <setjmp.h>
#include <stdio.h>

/* get_ms() returns the milliseconds elapsed since midnight,
   January 1, 1970. */
#include <sys/timeb.h>

#define GEN_STACK		1120
#define MAX_PLY			32
#define HIST_STACK		400

#define LIGHT			0
#define DARK			1

#define PAWN			0
#define KNIGHT			1
#define BISHOP			2
#define ROOK			3
#define QUEEN			4
#define KING			5

#define EMPTY			6

/* useful squares */
#define A1				56
#define B1				57
#define C1				58
#define D1				59
#define E1				60
#define F1				61
#define G1				62
#define H1				63
#define A8				0
#define B8				1
#define C8				2
#define D8				3
#define E8				4
#define F8				5
#define G8				6
#define H8				7

#define ROW(x)			(x >> 3)
#define COL(x)			(x & 7)

///////////////////////////EVALUATION////////////////////////////
#define DOUBLED_PAWN_PENALTY		10
#define ISOLATED_PAWN_PENALTY		20
#define BACKWARDS_PAWN_PENALTY		8
#define PASSED_PAWN_BONUS			20
#define ROOK_SEMI_OPEN_FILE_BONUS	10
#define ROOK_OPEN_FILE_BONUS		15
#define ROOK_ON_SEVENTH_BONUS		20


/* This is the basic description of a move. promote is what
   piece to promote the pawn to, if the move is a pawn
   promotion. bits is a bitfield that describes the move,
   with the following bits:

   1	capture
   2	castle
   4	en passant capture
   8	pushing a pawn 2 squares
   16	pawn move
   32	promote

   It's union'ed with an integer so two moves can easily
   be compared with each other. */

typedef struct {
    char from;
    char to;
    char promote;
    char bits;
} move_bytes;

typedef union {
    move_bytes b;
    int u;
} move;

/* an element of the move stack. it's just a move with a
   score, so it can be sorted by the search functions. */
typedef struct {
    move m;
    int score;
} gen_t;

/* an element of the history stack, with the information
   necessary to take a move back. */
typedef struct {
    move m;
    int capture;
    int castle;
    int ep;
    int fifty;
    int hash;
} hist_t;

class PVEChessEngine
{
public:
    PVEChessEngine();
public:
    /////////////////////////////////////////////////////////////////
    ///////////////////////////BOARD/////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /* init_board() sets the board to the initial game state. */
    void init_board();

    /* init_hash() initializes the random numbers used by set_hash(). */
    void init_hash();

    /* hash_rand() XORs some shifted random numbers together to make sure
       we have good coverage of all 32 bits. (rand() returns 16-bit numbers
       on some systems.) */
    int hash_rand();

    /* set_hash() uses the Zobrist method of generating a unique number (hash)
       for the current chess position. Of course, there are many more chess
       positions than there are 32 bit numbers, so the numbers generated are
       not really unique, but they're unique enough for our purposes (to detect
       repetitions of the position).
       The way it works is to XOR random numbers that correspond to features of
       the position, e.g., if there's a black knight on B8, hash is XORed with
       hash_piece[BLACK][KNIGHT][B8]. All of the pieces are XORed together,
       hash_side is XORed if it's black's move, and the en passant square is
       XORed if there is one. (A chess technicality is that one position can't
       be a repetition of another if the en passant state is different.) */
    void set_hash();

    /* in_check() returns TRUE if side s is in check and FALSE
       otherwise. It just scans the board to find side s's king
       and calls attack() to see if it's being attacked. */
    bool in_check(int s);

    /* attack() returns TRUE if square sq is being attacked by side
       s and FALSE otherwise. */
    bool attack(int sq, int s);

    /* gen() generates pseudo-legal moves for the current position.
       It scans the board to find friendly pieces and then determines
       what squares they attack. When it finds a piece/square
       combination, it calls gen_push to put the move on the "move
       stack." */
    void gen();

    /* gen_caps() is basically a copy of gen() that's modified to
       only generate capture and promote moves. It's used by the
       quiescence search. */
    void gen_caps();

    /* gen_push() puts a move on the move stack, unless it's a
       pawn promotion that needs to be handled by gen_promote().
       It also assigns a score to the move for alpha-beta move
       ordering. If the move is a capture, it uses MVV/LVA
       (Most Valuable Victim/Least Valuable Attacker). Otherwise,
       it uses the move's history heuristic value. Note that
       1,000,000 is added to a capture move's score, so it
       always gets ordered above a "normal" move. */
    void gen_push(int from, int to, int bits);

    /* gen_promote() is just like gen_push(), only it puts 4 moves
       on the move stack, one for each possible promotion piece */
    void gen_promote(int from, int to, int bits);

    /* makemove() makes a move. If the move is illegal, it
       undoes whatever it did and returns FALSE. Otherwise, it
       returns TRUE. */
    bool makemove(move_bytes m);

    /* takeback() is very similar to makemove(), only backwards :)  */
    void takeback();

    /////////////////////////////////////////////////////////////////
    ///////////////////////////SEARCH////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /* think() calls search() iteratively. Search statistics
       are printed depending on the value of output:
       0 = no output
       1 = normal output
       2 = xboard format output */
    void think(int output);

    /* search() does just that, in negamax fashion */
    int search(int alpha, int beta, int depth);

    /* quiesce() is a recursive minimax search function with
       alpha-beta cutoffs. In other words, negamax. It basically
       only searches capture sequences and allows the evaluation
       function to cut the search off (and set alpha). The idea
       is to find a position where there isn't a lot going on
       so the static evaluation function will work. */
    int quiesce(int alpha,int beta);

    /* reps() returns the number of times the current position
       has been repeated. It compares the current value of hash
       to previous values. */
    int reps();

    /* sort_pv() is called when the search function is following
       the PV (Principal Variation). It looks through the current
       ply's move list to see if the PV move is there. If so,
       it adds 10,000,000 to the move's score so it's played first
       by the search function. If not, follow_pv remains FALSE and
       search() stops calling sort_pv(). */
    void sort_pv();

    /* sort() searches the current ply's move list from 'from'
       to the end to find the move with the highest score. Then it
       swaps that move and the 'from' move so the move with the
       highest score gets searched next, and hopefully produces
       a cutoff. */
    void sort(int from);

    /* checkup() is called once in a while during the search. */
    void checkup();

    /////////////////////////////////////////////////////////////////
    ///////////////////////////EVALUATION////////////////////////////
    /////////////////////////////////////////////////////////////////

    int eval();

    int eval_light_pawn(int sq);

    int eval_dark_pawn(int sq);

    int eval_light_king(int sq);

    /* eval_lkp(f) evaluates the Light King Pawn on file f */

    int eval_lkp(int f);

    int eval_dark_king(int sq);

    int eval_dkp(int f);

private:
    /* the board representation */
    int color[64];  /* LIGHT, DARK, or EMPTY */
    int piece[64];  /* PAWN, KNIGHT, BISHOP, ROOK, QUEEN, KING, or EMPTY */
    int side;       /* the side to move */
    int xside;      /* the side not to move */
    int castle;     /* a bitfield with the castle permissions. if 1 is set,
                       white can still castle kingside. 2 is white queenside.
                       4 is black kingside. 8 is black queenside. */
    int ep;         /* the en passant square. if white moves e2e4, the en passant
                       square is set to e3, because that's where a pawn would move
                       in an en passant capture */
    int fifty;      /* the number of moves since a capture or pawn move, used
                       to handle the fifty-move-draw rule */
    int hash;       /* a (more or less) unique number that corresponds to the
                       position */
    int ply;        /* the number of half-moves (ply) since the
                       root of the search tree */
    int hply;       /* h for history; the number of ply since the beginning
                       of the game */

    /* gen_dat is some memory for move lists that are created by the move
       generators. The move list for ply n starts at first_move[n] and ends
       at first_move[n + 1]. */
    gen_t gen_dat[GEN_STACK];
    int first_move[MAX_PLY];

    /* the history heuristic array (used for move ordering) */
    int history[64][64];

    /* we need an array of hist_t's so we can take back the
       moves we make */
    hist_t hist_dat[HIST_STACK];

    /* the engine will search for max_time milliseconds or until it finishes
       searching max_depth ply. */
    int max_time;
    int max_depth;

    /* the time when the engine starts searching, and when it should stop */
    int start_time;
    int stop_time;

    int nodes;  /* the number of nodes we've searched */

    /* a "triangular" PV array; for a good explanation of why a triangular
       array is needed, see "How Computers Play Chess" by Levy and Newborn. */
    move pv[MAX_PLY][MAX_PLY];
    int pv_length[MAX_PLY];
    bool follow_pv;

    /* random numbers used to compute hash; see set_hash() in board.c */
    int hash_piece[2][6][64];  /* indexed by piece [color][type][square] */
    int hash_side;
    int hash_ep[64];

    /* Now we have the mailbox array, so called because it looks like a
       mailbox, at least according to Bob Hyatt. This is useful when we
       need to figure out what pieces can go where. Let's say we have a
       rook on square a4 (32) and we want to know if it can move one
       square to the left. We subtract 1, and we get 31 (h5). The rook
       obviously can't move to h5, but we don't know that without doing
       a lot of annoying work. Sooooo, what we do is figure out a4's
       mailbox number, which is 61. Then we subtract 1 from 61 (60) and
       see what mailbox[60] is. In this case, it's -1, so it's out of
       bounds and we can forget it. You can see how mailbox[] is used
       in attack() in board.c. */
    static int mailbox[120];
    static int mailbox64[64];
    /* slide, offsets, and offset are basically the vectors that
       pieces can move in. If slide for the piece is FALSE, it can
       only move one square in any one direction. offsets is the
       number of directions it can move in, and offset is an array
       of the actual directions. */
    static bool slide[6];
    static int offsets[6];
    static int offset[6][8];
    /* This is the castle_mask array. We can use it to determine
       the castling permissions after a move. What we do is
       logical-AND the castle bits with the castle_mask bits for
       both of the move's squares. Let's say castle is 1, meaning
       that white can still castle kingside. Now we play a move
       where the rook on h1 gets captured. We AND castle with
       castle_mask[63], so we have 1&14, and castle becomes 0 and
       white can't castle kingside anymore. */
    static int castle_mask[64];
    /* the piece letters, for print_board() */
    static char piece_char[6];
    /* the initial board state */
    static int init_color[64];
    static int init_piece[64];

    ///////////////////////////EVALUATION////////////////////////////

    /* the values of the pieces */
    static int piece_value[6];

    /* The "pcsq" arrays are piece/square tables. They're values
       added to the material value of the piece based on the
       location of the piece. */
    static int pawn_pcsq[64];

    static int knight_pcsq[64];

    static int bishop_pcsq[64];

    static int king_pcsq[64];

    static int king_endgame_pcsq[64];

    /* The flip array is used to calculate the piece/square
       values for DARK pieces. The piece/square value of a
       LIGHT pawn is pawn_pcsq[sq] and the value of a DARK
       pawn is pawn_pcsq[flip[sq]] */
    static int flip[64];

    /* pawn_rank[x][y] is the rank of the least advanced pawn of color x on file
       y - 1. There are "buffer files" on the left and right to avoid special-case
       logic later. If there's no pawn on a rank, we pretend the pawn is
       impossibly far advanced (0 for LIGHT and 7 for DARK). This makes it easy to
       test for pawns on a rank and it simplifies some pawn evaluation code. */
    int pawn_rank[2][10];

    int piece_mat[2];  /* the value of a side's pieces */
    int pawn_mat[2];  /* the value of a side's pawns */

    bool ftime_ok = false;  /* does ftime return milliseconds? */

    int get_ms();

    jmp_buf env;
    bool stop_search;

    /////////////////////////////////////////////////////////////////
    /// MAIN
    /* move_str returns a string with move m in coordinate notation */
    char *move_str(move_bytes m);
    /* parse the move s (in coordinate notation) and return the move's
       index in gen_dat, or -1 if the move is illegal */
    int parse_move(char *s);
    /////////////////////////////////////////////////////////////////
    /// BOOK
    /* the opening book file, declared here so we don't have to include stdio.h in
       a header file */
    FILE *book_file;

    /* open_book() opens the opening book file and initializes the random number
       generator so we play random book moves. */

    void open_book();

    /* close_book() closes the book file. This is called when the program exits. */
    void close_book();

    /* book_move() returns a book move (in integer format) or -1 if there is no
       book move. */
    int book_move();

    /* book_match() returns TRUE if the first part of s2 matches s1. */
    bool book_match(char *s1, char *s2);

};

#endif // PVECHESSENGINE_H
