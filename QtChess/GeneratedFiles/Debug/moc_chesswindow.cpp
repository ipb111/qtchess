/****************************************************************************
** Meta object code from reading C++ file 'chesswindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.1.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../chesswindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'chesswindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.1.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ChessWindow_t {
    QByteArrayData data[23];
    char stringdata[222];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_ChessWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_ChessWindow_t qt_meta_stringdata_ChessWindow = {
    {
QT_MOC_LITERAL(0, 0, 11),
QT_MOC_LITERAL(1, 12, 17),
QT_MOC_LITERAL(2, 30, 0),
QT_MOC_LITERAL(3, 31, 10),
QT_MOC_LITERAL(4, 42, 7),
QT_MOC_LITERAL(5, 50, 8),
QT_MOC_LITERAL(6, 59, 8),
QT_MOC_LITERAL(7, 68, 10),
QT_MOC_LITERAL(8, 79, 6),
QT_MOC_LITERAL(9, 86, 6),
QT_MOC_LITERAL(10, 93, 5),
QT_MOC_LITERAL(11, 99, 20),
QT_MOC_LITERAL(12, 120, 2),
QT_MOC_LITERAL(13, 123, 18),
QT_MOC_LITERAL(14, 142, 2),
QT_MOC_LITERAL(15, 145, 10),
QT_MOC_LITERAL(16, 156, 12),
QT_MOC_LITERAL(17, 169, 5),
QT_MOC_LITERAL(18, 175, 5),
QT_MOC_LITERAL(19, 181, 10),
QT_MOC_LITERAL(20, 192, 8),
QT_MOC_LITERAL(21, 201, 15),
QT_MOC_LITERAL(22, 217, 3)
    },
    "ChessWindow\0validationSucceed\0\0"
    "const int*\0newGame\0openGame\0saveGame\0"
    "saveGameAs\0unmove\0remove\0about\0"
    "recievePiecePosition\0n1\0recieveNewPosition\0"
    "n2\0sendViaLan\0switchPlayer\0black\0score\0"
    "createGame\0joinGame\0recievedMessage\0"
    "str\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ChessWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   84,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
       4,    0,   89,    2, 0x08,
       5,    0,   90,    2, 0x08,
       6,    0,   91,    2, 0x08,
       7,    0,   92,    2, 0x08,
       8,    0,   93,    2, 0x08,
       9,    0,   94,    2, 0x08,
      10,    0,   95,    2, 0x08,
      11,    1,   96,    2, 0x08,
      13,    2,   99,    2, 0x08,
      16,    2,  104,    2, 0x08,
      19,    0,  109,    2, 0x08,
      20,    0,  110,    2, 0x08,
      21,    1,  111,    2, 0x08,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int,    2,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int, QMetaType::Bool,   14,   15,
    QMetaType::Void, QMetaType::Bool, QMetaType::Int,   17,   18,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   22,

       0        // eod
};

void ChessWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ChessWindow *_t = static_cast<ChessWindow *>(_o);
        switch (_id) {
        case 0: _t->validationSucceed((*reinterpret_cast< const int*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->newGame(); break;
        case 2: _t->openGame(); break;
        case 3: _t->saveGame(); break;
        case 4: _t->saveGameAs(); break;
        case 5: _t->unmove(); break;
        case 6: _t->remove(); break;
        case 7: _t->about(); break;
        case 8: _t->recievePiecePosition((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->recieveNewPosition((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 10: _t->switchPlayer((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 11: _t->createGame(); break;
        case 12: _t->joinGame(); break;
        case 13: _t->recievedMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ChessWindow::*_t)(const int * , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ChessWindow::validationSucceed)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject ChessWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_ChessWindow.data,
      qt_meta_data_ChessWindow,  qt_static_metacall, 0, 0}
};


const QMetaObject *ChessWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ChessWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ChessWindow.stringdata))
        return static_cast<void*>(const_cast< ChessWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int ChessWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void ChessWindow::validationSucceed(const int * _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
