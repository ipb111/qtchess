/*
Copyright (c) <2013>, <Ilya Shoshin>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "chesswindow.h"

#define DEBUG

#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QFileDialog>
#include <QMessageBox>
#include <QHBoxLayout>
#include <QFrame>
#include <QResizeEvent>
#include <QPushButton>
#include <QDockWidget>
#include <QRadioButton>
#include <QTextEdit>
#include <QLineEdit>
#include <QSpinBox>
#include <QCheckBox>
#include <QDesktopServices>
#include <QTextBrowser>
#include <QWhatsThis>

#ifdef DEBUG
#include <QDebug>
#endif
#include "flowlayout.h"

ChessWindow::ChessWindow(QMainWindow *parent, int mode, QString gameName)
    : QMainWindow(parent), mode_(mode), game_(0), possibleMoves_(0),
    arraySize_(0), currentMove_(0), pveGame_(0), server_(0), client_(0), historyIO_(0)
{
    currentMove_ = 0;   // our
    automode_ = false;
	QSizePolicy p(sizePolicy());
    p.setHeightForWidth(true);
    setSizePolicy(p);
	createActions();    // �������� ������������ �������
    createMenus();		// �������� ����
	createUI(mode);
	//-------------------------------------------
	// Rendering
	//-------------------------------------------
    renderObject_ = glWidget_->getObject();
    connect(renderObject_, SIGNAL(recievePiecePosition(int)),
		this, SLOT(recievePiecePosition(int)));
    connect(renderObject_, SIGNAL(recieveNewPosition(int, bool)),
		this, SLOT(recieveNewPosition(int, bool)));
	connect(this ,SIGNAL(validationSucceed(const int *, int)),
        renderObject_, SLOT(validationSucceed(const int *, int)));
    if(pveGame_)
    {
        connect(this, SIGNAL(performMove(int,int)), renderObject_, SLOT(performMove(int, int)));
        connect(this, SIGNAL(selectComputerPiece()), renderObject_, SLOT(selectComputerPiece()));
        connect(renderObject_, SIGNAL(stopAnimation()), this, SLOT(computerTurn()));
        connect(this, SIGNAL(cancelComputerSelect()), renderObject_, SLOT(cancelComputerSelect()));
        connect(pveGame_, SIGNAL(SyncBoard(int *, int *)), renderObject_, SLOT(SyncBoard(int *, int *)));
    }
	//-------------------------------------------
	// PvP
	//-------------------------------------------
    game_ = new Partiya(this);
	game_->startPartiyaInitiation();
	currentMove_ = game_->getWhichTurn();
	connect(game_, SIGNAL(switchPlayer(bool, int)), this, SLOT(switchPlayer(bool, int)));
    connect(game_, SIGNAL(check()), this, SLOT(check()));
    connect(this, SIGNAL(syncPvPBoard(const int*, const Piece*)), renderObject_, SLOT(SyncBoardFromPvP(const int *, const Piece *)));
	//-------------------------------------------
	// IO
	//-------------------------------------------
    historyIO_ = new HistoryIO(HistoryIO::MODE_MV_BY_MV_ONLY_LOCAL);
    if(!gameName.isNull() && !gameName.isEmpty())
    {
        #warning BUG ToDo: check renderObject_ and fix the bug

        //GameIO gameIO(gameName.toStdString(), GameIO::READ);
        //bool p = 0;
        //gameIO.loadGame(game_->setMap(), game_->setPieces(), p);
        //game_->setTurn(p);
        //emit syncPvPBoard(game_->getMap(), game_->getPieces());
    }
}

ChessWindow::~ChessWindow() 
{
	delete[] possibleMoves_;
    delete historyIO_;
    delete game_;
    delete pveGame_;
}
//--------------------------------------------------------------------------
QSize ChessWindow::minimumSizeHint() const
{
  return QSize(760, 540);
}

QSize ChessWindow::sizeHint() const
{
  return QSize(760, 540);
}
//--------------------------------------------------------------------------
// GUI
//--------------------------------------------------------------------------
void ChessWindow::createActions()
{
	 newAct = new QAction(tr("&New"), this);
     newAct->setShortcut(tr("Ctrl+N"));
     newAct->setStatusTip(tr("Create a new file"));
     newAct->setIcon(QIcon(":/icons/file.ico"));
     connect(newAct, SIGNAL(triggered()), this, SLOT(newGame()));

     openAct = new QAction(tr("&Open..."), this);
     openAct->setShortcut(tr("Ctrl+O"));
     openAct->setStatusTip(tr("Open an existing file"));
     openAct->setIcon(QIcon(":/icons/open.ico"));
     connect(openAct, SIGNAL(triggered()), this, SLOT(openGame()));

     saveAct = new QAction(tr("&Save"), this);
     saveAct->setShortcut(tr("Ctrl+S"));
     saveAct->setStatusTip(tr("Save the document"));
     saveAct->setIcon(QIcon(":/icons/save.ico"));
     connect(saveAct, SIGNAL(triggered()), this, SLOT(saveGame()));

     saveAsAct = new QAction(tr("Save &As..."), this);
     saveAsAct->setStatusTip(tr("Save the document as new file"));
     connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveGameAs()));

     exitAct = new QAction(tr("&Exit"), this);
     exitAct->setShortcut(tr("Ctrl+Q"));
     exitAct->setStatusTip(tr("Exit the application"));
     connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

     unmoveAct = new QAction(tr("&Unmove"), this);
     unmoveAct->setShortcut(tr("Ctrl+B"));
     unmoveAct->setStatusTip(tr("Undo the latest move"));
     unmoveAct->setIcon(QIcon(":/icons/unmove.ico"));
     connect(unmoveAct, SIGNAL(triggered()), this, SLOT(unmove()));

	 removeAct = new QAction(tr("&Remove"), this);
     removeAct->setShortcut(tr("Ctrl+F"));
     removeAct->setStatusTip(tr("Redo the latest move"));
     removeAct->setIcon(QIcon(":/icons/remove.ico"));
     connect(removeAct, SIGNAL(triggered()), this, SLOT(remove()));

     helpAct = new QAction(tr("&Help"), this);
     helpAct->setShortcut(tr("Ctrl+H"));
     helpAct->setStatusTip(tr("Help..."));
     connect( helpAct, SIGNAL(triggered()), this, SLOT(help()));
     //connect(helpAct, SIGNAL(triggered()), this, SLOT(help()));
}

void ChessWindow::createMenus()
 {
     fileMenu = menuBar()->addMenu(tr("&File"));
     fileMenu->addAction(newAct);
     fileMenu->addAction(openAct);
     fileMenu->addAction(saveAct);
     fileMenu->addAction(saveAsAct);
     fileMenu->addSeparator();
     fileMenu->addAction(exitAct);

     editMenu = menuBar()->addMenu(tr("&Edit"));
     editMenu->addAction(unmoveAct);
	 editMenu->addAction(removeAct);

	 viewMenu = menuBar()->addMenu(tr("&View"));

     helpMenu = menuBar()->addMenu(tr("&Help"));
     //helpMenu->addAction(helpAct);
     helpMenu->addAction(QWhatsThis::createAction());
 }

void ChessWindow::createGLWidget()
{
	centralWidget_ = new QWidget(this);
	glWidget_ = new GLWidget(centralWidget_);
	int h = centralWidget_->sizeHint().height();
	glWidget_->setGeometry(QRect(QPoint(0,0),QSize(h, h)));
	setCentralWidget(centralWidget_);
}

void ChessWindow::createUI(int mode)
{
	createGLWidget();
	createGameNavigation();
	createGameScore();
	switch(mode)
	{
	case 0:		// PvP local
		break;
	case 1:		// PvP lan
		createGameLan();
		break;
	case 2:		// PvC
        createGamePVE();
		break;
	default:
		break;
	}
}

void ChessWindow::createGameNavigation()
{
	dockWidget_ = new QDockWidget(tr("Game navigation"), this);
    dockWidget_ ->setWhatsThis(tr("Shows all moves."));
	dockWidget_->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	dockWidget_->setFixedHeight(180);
    dockWidget_->setFixedWidth(220);

	QWidget *area = new QWidget(dockWidget_);
	FlowLayout *layout = new FlowLayout(area); 
	QPushButton *unMove = new QPushButton(tr("<-"), area);
	QPushButton *reMove = new QPushButton(tr("->"), area);
    unMove->setFixedWidth(90);
    unMove->setToolTip(tr("Cancel move."));
    reMove->setFixedWidth(90);
#warning reMove is invisible
    reMove->setVisible(false);
    navEdit_ = new QTextEdit(area);
    navEdit_->setMaximumSize(190,100);
    navEdit_->setReadOnly(true);
    navEdit_->setToolTip(tr("Displaying the last move."));
	layout->addWidget(unMove);
	layout->addWidget(reMove);
    layout->addWidget(navEdit_);
	area->setLayout(layout);
	dockWidget_->setWidget(area);
	addDockWidget(Qt::RightDockWidgetArea, dockWidget_);
	viewMenu->addAction(dockWidget_->toggleViewAction());
    connect(unMove, SIGNAL(clicked()), this, SLOT(unmove()));
    connect(reMove, SIGNAL(clicked()), this,  SLOT(remove()));
}

void ChessWindow::createGameScore()
{
	dockWidget_ = new QDockWidget(tr("Game score"), this);
    dockWidget_ ->setWhatsThis(tr("Shows score."));
	dockWidget_->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	dockWidget_->setFixedHeight(100);
    dockWidget_->setFixedWidth(220);
	QWidget *area = new QWidget(dockWidget_);
	FlowLayout *layout = new FlowLayout(area); 
	black_ = new QRadioButton(tr("Black      "), area);
	white_ = new QRadioButton(tr("White"), area);
    black_->setToolTip(tr("The current move belongs to black, if selected"));
    white_->setToolTip(tr("The current move belongs to white, if selected."));
	blackScore_ = new QLineEdit(tr("0"), area);
	whiteScore_ = new QLineEdit(tr("0"), area);
	blackScore_->setMaximumWidth(80);
	whiteScore_->setMaximumWidth(80);
	blackScore_->setReadOnly(true);
	whiteScore_->setReadOnly(true);
	white_->setChecked(true);
	black_->setCheckable(false);
	layout->addWidget(black_);
	layout->addWidget(white_);
	layout->addWidget(blackScore_);
	layout->addWidget(whiteScore_);
	area->setLayout(layout);
	dockWidget_->setWidget(area);
	addDockWidget(Qt::RightDockWidgetArea, dockWidget_);
	viewMenu->addAction(dockWidget_->toggleViewAction());
}

void ChessWindow::createGameLan()
{
	dockWidget_ = new QDockWidget(tr("Lan settings"), this);
    dockWidget_ ->setWhatsThis(tr("Shows lan settings."));
	dockWidget_->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    dockWidget_->setFixedHeight(220);
    dockWidget_->setFixedWidth(220);
	buttonCreate_ = new QPushButton(tr("Create game"), this);
    buttonCreate_->setToolTip(tr("Create a new game and become a host."));
	buttonJoin_ = new QPushButton(tr("Join game"), this);
    buttonJoin_->setToolTip(tr("Join the game."));
    buttonCreate_->setFixedWidth(200);
    buttonJoin_->setFixedWidth(200);
	QWidget *area = new QWidget(dockWidget_);
	lanEdit_ = new QTextEdit(area);
    lanEdit_->setToolTip(tr("View ip address list."));
    lanEdit_->setMaximumSize(190,100);
    lanEdit_->setReadOnly(true);
	FlowLayout *layout = new FlowLayout(area);
	layout->addWidget(buttonCreate_);
	layout->addWidget(buttonJoin_);
	layout->addWidget(lanEdit_);
	area->setLayout(layout);
	dockWidget_->setWidget(area);
	addDockWidget(Qt::RightDockWidgetArea, dockWidget_);
	viewMenu->addAction(dockWidget_->toggleViewAction());
	connect(buttonCreate_, SIGNAL(clicked()), this, SLOT(createGame())); 
	connect(buttonJoin_, SIGNAL(clicked()), this, SLOT(joinGame())); 
}

void ChessWindow::createGamePVE()
{
    dockWidget_ = new QDockWidget(tr("PVE info"), this);
    dockWidget_ ->setWhatsThis(tr("Shows pve info."));
    dockWidget_->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    dockWidget_->setFixedHeight(350);
    dockWidget_->setFixedWidth(220);
    QWidget *area = new QWidget(dockWidget_);
    pveEditInfo_ = new QTextEdit(area);
    pveEditInfo_->setToolTip(tr("Additional info from the chess engine."));
    pveEditInfo_->setMaximumSize(190,100);
    pveEditInfo_->setReadOnly(true);
    pveEditBoard_ = new QTextEdit(area);
    pveEditBoard_->setToolTip(tr("Board representation in chess engine."));
    pveEditBoard_->setMaximumSize(190,220);
    pveEditBoard_->setReadOnly(true);
    QLineEdit *lbl = new QLineEdit(area);
    lbl->setMaximumWidth(70);
    lbl->setText(tr("Depth:"));
    lbl->setReadOnly(true);
    QSpinBox *spinBoxDepth_ = new QSpinBox(area);
    spinBoxDepth_->setToolTip(tr("Setup max depth."));
    spinBoxDepth_->setMinimum(4);
    spinBoxDepth_->setMaximum(7);
    spinBoxDepth_->setValue(4);
    connect(spinBoxDepth_, SIGNAL(valueChanged(int)), this, SLOT(changeDepth(int)));
    QCheckBox *checkAutoMode = new QCheckBox(tr("Autoplay"), area);
    checkAutoMode->setToolTip(tr("Make the computer to play with itself."));
    checkAutoMode->setChecked(false);
    connect(checkAutoMode, SIGNAL(clicked(bool)), this, SLOT(autoMode(bool)));
    FlowLayout *layout = new FlowLayout(area);
    layout->addWidget(lbl);
    layout->addWidget(spinBoxDepth_);
    layout->addWidget(checkAutoMode);
    layout->addWidget(pveEditInfo_);
    layout->addWidget(pveEditBoard_);
    area->setLayout(layout);
    dockWidget_->setWidget(area);
    addDockWidget(Qt::RightDockWidgetArea, dockWidget_);
    viewMenu->addAction(dockWidget_->toggleViewAction());

    pveGame_ = CreateChessEngine();
    pveGame_->Init();
    pveGame_->SetupMaxDepth(4);
    pveGame_->SetupMaxTime(1<<25);
    //pveGame_->SetupMaxDepth(7);
    //pveGame_->SetupMaxTime(1<<30);
    connect(pveGame_, SIGNAL(SendData(std::string, int)), this, SLOT(infoRecieved(std::string, int)));
}

//--------------------------------------------------------------------------
// ����������� �������
//------------------------------------------------------------------------------
// �������� ������ �����
void ChessWindow::newGame()
{
    if(pveGame_)
       pveGame_->New();
    emit newGameSignal();
}

// �������� �����
void ChessWindow::openGame()
{
    // �������� ����������� ���� � ��������� ����� �����

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "",
                                                    tr("Text Files (*.txt)"));

	currentGameFile_ = fileName;

    if(fileName != "") {

        GameIO gameIO(fileName.toStdString(), GameIO::READ);
        bool p = 0;
        gameIO.loadGame(game_->setMap(), game_->setPieces(), p);
        game_->setTurn(p);
        emit syncPvPBoard(game_->getMap(), game_->getPieces());
    }
}

void ChessWindow::saveGame()
{
    if(currentGameFile_.isNull() || currentGameFile_.isEmpty())
        saveGameAs();
    else
    {
        GameIO gameIO(currentGameFile_.toStdString(), GameIO::WRITE);
        gameIO.saveGame(game_->getMap(),  game_->getPieces(), game_->getWhichTurn());
    }
}

void ChessWindow::saveAs()
{
    saveGameAs();
}

void ChessWindow::saveGameAs()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "",
                                                    tr("Text Files (*.txt)"));

    currentGameFile_ = fileName;

    if(fileName != "") {

        GameIO gameIO(fileName.toStdString(), GameIO::WRITE);
        gameIO.saveGame(game_->getMap(),  game_->getPieces(), game_->getWhichTurn());
    }
    else QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
}

void ChessWindow::unmove()
{
    try{
        auto data = historyIO_->unMove();
        HistoryIO::CellData d = (HistoryIO::CellData)data.second;
        game_->unMove(data.first, d.pN1, d.pN21);
        emit syncPvPBoard(game_->getMap(), game_->getPieces());
        navEdit_->setFocus();
        QTextCursor storeCursorPos = navEdit_->textCursor();
        navEdit_->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
        navEdit_->moveCursor(QTextCursor::StartOfLine, QTextCursor::MoveAnchor);
        navEdit_->moveCursor(QTextCursor::End, QTextCursor::KeepAnchor);
        navEdit_->textCursor().removeSelectedText();
        navEdit_->textCursor().deletePreviousChar();
        navEdit_->setTextCursor(storeCursorPos);
        if(pveGame_)
        {
            pveGame_->Unmove();
            pveGame_->Sync();
        }
    }
    catch(...)
    { }
}

void ChessWindow::remove()
{
    try{
        historyIO_->reMove();
        // To Do: remove in game_
        qDebug() << "#warning Unfinished\n";
        QString str = QString::fromStdString(historyIO_->getMoveString());
        navEdit_->append(str);
    }
    catch(...)
    { }
}

void ChessWindow::help()
{
    QWidget *webWidget = new QWidget;
    webWidget->setAttribute( Qt::WA_DeleteOnClose);
    QHBoxLayout *layout = new QHBoxLayout(webWidget);
    QTextBrowser *webView = new QTextBrowser(webWidget);
    QFile f;
    f.setFileName(":/help/help.html");
    bool bResult = f.open(QIODevice::ReadOnly);
    if(bResult)
    {
        QString str = f.readAll();
        f.close();
        webView->setHtml(str);
        //webView->setStyleSheet("background: #ffffff");
        webView->setWindowTitle("Chess help");
        layout->addWidget(webView);
        webWidget->setLayout(layout);
        webWidget->show();
    }
}

void ChessWindow::resizeEvent(QResizeEvent *e)
{
	QMainWindow::resizeEvent(e);
	
	int h = centralWidget_->size().height();
	glWidget_->resize(QSize(h, h));
}

//-------------------------------------------------------------------
// ����������� �������� ������ RenderingEngine
//-------------------------------------------------------------------
void ChessWindow::recievePiecePosition(int n1)
{
    switch(mode_)
    {
        case 0: // PvP
        case 1:
        {
            if(possibleMoves_)
            {
                delete[] possibleMoves_;
                possibleMoves_ = 0;
            }

            bool success;
            try
            {
                success = game_->takeCoord(n1, &possibleMoves_, arraySize_);
                historyIO_->setPiecePosition(n1);   //  ���������� � ����
            }
            catch(const std::exception& e)
            {
                success = false;
        #ifdef DEBUG
                qDebug() << "takeCoord(n1) produced: "<<e.what();
        #endif
            }
            catch(...)
            {
                success = false;
        #ifdef DEBUG
                qDebug() << "takeCoord(n1) produced false";
        #endif
            }
            if(success && arraySize_ != 0)
            {
                emit validationSucceed(possibleMoves_, arraySize_);
                if(possibleMoves_)
                {
                    delete[] possibleMoves_;
                    possibleMoves_ = 0;
                }
            }
        }
            break;
        case 2: // PvE
        {
            emit selectComputerPiece();
            historyIO_->setPiecePosition(n1);   //  ���������� � ����
        }
            break;
    }
}

void ChessWindow::recieveNewPosition(int n2, bool sendViaLan)
{
    switch(mode_)
    {
    case 0: // PvP
    case 1:
    {
	if(possibleMoves_)
	{
		delete[] possibleMoves_;
		possibleMoves_ = 0;
	}
	bool success;
	try
	{
		success = game_->takeCoord(n2, &possibleMoves_, arraySize_);
        // history
        HistoryIO::CellData data(game_->getJMChess(), game_->getEatenChess());
        historyIO_->setNewPosition(n2, data);
        QString str = QString::fromStdString(historyIO_->getMoveString());
		navEdit_->append(str);
		// �������� ������ �� ��������� ����
		if(client_ && sendViaLan)
		{
			client_->SendData(str);
		}
		else if (server_ && sendViaLan)
		{
            try {
			server_->SendData(str);
            }catch(...){}
		}
	}
	catch(const std::exception& e)
	{
		success = false;
#ifdef DEBUG
		qDebug() << "takeCoord(n2) produced: "<<e.what();
#endif
	}catch(...)
	{
		success = false;
#ifdef DEBUG
		qDebug() << "takeCoord(n2) produced false";
#endif
	}

	if(success)
	{
		emit validationSucceed(possibleMoves_, arraySize_);
	}
    }
        break;
    case 2:     // PvE
    {
        // history
        historyIO_->setNewPosition(n2, HistoryIO::CellData());
        QString str = QString::fromStdString(historyIO_->getMoveString());

        if(pveGame_->OpponentTurn(str.toStdString().c_str()))
        {
            navEdit_->append(str);

            currentMove_ = 1;   // comp move
            switchPlayer(true, 0);
            char n1Str[2];
            char n2Str[2];
            // letter               // digit
            n1Str[0] = str[0].toLatin1(); n1Str[1] = str[1].toLatin1();
            n2Str[0] = str[2].toLatin1(); n2Str[1] = str[3].toLatin1();
            int n1 = HistoryIO::fromStandartNotation(n1Str);
            int n2 = HistoryIO::fromStandartNotation(n2Str);
            emit performMove(n1, n2);
            pveGame_->PrintBoardString();
        }
        else
            emit cancelComputerSelect();
    }
        break;
    }
}

void ChessWindow::computerTurn()
{
    pveGame_->Sync();
    if(currentMove_)
    {
        char *compMove = new char[4];
        pveGame_->ComputerTurn(compMove);
        navEdit_->append(compMove);
        currentMove_ = automode_ ? 1 : 0;   // opponent move
        switchPlayer(false, 0);
        char n1Str[2];
        char n2Str[2];
        // letter               // digit
        n1Str[0] = compMove[0]; n1Str[1] = compMove[1];
        n2Str[0] = compMove[2]; n2Str[1] = compMove[3];
        int n1 = HistoryIO::fromStandartNotation(n1Str);
        int n2 = HistoryIO::fromStandartNotation(n2Str);
        delete[] compMove;
        emit performMove(n1, n2);
        pveGame_->PrintBoardString();
    }
}

void ChessWindow::switchPlayer(bool black, int score)
{
	if(black)
	{
		black_->setCheckable(true);
		black_->setChecked(true);
		white_->setCheckable(false);
        blackScore_->setText(QString::number(score));
	}
	else
	{
		white_->setCheckable(true);
		white_->setChecked(true);
		black_->setCheckable(false);
		whiteScore_->setText(QString::number(score));
	}

}

//-------------------------------------------------------------------
// ����������� �������� ������� client, server
//-------------------------------------------------------------------
void ChessWindow::createGame()
{
	if(server_)
	{
		 QMessageBox msgBox;
		 msgBox.setText(tr("The server is running!"));
		 msgBox.setStandardButtons(QMessageBox::Ok);
		 msgBox.setDefaultButton(QMessageBox::Ok);
		 msgBox.exec();
		 return;
	}

#ifdef DEBUG
		qDebug() << "Creating the game...";
#endif

	server_ = new Server(0, 1922);
    connect(server_, SIGNAL(RecieveMessage(QString)), this, SLOT(recievedMessage(QString)));
	// ����� ������ ip �������
	lanEdit_->clear();
	QList<QNetworkInterface> namelist;
	QList<QHostAddress> addrlist;
	namelist = QNetworkInterface::allInterfaces();
	addrlist = QNetworkInterface::allAddresses();
	setlocale(LC_ALL, "Russian");
	unsigned int i = 1;
	foreach (const QHostAddress &address, addrlist) 
	{
		if (address.protocol() == QAbstractSocket::IPv4Protocol && 
			address != QHostAddress(QHostAddress::LocalHost)    && 
			QNetworkInterface::IsUp                             && 
			QNetworkInterface::IsRunning                        && 
			QNetworkInterface::IsLoopBack) 
		{
#ifdef DEBUG
			qDebug() << namelist.takeFirst().humanReadableName() << address.toString();
#endif
			// ����� �� �����
            lanEdit_->append(QString::number(i++) + ")" + namelist.takeFirst().humanReadableName() + " " + address.toString());
		}
	}
}

void ChessWindow::joinGame()
{
	if(client_)
	{
		 QMessageBox msgBox;
		 msgBox.setText(tr("The client is connected!"));
		 msgBox.setStandardButtons(QMessageBox::Ok);
		 msgBox.setDefaultButton(QMessageBox::Ok);
		 msgBox.exec();
		 return;
	}

#ifdef DEBUG
		qDebug() << "Joining the game...";
#endif

	AddressDialog add(this);
	if(add.exec())
	{
		QString address = add.getIPAddress();
#ifdef DEBUG
		client_ = new Client("localhost", 1922, 0); 
#else
		client_ = new Client(address, 1922, 0); 
#endif
        connect(client_, SIGNAL(RecieveMessage(QString)), this, SLOT(recievedMessage(QString)));
		add.close();
	}
	else
	{
#ifdef DEBUG
		qDebug() << "Rejected to input ip.";
#endif
	}
}

void ChessWindow::recievedMessage(QString str)
{
	if(str.length() == 4)
	{
        data::move mv;
        try
        {
		mv.n1 = HistoryIO::fromStandartNotation(str.left(2).toStdString().c_str());
		mv.n2 = HistoryIO::fromStandartNotation(str.right(2).toStdString().c_str());
        }catch(...){}
#ifdef DEBUG
		qDebug() << "recieved str: "<<str<<" n1: "<<mv.n1<<" n2: "<<mv.n2;
        //navEdit_->setText("recieved str: " + str + " n1: " + mv.n1 + " n2: " + mv.n2);
#endif
        //recievePiecePosition(mv.n1);
        //recieveNewPosition(mv.n2, false);
	}
}

void ChessWindow::infoRecieved(std::string str, int dataType)
{
    switch(dataType)
    {
    case 0:     // print result
        qDebug() << "update info\n";
        pveEditInfo_->append(QString::fromStdString(str));
        break;
    case 1:     // print board
        qDebug() << "update board\n";
        pveEditBoard_->setText(QString::fromStdString(str));
        break;
    case 2:
        QMessageBox msgBox;
        msgBox.setText("Mate!");
        msgBox.exec();
        navEdit_->append("x");
        break;
    }
}

void ChessWindow::changeDepth(int depth)
{
    if(pveGame_)
    {
        pveGame_->SetupMaxDepth(depth);
        qDebug() << "depth changed to " << depth << "\n";
    }
}

void ChessWindow::autoMode(bool autoMode)
{
    automode_ = autoMode;
}

void ChessWindow::check()
{
    QMessageBox msgBox;
    msgBox.setText("Check!");
    msgBox.exec();
    navEdit_->append("+\n");
}
