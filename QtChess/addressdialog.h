#ifndef ADDRESSDIALOG_H
#define ADDRESSDIALOG_H

#include <QDialog>

class QLineEdit;

class AddressDialog : public QDialog
{
	Q_OBJECT

public:
	explicit AddressDialog(QWidget *parent = 0);
	~AddressDialog() { };

	QString getIPAddress() const;

private:
	 QLineEdit *lineAdd_;
};

#endif // ADDRESSDIALOG_H
