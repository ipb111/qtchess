#ifndef IPVECHESSENGINE_H
#define IPVECHESSENGINE_H

#include <QWidget>
#include <QObject>

struct IPVEChessEngine *CreateChessEngine();

struct IPVEChessEngine : public QObject
{
    IPVEChessEngine(QWidget *parent) : QObject(parent) { }
    virtual ~IPVEChessEngine() { }

    virtual void SetupMaxDepth(unsigned int depth) = 0;
    virtual void SetupMaxTime(unsigned int time) = 0;
    virtual bool ComputerTurn(char *outMove) = 0;
    virtual void PrintBoardString() = 0;
    virtual bool OpponentTurn(const char *mv) = 0;
    virtual void EndGame() = 0;
    virtual void Unmove() = 0;
    virtual void Init() = 0;
    virtual void New() = 0;

    virtual void Sync() = 0;
};

Q_DECLARE_INTERFACE(IPVEChessEngine, "pve/1.0")

#endif // IPVECHESSENGINE_H
