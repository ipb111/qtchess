/*
Copyright (c) <2013>, <Ilya Shoshin>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "startwindow.h"

#include <QFileDialog>
#include <QToolTip>

StartWindow::StartWindow(QWidget *parent)
	: QWidget(parent)
{
	const int offset = 10;
	_logo = new QLabel(this);
	_logo->setGeometry(QRect(QPoint(offset, offset), QSize(170, 170)));
	_logo->setPixmap(QPixmap(":/images/logo"));
	_logo->setScaledContents(true);
    _buttonNew = new QPushButton(tr("New game"), this);
    _buttonNew->setToolTip(tr("Create new game."));
    _buttonLoad = new QPushButton(tr("Load game"), this);
    _buttonLoad->setToolTip(tr("Load existing game."));
	_buttonNew->setGeometry (QRect(QPoint(offset,  _logo->pos().y() + _logo->size().height() + offset), QSize(200, 50)));
	_buttonLoad->setGeometry(QRect(QPoint(_buttonNew->pos().x() + _buttonNew->width() + offset, _logo->pos().y() + _logo->height() + offset), QSize(200, 50)));
	connect(_buttonNew, SIGNAL(released()), this, SLOT(handleButtonNewClick()));
	connect(_buttonLoad, SIGNAL(released()), this, SLOT(handleButtonLoadClick()));
    _company = new QLabel(tr("Chess"), this);
	_company->setFont(QFont("Tahoma", 24));
	_company->setGeometry(QRect(QPoint(_buttonNew->pos().x() + _buttonNew->width() + offset, offset), QSize(200, 200)));
	_mode = new QLabel(tr(" Game mode:"), this);
	_mode->setFont(QFont("Tahoma", 10));
	_mode->setGeometry(QRect(QPoint((_buttonNew->pos().x() + _buttonNew->width() + offset) * 0.5f, _buttonNew->pos().y() + _buttonNew->height() + offset), QSize(200, 30)));
	_comboMode = new QComboBox(this);
	_comboMode->setGeometry(QRect(QPoint(_buttonLoad->pos().x(),_buttonNew->pos().y() + _buttonNew->height() + offset), QSize(200, 30)));
    _comboMode->addItem(tr("PvP local"));
    _comboMode->addItem(tr("PvP via network"));
    _comboMode->addItem(tr("PvE"));
    _comboMode->setCurrentIndex(2);
	_footer = new QLabel(tr("(c) 2013 ipb-1-11 (MIREA)"), this);
	_footer->setFont(QFont("Tahoma", 8));
	_footer->setGeometry(QRect(QPoint((_buttonNew->pos().x() + _buttonNew->width() + offset) * 0.65f, _comboMode->pos().y() + _comboMode->height() + offset), QSize(200, 30)));
}

StartWindow::~StartWindow()
{
	// �����, ��������� ����� ������ ������������, ��� ��� ���������� (� �������� Qt) ����� �����������
	// ��� �������. ��������� ������ ��� ������ ����� ������������:
	// setAttribute( Qt::WA_DeleteOnClose ); /* Qt ������ ������ � ���� ��� ����������� �� �������� */
}

//--------------------------------------------------------------------------
// �����������
//--------------------------------------------------------------------------

void StartWindow::handleButtonNewClick()
{
	newGame();
}

void StartWindow::handleButtonLoadClick()
{
	// �������� ����������� ���� � ��������� ����� �����
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "",
                                                    tr("Text Files (*.txt)"));
	if(!fileName.isNull() && !fileName.isEmpty())
		loadGame(fileName);
}

//--------------------------------------------------------------------------

void StartWindow::newGame()
{
	emit openChessWindow(_comboMode->currentIndex());
}

void StartWindow::loadGame(QString gameName)
{
	emit openChessWindow(_comboMode->currentIndex(), gameName);
}
