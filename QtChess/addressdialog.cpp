#include "addressdialog.h"

#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QGridLayout>

AddressDialog::AddressDialog(QWidget *parent)
	: QDialog(parent, Qt::WindowSystemMenuHint)
{
	this->setModal(true);
	this->setWindowModality(Qt::WindowModal);
	this->setWindowTitle("IP address");

	QGridLayout *mainLayout = new QGridLayout(this);
    mainLayout->setSizeConstraint(QLayout::SetFixedSize);

	QLabel *labelIP = new QLabel("IP:",this);
	lineAdd_ = new QLineEdit(this);
    lineAdd_->setToolTip(tr("Input the ip address of the host."));
	lineAdd_->setInputMask("000.000.000.000;");

	QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
		| QDialogButtonBox::Cancel, Qt::Horizontal, this);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

	mainLayout->addWidget(buttonBox, 1, 1);
    mainLayout->addWidget(labelIP, 0, 0);
	mainLayout->addWidget(lineAdd_, 0, 1);

	setLayout(mainLayout);
}

QString AddressDialog::getIPAddress() const
{
	return lineAdd_->text();
}
